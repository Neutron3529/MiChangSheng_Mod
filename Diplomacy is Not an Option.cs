#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin
#   Copyright (C) 2024- Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put this file (suppose its name is `${file}.cs`)
#   * and `utils.cs` in `steamapps` folder, open a terminal in the same folder,
#   * and execute:
#   *
#   * ```
#   *     chmod +x "${file}.cs" && ./"$_"
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro KDE), if such script do not work
#   * in your computer, you could just try the instructions below :

export CODE_START_LINE_NO=101                               # might be modified if the number mismatch.
export GAME_NAME="${0%\.cs}"                                # might modify if the name mismatch.
export GAME_DIR="$GAME_NAME"                                # might be modified, but "$GAME_NAME" cover most of the cases.

export PLUGIN_ID="Neutron3529.Cheat"                        # for id (deleting ${PLUGIN_ID}.cfg) and class name (its last name), should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # for logger, should be modified
export VERSION="0.1.0"                                      # should be modified

export FILE_NAME="$0"
export ASSEMBLY="Assembly-CSharp"                           # might be modified
export UTILS="utils.cs"                                     # might be modified if you do not put utils.cs in the current dir.
export GAME_BASE_DIR="common/$GAME_DIR"                     # this could be used for modifying the following 3 dir in a faster way. Should not be modified unless the mod is installed in another location.
export plugins_dir="${GAME_BASE_DIR}/BepInEx/plugins"
export BepInEx_dir="${GAME_BASE_DIR}/BepInEx/core"
export Managed_dir="${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed"
export plugins_name="${FILE_NAME%.*}"                       # change output dll name (avoid taiwu-like errors)
export BepInEx_dll='
    0Harmony.dll
    BepInEx.dll
    BepInEx.Harmony.dll
'
export Managed_dll=$([[ -e "${Managed_dir}/netstandard.dll" ]] && echo "netstandard.dll")'
    System.dll
    System.Core.dll
    UnityEngine.dll
    UnityEngine.AIModule.dll
    UnityEngine.CoreModule.dll
    UnityEngine.UI.dll
    mscorlib.dll
''
Unity.Entities.dll
Unity.Jobs.dll
Unity.Burst.dll
Unity.Collections.dll
Unity.Mathematics.dll
DNO.Main.dll
' # game specific dlls could be added here.
export DOTNET="dotnet" # the location of the DOTNET executable file.
[[ -z "$DOTNET_CSC_DLL" ]] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll
export IFS=$'\n' # to disable the annoying space.
case $1 in
    V | v | VERBOSE | verbose )   EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
    D | d | DEBUG   | debug   )   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
    *)                            EXTRA_DEFINE=""                                               ;;
esac

# Pre-compile code
# Compile
( yes "" | head -n $(($CODE_START_LINE_NO - 1)) && tail $FILE_NAME -n+$CODE_START_LINE_NO) | sed -e s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g -e s/%%PLUGIN_ID%%/${PLUGIN_ID}/g -e s/%%PLUGIN_ID_LAST%%/${PLUGIN_ID##*.}/g -e s/%%VERSION%%/${VERSION}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
    $(for i in ${BepInEx_dll} ; do [[ -z "${i// }" ]] || echo -e "-r:\"${BepInEx_dir}/${i// }\"\n" ; done) \
    $(for i in ${Managed_dll} ; do [[ -z "${i// }" ]] || echo -e "-r:\"${Managed_dir}/${i// }\"\n" ; done) \
    $(for i in "${Managed_dir}/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
    -out:"${plugins_dir}/${plugins_name}".dll \
    -optimize $EXTRA_DEFINE \
    - $UTILS || exit
# Post-compile code
rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg"
if [[ -n "$2" ]] ; then
    git add ${FILE_NAME}
    case $2 in
        R | r | RANDOM | random | U | u | UPLOAD | upload ) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        *) git commit -am "$2" ;;
    esac
    git push
fi
exit


using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Components;
using Components.RawComponents;
using Systems;
using Systems.ComponentSystemGroups;
using static Utility.SharedNativeContainersUtility;

using Utility;
using Components.Structs;
using Components.RawComponents.ResearchComponents;
using Neutron3529.Utils;
namespace %%NAMESPACE_ID%%;
// 在删除lib_burst_generated.dll之后可用，但速度……
[BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
public class Cheat : /*Neutron3529.Utils.*/ModEntry {
    public Cheat() : base("%%PLUGIN_ID%%") {}
    public override void Awake() {
        base.Awake();
        // 主要逻辑放在`utils.cs`中，这里的Awake2只是为了以防万一
        // 目前来说，这个函数的唯一用途是用来“叮”……
        // 就像这样：
        logger("叮~修改器启动，请安心游戏");
    }
    [BurstCompile]
    public class ConstEntry : Const {
        [Desc("随身治疗（每帧都会回满生命值）")]
        public static bool immue = false;
        [Desc("剑刃风暴（增加攻速，请不要使用技能修改攻速（因为我没做技能的读取））")]
        public static bool burst = false;
        [Desc("侵略如火（调整最大生命倍率）")]
        public static float invade_h = 10;
        [Desc("侵略如火（调整攻速倍率）")]
        public static float invade_f = 10;
        [Desc("侵略如火（调整移速倍率）")]
        public static float invade_s = 10;
        [Desc("侵略如火（调整攻击力倍率）")]
        public static float invade_a = 10;
        [Desc("粮草先行（仓库永远爆满）")]
        public static bool reserve = true;
        [Desc("才思敏捷（研究速度飞快）")]
        public static bool research = true;
        [Desc("训练有素（因此可以立刻完成训练）")]
        public static bool well_trained = true;
        [Desc("博古通今（获取全部任务信息）")]
        public static bool dump = true;
        [Desc("鲁班再世（修建建筑速度增加）")]
        public static bool luban = true;
        [Desc("不养懒汉（村民生产速度获得极大提升）")]
        public static bool shiny_tools = true;

        public static ComponentType[] None => new ComponentType[] {
            ComponentType.ReadOnly<Enemy>(),
            ComponentType.ReadOnly<Dead>()/*,
            ComponentType.ReadOnly<Exploded>(),
            ComponentType.ReadOnly<DelayedDestroy>(),
            ComponentType.ReadOnly<MineTrapBase>(),
            ComponentType.ReadOnly<SpikeTrapBase>()*/
        };
    }
    // [Desc("获取任务信息（需删除Diplomacy is Not an Option/Diplomacy is Not an Option_Data/Plugins/x86_64/lib_burst_generated.dll）")]
    // public class InjectMissionInit : Entry {
    //     static int cntr = 0;
    //     static Dictionary<int,string> hm = new();
    //     static string Name(int i) {
    //         if (hm.TryGetValue(i, out var s)) {
    //             return s;
    //         } else {
    //             return "未知id: "+i.ToString();
    //         }
    //     }
    //     [HarmonyPatch(typeof(ScriptableObjectDefinitions.MissionTree), "Init")]
    //     public static void Postfix(ScriptableObjectDefinitions.MissionTree __instance) {
    //         if (cntr++==0) {
    //             logger("注入成功");
    //             foreach(var item in __instance.items) {
    //                 hm[item.config.missionId] = UI.LocalizationStrings.GetMissionsString(item.config.missionTitle);
    //             }
    //             foreach(var item in __instance.items) {
    //                 logger($"任务{item.name} ({item.config.missionId})： {Name(item.config.missionId)}\nFaction: {item.config.faction}\nMissionType: {item.config.type}\nDesc: {UI.LocalizationStrings.GetMissionsString(item.config.missionDescription)}\nFrom: {string.Join(", ",item.previousMissionIds.Select(Name))}\nTo: {string.Join(", ", item.nextMissionsIds.Select(Name))}");
    //             }
    //         } else if (cntr < 10) {
    //             logwarn($"这是第{cntr}次调用Init");
    //         } else if (cntr == 10) {
    //             logwarn($"这是第{cntr}次调用Init，忽略其调用信息");
    //         }
    //     }
    // }
}

[BurstCompile]
[UpdateInGroup(typeof(FightGroup))]
[UpdateAfter(typeof(Systems.FightSystems.AttackSearchSystem))]
public class Healer : Systems.SystemBaseSimulation {
    EntityQuery _query;
    ComponentTypeHandle<Health> _HealthRW;
    ComponentTypeHandle<HealthBase> _HealthBaseRO;
    [BurstDiscard]
    protected override void OnCreateSimulation() {
        if(Enabled = Cheat.ConstEntry.immue) {
            _HealthRW = this.GetComponentTypeHandle<Health>();
            _HealthBaseRO = this.GetComponentTypeHandle<HealthBase>(isReadOnly: true);
            this._query = base.GetEntityQuery(new EntityQueryDesc {
                All = new ComponentType[] {
                    ComponentType.ReadWrite<Health>(),
                    ComponentType.ReadOnly<HealthBase>(),
                    ComponentType.ReadOnly<Damaged>()
                },
                None = Cheat.ConstEntry.None
            });
        }
        Cheat.logger($"{(Enabled?"已":"未")}启用：随身治疗（每帧都会回满生命值）");
    }
    [BurstCompile]
    protected override void OnUpdateSimulation() {
        // Cheat.logger("OnUpdateSimulation started.");
        // base.Dependency = JobChunkExtensions.ScheduleParallel(new Jobs(_HealthRW, _HealthBaseRO), this._query, base.Dependency);
        var z = this._query.GetArchetypeChunkIterator();
        var x = new Jobs(_HealthRW, _HealthBaseRO);
        x.RunWithoutJobs(ref z);
        // var chunks = _query.CreateArchetypeChunkArray(Allocator.TempJob);

        // var HealerJob = new HealerJob
        // {
        //     // Chunks = chunks,
        //     Health = health,
        //     HealthBase = healthBase
        // };
        // this.Dependency = HealerJob.Schedule(this.Dependency);
        // var healthDataFromEntity = base.GetComponentDataFromEntity<Health>();
        // var healthBaseDataFromEntity = base.GetComponentDataFromEntity<HealthBase>(isReadOnly: true);
        // var entityhandle = system.GetEntityTypeHandle();
        // Entities
        //    .WithName("Healer") // Shown in error messages and profiler
        //    .WithAll<Health, HealthBase>() // Require the LocalToWorld component
        //    .WithNone<Enemy, Dead, Exploded>() // Require the LocalToWorld component
        //    .WithNone<DelayedDestroy, MineTrapBase, SpikeTrapBase>() // Require the LocalToWorld component
        //    .ForEach(
        //        // Write to Displacement (ref), read Velocity (in)
        //        (Unity.Entities.UniversalDelegates.RI<Health, HealthBase>)((ref Health h, in HealthBase hb) => {
        //             Cheat.logger("executed {cd.currentHealth} -> {hb.GetData().maxHealth}");
        //             h.currentHealth=hb.GetData().maxHealth;
        //         })
        //    )
        //    .ScheduleParallel(base.Dependency); // Run it.
        // // base.Dependency = HealerJob.ScheduleParallel(this, this._query, base.Dependency);
        // Cheat.logger("OnUpdateSimulation done.");
    }
    [BurstCompile]
    public partial struct Jobs : IJobChunk {
        ComponentTypeHandle<Health> _HealthRW;
        ComponentTypeHandle<HealthBase> _HealthBaseRO;
        public Jobs(ComponentTypeHandle<Health> _HealthRW, ComponentTypeHandle<HealthBase> _HealthBaseRO) {
            this._HealthRW=_HealthRW;
            this._HealthBaseRO=_HealthBaseRO;
        }
        [BurstCompile]
        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
            var count = chunk.Count;
            IntPtr health = InternalCompilerInterface.UnsafeGetChunkNativeArrayIntPtr(chunk, this._HealthRW);
            IntPtr healthBase = InternalCompilerInterface.UnsafeGetChunkNativeArrayReadOnlyIntPtr(chunk, this._HealthBaseRO);
            for (int i = 0; i != count; i++) {
                this.Exec(ref InternalCompilerInterface.UnsafeGetRefToNativeArrayPtrElement<Health>(health, i),
                            in InternalCompilerInterface.UnsafeGetRefToNativeArrayPtrElement<HealthBase>(healthBase, i));
            }
        }
        [BurstCompile]
        void Exec(ref Health h, in HealthBase hb) {
            // Cheat.logger($"executed {h.currentHealth} -> {hb.GetData().maxHealth}");
            h.currentHealth+=hb.GetData().maxHealth;
        }
    }
}

[BurstCompile]
[UpdateInGroup(typeof(FightGroup))]
[UpdateBefore(typeof(Systems.FightSystems.AttackSearchSystem))]
public class Speeder : Systems.SystemBaseSimulation {
    EntityQuery _query;
    ComponentTypeHandle<AttackCooldown> _AttackCooldownRW;
    [ReadOnly]
    EntityTypeHandle _EntityRW;
    [ReadOnly]
    ResearchAffectUtility<ResearchUnitAffectData, UnitBase, UnitBaseData> _unitAffectUtilityRO;
    protected override void OnCreateSimulation() {
        if(Enabled = Cheat.ConstEntry.burst) {
            _AttackCooldownRW = this.GetComponentTypeHandle<AttackCooldown>();
            _EntityRW = this.GetEntityTypeHandle();
            _unitAffectUtilityRO = ResearchAffectUtility<ResearchUnitAffectData, UnitBase, UnitBaseData>.Get(this);
            this._query = base.GetEntityQuery(new EntityQueryDesc {
                All = new ComponentType[] {
                    ComponentType.ReadWrite<AttackCooldown>()
                },
                None = Cheat.ConstEntry.None
            });
        }
        Cheat.logger($"{(Enabled?"已":"未")}启用：剑刃风暴（增加攻速）");
    }
    [BurstCompile]
    protected override void OnUpdateSimulation() {
        // Cheat.logger("OnUpdateSimulation started.");
        var z = this._query.GetArchetypeChunkIterator();
        var x = new Jobs(_AttackCooldownRW, _EntityRW, _unitAffectUtilityRO);
        x.RunWithoutJobs(ref z);
    }
    [BurstCompile]
    public partial struct Jobs : IJobChunk {
        ComponentTypeHandle<AttackCooldown> _AttackCooldownRW;
        [ReadOnly]
        EntityTypeHandle _EntityRW;
        [ReadOnly]
        ResearchAffectUtility<ResearchUnitAffectData, UnitBase, UnitBaseData> _unitAffectUtilityRO;
        public Jobs(ComponentTypeHandle<AttackCooldown> _AttackCooldownRW, EntityTypeHandle _EntityRW, ResearchAffectUtility<ResearchUnitAffectData, UnitBase, UnitBaseData> _unitAffectUtilityRO) {
            this._AttackCooldownRW=_AttackCooldownRW;
            this._EntityRW = _EntityRW;
            this._unitAffectUtilityRO = _unitAffectUtilityRO;
        }
        [BurstCompile]
        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
            var count = chunk.Count;
            IntPtr entityRW = InternalCompilerInterface.UnsafeGetChunkEntityArrayIntPtr(chunk, this._EntityRW);
            IntPtr attackCooldownRW = InternalCompilerInterface.UnsafeGetChunkNativeArrayIntPtr(chunk, this._AttackCooldownRW);
            for (int i = 0; i != count; i++) {
                this.Exec(ref InternalCompilerInterface.UnsafeGetRefToNativeArrayPtrElement<AttackCooldown>(attackCooldownRW, i),
                    _unitAffectUtilityRO.GetData(InternalCompilerInterface.UnsafeGetCopyOfNativeArrayPtrElement<Entity>(entityRW, i))
                );
            }
        }
        [BurstCompile]
        void Exec(ref AttackCooldown cd, UnitBaseData unitBaseData) {
            // Cheat.logger($"executed {h.currentHealth} -> {hb.GetData().maxHealth}");
            /*if(cd.value > unitBaseData.attackRate) {
                cd.value = unitBaseData.attackRate;
            } else */if(cd.value == unitBaseData.attackRate) {
                // reset attack
            } else if(cd.value > unitBaseData.applyDelay) {
                cd.value = unitBaseData.applyDelay; // Ranged : unitBaseData.attackRate (calculate target) ->  unitBaseData.applyDelay (shoot)
            } else if(cd.value>0) {
                cd.value = unitBaseData.attackRate;
            }/* else if (cd.value>0) {
                cd.value = 0; // reset animation.
            }*/
        }
    }
}


[BurstCompile]
[UpdateBefore(typeof(ResourceStacksSystem))]
[UpdateInGroup(typeof(ResourceDeliveryGroup))]
public class ReservedResources : Systems.SystemBaseSimulation {
    EntityQuery _queryFood;
    EntityQuery _queryWood;
    EntityQuery _queryIron;
    EntityQuery _queryStone;
    // ComponentTypeHandle<CurrentFood> _FoodRW;
    ComponentTypeHandle<FoodStorage> _FoodRW;
    // ComponentTypeHandle<FoodReserve> _FoodRO;
    // ComponentTypeHandle<CurrentWood> _WoodRW;
    ComponentTypeHandle<WoodStorage> _WoodRW;
    // ComponentTypeHandle<WoodReserve> _WoodRO;
    // ComponentTypeHandle<CurrentStone> _StoneRW;
    ComponentTypeHandle<StoneStorage> _StoneRW;
    // ComponentTypeHandle<StoneReserve> _StoneRO;
    // ComponentTypeHandle<CurrentIron> _IronRW;
    ComponentTypeHandle<IronStorage> _IronRW;
    // ComponentTypeHandle<IronReserve> _IronRO;
    protected override void OnCreateSimulation() {
        if(Enabled = Cheat.ConstEntry.reserve) {
            // ComponentTypeHandle<([^>]+)> ([^;]+);
            // ->
            // \2 = this.GgetComponentTypeHandle<\1>();
            _FoodRW = this.GetComponentTypeHandle<FoodStorage>();
            _WoodRW = this.GetComponentTypeHandle<WoodStorage>();
            _StoneRW = this.GetComponentTypeHandle<StoneStorage>();
            _IronRW = this.GetComponentTypeHandle<IronStorage>();
            this._queryFood = base.GetEntityQuery(new EntityQueryDesc {
                All = new ComponentType[] {
                    ComponentType.ReadWrite<FoodStorage>()
                },
                None = Cheat.ConstEntry.None
            });
            this._queryWood = base.GetEntityQuery(new EntityQueryDesc {
                All = new ComponentType[] {
                    ComponentType.ReadWrite<WoodStorage>()
                },
                None = Cheat.ConstEntry.None
            });
            this._queryStone = base.GetEntityQuery(new EntityQueryDesc {
                All = new ComponentType[] {
                    ComponentType.ReadWrite<StoneStorage>()
                },
                None = Cheat.ConstEntry.None
            });
            this._queryIron = base.GetEntityQuery(new EntityQueryDesc {
                All = new ComponentType[] {
                    ComponentType.ReadWrite<IronStorage>()
                },
                None = Cheat.ConstEntry.None
            });
        }
        Cheat.logger($"{(Enabled?"已":"未")}启用：粮草先行（仓库永远爆满）");
    }
    [BurstCompile]
    protected override void OnUpdateSimulation() {
        // Cheat.logger("OnUpdateSimulation started.");
        {
            var x = new JobFood(_FoodRW, this);
            var z = this._queryFood.GetArchetypeChunkIterator();
            x.RunWithoutJobs(ref z);
        }{
            var x = new JobWood(_WoodRW, this);
            var z = this._queryWood.GetArchetypeChunkIterator();
            x.RunWithoutJobs(ref z);
        }{
            var x = new JobStone(_StoneRW, this);
            var z = this._queryStone.GetArchetypeChunkIterator();
            x.RunWithoutJobs(ref z);
        }{
            var x = new JobIron(_IronRW, this);
            var z = this._queryIron.GetArchetypeChunkIterator();
            x.RunWithoutJobs(ref z);
        }
    }
    [BurstCompile]
    public partial struct JobFood : IJobChunk {
        ComponentTypeHandle<FoodStorage> _FoodRW;
        Systems.SystemBaseSimulation system;
        public JobFood(ComponentTypeHandle<FoodStorage> _FoodRW, Systems.SystemBaseSimulation system) {
            this._FoodRW=_FoodRW;
            this.system = system;
        }
        [BurstCompile]
        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
            var count = chunk.Count;
            IntPtr FoodRW = InternalCompilerInterface.UnsafeGetChunkNativeArrayIntPtr(chunk, this._FoodRW);
            for (int i = 0; i != count; i++) {
                this.Exec(ref InternalCompilerInterface.UnsafeGetRefToNativeArrayPtrElement<FoodStorage>(FoodRW, i));
            }
        }
        [BurstCompile]
        void Exec(ref FoodStorage val) {
            var diff = 300 - val.stored;
            if(diff > 0){
                val.stored=300;
                var storage = this.system.GetSingleton<CurrentFood>();
                storage.IncreaseAmount(diff);
                this.system.SetSingleton<CurrentFood>(storage);
            }
        }
    }
    [BurstCompile]
    public partial struct JobWood : IJobChunk {
        ComponentTypeHandle<WoodStorage> _WoodRW;
        Systems.SystemBaseSimulation system;
        public JobWood(ComponentTypeHandle<WoodStorage> _WoodRW, Systems.SystemBaseSimulation system) {
            this._WoodRW=_WoodRW;
            this.system = system;
        }
        [BurstCompile]
        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
            var count = chunk.Count;
            IntPtr WoodRW = InternalCompilerInterface.UnsafeGetChunkNativeArrayIntPtr(chunk, this._WoodRW);
            for (int i = 0; i != count; i++) {
                this.Exec(ref InternalCompilerInterface.UnsafeGetRefToNativeArrayPtrElement<WoodStorage>(WoodRW, i));
            }
        }
        [BurstCompile]
        void Exec(ref WoodStorage val) {
            var diff = 300 - val.stored;
            if(diff > 0){
                val.stored=300;
                var storage = this.system.GetSingleton<CurrentWood>();
                storage.IncreaseAmount(diff);
                this.system.SetSingleton<CurrentWood>(storage);
            }
        }
    }
    [BurstCompile]
    public partial struct JobStone : IJobChunk {
        ComponentTypeHandle<StoneStorage> _StoneRW;
        Systems.SystemBaseSimulation system;
        public JobStone(ComponentTypeHandle<StoneStorage> _StoneRW, Systems.SystemBaseSimulation system) {
            this._StoneRW=_StoneRW;
            this.system = system;
        }
        [BurstCompile]
        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
            var count = chunk.Count;
            IntPtr StoneRW = InternalCompilerInterface.UnsafeGetChunkNativeArrayIntPtr(chunk, this._StoneRW);
            for (int i = 0; i != count; i++) {
                this.Exec(ref InternalCompilerInterface.UnsafeGetRefToNativeArrayPtrElement<StoneStorage>(StoneRW, i));
            }
        }
        [BurstCompile]
        void Exec(ref StoneStorage val) {
            var diff = 300 - val.stored;
            if(diff > 0){
                val.stored=300;
                var storage = this.system.GetSingleton<CurrentStone>();
                storage.IncreaseAmount(diff);
                this.system.SetSingleton<CurrentStone>(storage);
            }
        }
    }
    [BurstCompile]
    public partial struct JobIron : IJobChunk {
        ComponentTypeHandle<IronStorage> _IronRW;
        Systems.SystemBaseSimulation system;
        public JobIron(ComponentTypeHandle<IronStorage> _IronRW, Systems.SystemBaseSimulation system) {
            this._IronRW=_IronRW;
            this.system = system;
        }
        [BurstCompile]
        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
            var count = chunk.Count;
            IntPtr IronRW = InternalCompilerInterface.UnsafeGetChunkNativeArrayIntPtr(chunk, this._IronRW);
            for (int i = 0; i != count; i++) {
                this.Exec(ref InternalCompilerInterface.UnsafeGetRefToNativeArrayPtrElement<IronStorage>(IronRW, i));
            }
        }
        [BurstCompile]
        void Exec(ref IronStorage val) {
            var diff = 300 - val.stored;
            if(diff > 0){
                val.stored=300;
                var storage = this.system.GetSingleton<CurrentIron>();
                storage.IncreaseAmount(diff);
                this.system.SetSingleton<CurrentIron>(storage);
            }
        }
    }
}



[BurstCompile]
[UpdateAfter(typeof(ResearchSystem))]
public class FastResearch : Systems.SystemBaseSimulation {
    protected override void OnCreateSimulation() {
        Enabled = Cheat.ConstEntry.research;
        Cheat.logger($"{(Cheat.ConstEntry.research?"已":"未")}启用：才思敏捷（研究速度飞快）");
    }
    [BurstCompile]
    protected override void OnUpdateSimulation() {
        if (this.HasSingleton<Components.SharedContainerSingletons.ResearchContainerSingleton>()) {
            var container = ComponentSystemBaseManagedComponentExtensions.GetSingleton<Components.SharedContainerSingletons.ResearchContainerSingleton>(this).Container;
            if (container.CurrentResearchId.Value>0) {
                container.ResearchProgress.Value = 1;
            }
        }
    }
}


[BurstCompile]
[UpdateAfter(typeof(ResearchSystem))]
public class Dump : Systems.SystemBaseSimulation {
    protected override void OnCreateSimulation() {
        Enabled = Cheat.ConstEntry.dump;
        Cheat.logger($"{(Enabled?"已":"未")}启用：博古通今（获取全部任务信息）");
    }
    int dump_delay = 600;
    [BurstCompile]
    protected override void OnUpdateSimulation() {
        if((Systems.GameStateSystems.GameState.DefaultSettings?.сommonConfiguration?.campaignMissionsTree?.items?.Count??0)>0 && dump_delay--<0) {
            Enabled = false;
            Cheat.logger("任务信息：");
            static string name(int i, Dictionary<int,string> hm) {
                if (hm.TryGetValue(i, out var s)) {
                    return $"「{s}」";
                } else {
                    return $"「未知id: {i}」";
                }
            }
            // 初始化任务列表
            var __instance = Systems.GameStateSystems.GameState.DefaultSettings.сommonConfiguration.campaignMissionsTree;
            Dictionary<int,string> hm = new();
            var Name = (int i)=>name(i,hm);
            foreach(var item in __instance.items) {
                hm[item.config.missionId] = UI.LocalizationStrings.GetMissionsString(item.config.missionTitle);
            }
            foreach(var item in __instance.items) {
                Cheat.logger($"任务{item.name}（{item.config.missionId}）: {Name(item.config.missionId)}\nFaction: {item.config.faction}\nMissionType: {item.config.type}\nDesc: {UI.LocalizationStrings.GetMissionsString(item.config.missionDescription)}\nFrom: {string.Join("、",item.previousMissionIds.Select(Name))}\nTo: {string.Join("、",item.nextMissionsIds.Select(Name))}".Replace("<br>","\n"));
            }
            Cheat.logger("----任务信息-统计完毕----");
        }
    }
}

[BurstCompile]
[UpdateInGroup(typeof(InitializationSystemGroup))]
[UpdateBefore(typeof(ArmyTrainingSystem))]
public class WellTrained : Systems.SystemBaseSimulation {
    EntityQuery _query;
    BufferTypeHandle<UnitTrainingProcess> _unitTrainingProcessBufferHandleRW;
    protected override void OnCreateSimulation() {
        if(Enabled = Cheat.ConstEntry.well_trained) {
            _unitTrainingProcessBufferHandleRW = this.GetBufferTypeHandle<UnitTrainingProcess>();
            this._query = base.GetEntityQuery(new EntityQueryDesc {
                All = new ComponentType[] {
                    ComponentType.ReadOnly<UnitTrainingProcess>()
                },
                None = new ComponentType[]
                {
                    ComponentType.ReadOnly<CurrentBuildingForConstruction>(),
                    ComponentType.ReadOnly<InConstruction>()
                }
            });
        }
        Cheat.logger($"{(Enabled?"已":"未")}启用：鬼子来啦（受到此口号的影响，军民参军速度上升）");
    }
    [BurstCompile]
    protected override void OnUpdateSimulation() {
        var x = new Jobs(this);
        var z = this._query.GetArchetypeChunkIterator();
        x.RunWithoutJobs(ref z);
    }
    [BurstCompile]
    public partial struct Jobs : IJobChunk {
        WellTrained system;
        public Jobs(WellTrained system) {
            this.system = system;
        }
        [BurstCompile]
        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
            var count = chunk.Count;
            // IntPtr entityRW = InternalCompilerInterface.UnsafeGetChunkEntityArrayIntPtr(chunk, this._EntityRW);
            var _unitTrainingProcessBufferAccessorRW = chunk.GetBufferAccessor(this.system._unitTrainingProcessBufferHandleRW);
            for (int i = 0; i != count; i++) {
                var UnitTrainingProcess = _unitTrainingProcessBufferAccessorRW[i];
                if (UnitTrainingProcess.Length > 0) {
                    var temp = UnitTrainingProcess[0];
                    temp.TimeToTrainingEnd = 0f;
                    UnitTrainingProcess[0] = temp;
                }
            }
        }
    }
}


[BurstCompile]
[UpdateInGroup(typeof(InitializationSystemGroup))]
[UpdateBefore(typeof(ArmyTrainingSystem))]
public class LubanTheSecond : Systems.SystemBaseSimulation {
    EntityQuery _query;
    ComponentTypeHandle<ConstructionWorkCollector> _ConstructionWorkCollectorRW;
    ComponentTypeHandle<Health> _HealthRW;
    protected override void OnCreateSimulation() {
        if(Enabled = Cheat.ConstEntry.luban) {
            _ConstructionWorkCollectorRW = this.GetComponentTypeHandle<ConstructionWorkCollector>();
            _HealthRW = this.GetComponentTypeHandle<Health>();
            this._query = base.GetEntityQuery(new EntityQueryDesc {
                All = new ComponentType[] {
                    ComponentType.ReadWrite<ConstructionWorkCollector>(),
                    ComponentType.ReadWrite<Health>(),
                    // ComponentType.ReadOnly<CurrentBuildingForConstruction>(),
                    // ComponentType.ReadOnly<InConstruction>() // 升级中的建筑不会含有这个标记
                },
                None = new ComponentType[]{}
            });
        }
        Cheat.logger($"{(Enabled?"已":"未")}启用：鲁班再世（修建建筑速度增加）");
    }
    [BurstCompile]
    protected override void OnUpdateSimulation() {
        var x = new Jobs(this);
        var z = this._query.GetArchetypeChunkIterator();
        x.RunWithoutJobs(ref z);
    }
    [BurstCompile]
    public partial struct Jobs : IJobChunk {
        LubanTheSecond system;
        public Jobs(LubanTheSecond system) {
            this.system = system;
        }
        [BurstCompile]
        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
            var count = chunk.Count;
            IntPtr ConstructionWorkCollectorRW = InternalCompilerInterface.UnsafeGetChunkNativeArrayIntPtr(chunk, this.system._ConstructionWorkCollectorRW);
            IntPtr HealthRW = InternalCompilerInterface.UnsafeGetChunkNativeArrayIntPtr(chunk, this.system._HealthRW);
            for (int i = 0; i != count; i++) {
                this.Exec(ref InternalCompilerInterface.UnsafeGetRefToNativeArrayPtrElement<ConstructionWorkCollector>(ConstructionWorkCollectorRW, i),
                    ref InternalCompilerInterface.UnsafeGetRefToNativeArrayPtrElement<Health>(HealthRW, i));
            }
        }
        [BurstCompile]
        public void Exec(ref ConstructionWorkCollector cons, ref Health health) {
            if(cons.WorkAmountDone>0){
                cons.WorkAmountDone = cons.WorkAmountRequired;
                health.currentHealth += 10000000;
            }
        }
    }
}

[BurstCompile]
[UpdateInGroup(typeof(ResourceProducingGroup))]
[UpdateBefore(typeof(Systems.ResourceSystems.ResourceProduceSystem))]
public class ShinyTools : Systems.SystemBaseSimulation {
    EntityQuery _query;
    ComponentTypeHandle<WorkerProcess> _WorkerProcessRW;
    protected override void OnCreateSimulation() {
        if(Enabled = Cheat.ConstEntry.shiny_tools) {
            _WorkerProcessRW = this.GetComponentTypeHandle<WorkerProcess>();
            this._query = base.GetEntityQuery(new EntityQueryDesc {
                All = new ComponentType[] {
                    ComponentType.ReadWrite<WorkerProcess>()
                },
                None = new ComponentType[]{}
            });
        }
        Cheat.logger($"{(Enabled?"已":"未")}启用：不养懒汉（村民生产速度获得极大提升）");
    }
    [BurstCompile]
    protected override void OnUpdateSimulation() {
        var x = new Jobs(this);
        var z = this._query.GetArchetypeChunkIterator();
        x.RunWithoutJobs(ref z);
    }
    [BurstCompile]
    public partial struct Jobs : IJobChunk {
        ShinyTools system;
        public Jobs(ShinyTools system) {
            this.system = system;
        }
        [BurstCompile]
        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
            var count = chunk.Count;
            IntPtr workerProcess = InternalCompilerInterface.UnsafeGetChunkNativeArrayIntPtr(chunk, this.system._WorkerProcessRW);
            for (int i = 0; i != count; i++) {
                this.Exec(ref InternalCompilerInterface.UnsafeGetRefToNativeArrayPtrElement<WorkerProcess>(workerProcess, i));
            }
        }
        [BurstCompile]
        public void Exec(ref WorkerProcess w) {
            w.workStartTime = 0;
        }
    }
}

// [BurstCompile]
// public partial struct HealerJob : IJobParallelForDefer {
//     // [DeallocateOnJobCompletion] public NativeArray<ArchetypeChunk> Chunks;
//     [NativeDisableParallelForRestriction]
//     public ComponentDataFromEntity<Health> Health;
//     [ReadOnly]
//     public ComponentDataFromEntity<HealthBase> HealthBase;
//
//     [BurstDiscard]
//     public void Execute(int index) {
//         Entity entity = this.Entity[index];
//         var health = Health[entity];
//         health.currentHealth=HealthBase[entity].GetData().maxHealth;
//         Health[entity] = health;
//     }
// }
// [BurstCompile]
// public partial struct HealerJob : IJobEntity {
//     // ComponentTypeHandle<Entity> _entityTypeHandleRO;
//     // ComponentTypeHandle<Health> _HealthRW;
//     // ComponentTypeHandle<HealthBase> _HealthBaseRO;
//     void Execute(ref Health h, ref HealthBase hb) {
//         Cheat.logger("executed {cd.currentHealth} -> {hb.GetData().maxHealth}");
//         h.currentHealth=hb.GetData().maxHealth;
//     }
//     // public static void Run(ref Health h, ref HealthBase hb) {
//     //     Cheat.logger("executed {cd.currentHealth} -> {hb.GetData().maxHealth}");
//     //     h.currentHealth=hb.GetData().maxHealth;
//     // }
//     // public static JobHandle ScheduleParallel(SystemBase system, EntityQuery query, JobHandle dependency) {
//     //     Cheat.logger("scheduled");
//     //     HealerJob jobData = default(HealerJob);
//     //     jobData._HealthRW = system.GetComponentTypeHandle<Health>();
//     //     jobData._HealthBaseRO = system.GetComponentTypeHandle<HealthBase>(isReadOnly: true);
//     //     dependency = system.GetReadDependency<Components.SingletonComponents.SkillsContainerSingleton>(dependency);
//     //     dependency = IJobEntityExtensions.ScheduleParallel(jobData, query, dependency);
//     //     system.AddReadDependency<Components.SingletonComponents.SkillsContainerSingleton>(dependency);
//     //     // systemUtility.SharedNativeContainersUtility.AddReadDependency<HealerJob>(dependency);
//     //     return dependency;
//     // }
// }


// [BurstCompile]
// public struct FillHp : IJobEntityBatch {
//     [ReadOnly]
//     private ComponentTypeHandle<Enemy> _enemyHandleRO;
//     [ReadOnly]
//     private ComponentTypeHandle<Health> _healthTypeHandler;
//     [ReadOnly]
//     private ComponentTypeHandle<HealthBase> _healthBaseTypeHandler;
//     [ReadOnly]
//     private ComponentTypeHandle<BuildingBase> _buildingBaseTypeHandler;
//     [ReadOnly]
//     private ComponentTypeHandle<PriceBase> _priceBaseTypeHandler;
//     [ReadOnly]
//     private ComponentDataFromEntity<ResearchHealthAffectData> _researchHealthAffectComponentData;
//     [ReadOnly]
//     private ComponentTypeHandle<ResearchAffectedData> _researchAffectedDataTypeHandler;
//     private NativeReference<CommonState> _commonStateReference;
//     private float _elapsedTime;
//     private bool _freeConstructionAndTrain;
//     public static void Run(SystemBase system, EntityQuery query, NativeReference<CommonState> stateReference) {
//         FillHp jobData = default(FillHp);
//         jobData._enemyHandleRO = system.GetComponentTypeHandle<Enemy>(isReadOnly: true);
//         jobData._healthTypeHandler = system.GetComponentTypeHandle<Health>(isReadOnly: false);
//         jobData._healthBaseTypeHandler = system.GetComponentTypeHandle<HealthBase>(isReadOnly: true);
//         jobData._researchAffectedDataTypeHandler = system.GetComponentTypeHandle<ResearchAffectedData>(isReadOnly: true);
//         jobData._researchHealthAffectComponentData = system.GetComponentDataFromEntity<ResearchHealthAffectData>(isReadOnly: true);
//         jobData._commonStateReference = stateReference;
//         jobData._elapsedTime = system.GetSingleton<CurrentSessionTimeSingleton>().elapsedTime;
//         jobData._freeConstructionAndTrain = GameState.Settings.missionConfiguration.type == MissionConfiguration.MissionType.Sandbox && (GameState.Settings.sandboxOverride?.cheats.freeConstructionAndTrain ?? false);
//         JobEntityBatchExtensions.Run(jobData, query);
//     }
//
//     public void Execute(ArchetypeChunk chunk, int chunkIndex)
//     {
//         CommonState value = this._commonStateReference.Value;
//         NativeArray<Health> nativeArray = chunk.GetNativeArray(this._healthTypeHandler);
//         NativeArray<HealthBase> nativeArray2 = chunk.GetNativeArray(this._healthBaseTypeHandler);
//         NativeArray<BuildingBase> nativeArray3 = chunk.GetNativeArray(this._buildingBaseTypeHandler);
//         NativeArray<PriceBase> nativeArray4 = chunk.GetNativeArray(this._priceBaseTypeHandler);
//         NativeArray<ResearchAffectedData> nativeArray5 = chunk.GetNativeArray(this._researchAffectedDataTypeHandler);
//         for (int i = 0; i < nativeArray.Length; i++)
//         {
//             Health health = nativeArray[i];
//             HealthBaseData value2 = nativeArray2[i].value.Value;
//             bool num = nativeArray5.Length > 0;
//             Entity entity = (num ? nativeArray5[i].affect : Entity.Null);
//             if (num && this._researchHealthAffectComponentData.HasComponent(entity))
//             {
//                 value2 = this._researchHealthAffectComponentData[entity].value;
//             }
//             if (value2.maxHealth > health.currentHealth && health.underAttackTimerEnd <= this._elapsedTime && !nativeArray3[i].value.Value.disableRepair)
//             {
//                 PriceBaseData priceBaseData = nativeArray4[i].Value(this._freeConstructionAndTrain);
//                 priceBaseData.peoplePrice = 0;
//                 value.canBeRepairedGlobalCount++;
//                 float multiplier = 1f - math.clamp(health.currentHealth / value2.maxHealth, 0f, 1f);
//                 value.globalRepairPrice += priceBaseData.MultiplyWithCeilRounding(multiplier);
//             }
//         }
//         this._commonStateReference.Value = value;
//     }
// }


// [BurstCompile]
// public partial struct ProcessSpawnerJob : IJobEntity
// {
//     public EntityCommandBuffer.ParallelWriter Ecb;
//
//     // IJobEntity generates a component data query based on the parameters of its `Execute` method.
//     // This example queries for all Spawner components and uses `ref` to specify that the operation
//     // requires read and write access. Unity processes `Execute` for each entity that matches the
//     // component data query.
//     private void Execute([ChunkIndexInQuery] int chunkIndex, ref Spawner spawner)
//     {
//     }
// }



[BurstCompile]
[UpdateInGroup(typeof(FightGroup))]
[UpdateAfter(typeof(Systems.FightSystems.AttackSearchSystem))]
public class invade : Systems.SystemBaseSimulation {
    EntityQuery _query;
    ComponentTypeHandle<ResearchUnitAffectData> _ResearchUnitAffectDataRW;
    [BurstDiscard]
    protected override void OnCreateSimulation() {
        if(Enabled = (Cheat.ConstEntry.invade_f>1)) {
            _ResearchUnitAffectDataRW = this.GetComponentTypeHandle<ResearchUnitAffectData>();
            this._query = base.GetEntityQuery(new EntityQueryDesc {
                All = new ComponentType[] {
                    ComponentType.ReadWrite<ResearchUnitAffectData>()
                },
                None = new ComponentType[] {
                    ComponentType.ReadOnly<Enemy>(),
                    ComponentType.ReadOnly<Dead>()
                }
            });
        }
        Cheat.logger($"{(Enabled?"已":"未")}启用：随身治疗（每帧都会回满生命值）");
    }
    [BurstCompile]
    protected override void OnUpdateSimulation() {
        var z = this._query.GetArchetypeChunkIterator();
        var x = new Jobs(_ResearchUnitAffectDataRW);
        x.RunWithoutJobs(ref z);
    }
    [BurstCompile]
    public partial struct Jobs : IJobChunk {
        ComponentTypeHandle<ResearchUnitAffectData> _ResearchUnitAffectDataRW;
        public Jobs(ComponentTypeHandle<ResearchUnitAffectData> _ResearchUnitAffectDataRW) {
            this._ResearchUnitAffectDataRW=_ResearchUnitAffectDataRW;
        }
        [BurstCompile]
        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
            var count = chunk.Count;
            IntPtr unitBaseData = InternalCompilerInterface.UnsafeGetChunkNativeArrayIntPtr(chunk, this._ResearchUnitAffectDataRW);
            for (int i = 0; i != count; i++) {
                this.Exec(ref InternalCompilerInterface.UnsafeGetRefToNativeArrayPtrElement<ResearchUnitAffectData>(unitBaseData, i));
            }
        }
        [BurstCompile]
        void Exec(ref ResearchUnitAffectData ubd) {
            if(ubd.value.attackRate>0.1f) {
                ubd.value.applyDelay = 0f;
                ubd.value.attackRate = ubd.value.maxAttackRange>3f?0.0125f:0.1f;
                ubd.value.minAttackRange=0f;
                ubd.value.maxAttackRange+=10f;
            }
        }
    }
}


[BurstCompile]
[UpdateInGroup(typeof(FightGroup))]
[UpdateAfter(typeof(Systems.FightSystems.AttackSearchSystem))]
public class invade_s : Systems.SystemBaseSimulation {
    EntityQuery _query;
    ComponentTypeHandle<ResearchNavigationAgentAffectData> _ResearchNavigationAgentAffectDataRW;
    [BurstDiscard]
    protected override void OnCreateSimulation() {
        if(Enabled = (Cheat.ConstEntry.invade_s>1)) {
            _ResearchNavigationAgentAffectDataRW = this.GetComponentTypeHandle<ResearchNavigationAgentAffectData>();
            this._query = base.GetEntityQuery(new EntityQueryDesc {
                All = new ComponentType[] {
                    ComponentType.ReadWrite<ResearchNavigationAgentAffectData>()
                },
                None = new ComponentType[] {
                    ComponentType.ReadOnly<Enemy>(),
                    ComponentType.ReadOnly<Dead>()
                }
            });
        }
        Cheat.logger($"{(Enabled?"已":"未")}启用：随身治疗（每帧都会回满生命值）");
    }
    [BurstCompile]
    protected override void OnUpdateSimulation() {
        var z = this._query.GetArchetypeChunkIterator();
        var x = new Jobs(_ResearchNavigationAgentAffectDataRW);
        x.RunWithoutJobs(ref z);
    }
    [BurstCompile]
    public partial struct Jobs : IJobChunk {
        ComponentTypeHandle<ResearchNavigationAgentAffectData> _ResearchNavigationAgentAffectDataRW;
        public Jobs(ComponentTypeHandle<ResearchNavigationAgentAffectData> _ResearchNavigationAgentAffectDataRW) {
            this._ResearchNavigationAgentAffectDataRW=_ResearchNavigationAgentAffectDataRW;
        }
        [BurstCompile]
        public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex) {
            var count = chunk.Count;
            IntPtr unitBaseData = InternalCompilerInterface.UnsafeGetChunkNativeArrayIntPtr(chunk, this._ResearchNavigationAgentAffectDataRW);
            for (int i = 0; i != count; i++) {
                this.Exec(ref InternalCompilerInterface.UnsafeGetRefToNativeArrayPtrElement<ResearchNavigationAgentAffectData>(unitBaseData, i));
            }
        }
        [BurstCompile]
        void Exec(ref ResearchNavigationAgentAffectData ubd) {
            if(ubd.value.speed<Cheat.ConstEntry.invade_s) {
                ubd.value.speed *= Cheat.ConstEntry.invade_s;
                ubd.value.rotationSpeed *= Cheat.ConstEntry.invade_s;
                ubd.value.acceleration *= Cheat.ConstEntry.invade_s;
            }
        }
    }
}
