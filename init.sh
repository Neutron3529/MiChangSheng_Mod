#!/bin/bash
echo "这里记载了所以作者自用的源，作者会在每次clone时候执行一遍init.sh防止push时候没有push到各镜像备份上面"

git remote set-url --add origin ./backup.git

git remote set-url --del origin .*gitee.com.* >/dev/null
git remote set-url --del origin .*gitlab.com.* >/dev/null

git remote set-url --add origin git@gitee.com:Neutron3529/MiChangSheng_Mod.git
git remote set-url --add origin git@gitlab.com:Neutron3529/HarmonyMods.git

git remote set-url --del origin ./backup.git
