# 各种BepInEx Mod的仓库

这里存放着各种mod，目前Mod的用法有两种，较新的Mod需要把文件夹放在steam中与steamapps同级的目录下，较老的Mod需要把文件夹放在与游戏exe文件同级的目录下

这里面所有理论上可以编译的.cs文件都是shell套壳，如果环境变量设置无误，可以直接使用`./file_name.cs`来编译

# LICENSE

仓库里面的**全部代码文件**均按AGPL v3 or later发布，早些时候写的cs脚本没有注明这一点，但它们的确也是按AGPL条款发布的程序。

这意味着，你可以转载，但转载时请务必记得开源，也请务必记得提醒其他转载类似mod的人开源。

# 需要预先安装

dotnet编译器
(linux系统，steam，git)


## 觅长生mod

包含若干功能
懒得写文档了
自己看config好了

## 用法

默认情况下，可以直接执行
```
cd ~/.steam/steam/steamapps/common/觅长生/64/
git clone https://gitee.com/Neutron3529/MiChangSheng_Mod
cd MiChangSheng_Mod
chmod +x cheat.cs && ./$_
```
一步完成mod的编译和安装

> 这个仓库里面有一些其他Mod，我都打包放一起了……想用的可以自取，安装方法都一样

### 具体功能

这是一个已经过期的config……

```
## Settings file was created by plugin Cheat v0.1.1
## Plugin GUID: Neutron3529.Cheat

[config]

## 论道后不减玩家灵感
# Setting type: Boolean
# Default value: true
not_reduce_player_linggan = true

## 额外抽卡
# Setting type: Int32
# Default value: 500
extra_draw_cards = 500

## 抽卡时恢复生命
# Setting type: Boolean
# Default value: true
recover_when_draw = true

## 读书用时(设为-1禁用，设为0或许会引发异常，或许不会)
# Setting type: Int32
# Default value: 1
read_book_time = 1

## 突破用时(单位是月，设为-1禁用，设为0或许会引发异常，或许不会)
# Setting type: Int32
# Default value: 1
tupo_time = 1

## 感悟用时（设为-1禁用，未测试设为0是否会导致游戏异常）
# Setting type: Int32
# Default value: 1
ganwu_time = 1

## 思绪至少为需感悟百年的六品（感悟百年是字面时间，因为Mod里面有一天感悟的补丁）
# Setting type: Boolean
# Default value: true
add_sixu_quality = true

## 反转道友交易的物品价值（未更改显示以及结算计算）
# Setting type: Boolean
# Default value: true
rev_exchange_money = true

## 论道获得额外悟道点(需启用“额外论道回合数”)
# Setting type: Int32
# Default value: 300
extra_wudaodian = 300

## 额外论道回合数
# Setting type: Int32
# Default value: 95
extra_lundao_huihe = 95

## 强制更改全部进度条动画为提示信息（可能会引发不一致导致BUG）
# Setting type: Boolean
# Default value: false
force_change_play_fader = false

## 更改部分进度条动画为提示信息，可能会触发偶发BUG
# Setting type: Boolean
# Default value: true
change_play_fader = true

## 不增加耐药性
# Setting type: Boolean
# Default value: true
not_AddNaiYao = true

## 炼器用时为0
# Setting type: Boolean
# Default value: true
lianqi_time = true

## 采集(以及灵核采集)只有字面用时（影响收益）而实际用时为1月，同时增加字面收益 -- 这是最少的用时，因为游戏用一个BUG对“采集不耗时”这个修改做了检查……
# Setting type: Boolean
# Default value: true
caiji_time = true
```
