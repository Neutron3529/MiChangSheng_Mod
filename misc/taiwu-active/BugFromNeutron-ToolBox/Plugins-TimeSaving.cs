// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../Backend/System.dll" -r:"../../Backend/System.Collections.dll" -r:"../../Backend/mscorlib.dll" -r:"../../Backend/netstandard.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../Backend/System.Private.CoreLib.dll" -r:"../../Backend/System.Runtime.dll" -r:"../../Backend/System.Linq.dll" -r:"../../Backend/System.Linq.Expressions.dll" -r:"../../Backend/System.Text.Json.dll" -r:"../../Backend/System.Text.Encodings.Web.dll" -r:"../../Backend/System.ComponentModel.Primitives.dll" -unsafe -optimize -deterministic Plugins-TimeSaving.cs reflection.CS ../UTILS/*.CS -out:TimeSaving.dll -unsafe -debug -define:BACKEND -define:NO_HARMONY -define:MapEditor -define:Parse -define:CharacterMod -define:Expr

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法
//! 这是一个示例Mod，用途是展示事件Mod的标准写法

/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.ComponentModel;
using Utils;
using Utils.Expr;
using static Utils.Logger;
using static Utils.Expr.Checker;
using static Utils.CharacterMod;

using GameData.Domains;
using GameData.Domains.TaiwuEvent.EventHelper;
namespace Neutron3529.Bug;
public static class 节省时间 {
    [DisplayName("int_超度全部灵魂")]
    public static int 超度全部灵魂(){
        if(check(_taiwuCurrProfessionId.enable)){
            var cur=_taiwuCurrProfessionId.get(DomainManager.Extra);
            _taiwuCurrProfessionId.set(DomainManager.Extra,6);  // bypass AddSavedSoul Check;
            var cnt=0;
            var waitingReincarnationChars = new HashSet<int>();
            DomainManager.Character.GetAllWaitingReincarnationCharIds(waitingReincarnationChars);
            foreach (int charId in waitingReincarnationChars) {
                if(!DomainManager.Extra.IsSavedSoul(charId)){
                    DomainManager.Extra.AddSavedSoul(context, charId);
                    cnt++;
                }
            }
            _taiwuCurrProfessionId.set(DomainManager.Extra, cur);  // bypass AddSavedSoul Check;
            return cnt;
        } else {
            return -1;
        }
    }
    [DisplayName("void_增加人物1所在地区的恩义值2")]
    public static void 增加人物1所在地区的恩义值2(GameData.Domains.Character.Character character, short value){
        if(character.GetLocation().IsValid()){
            DomainManager.Extra.SetAreaSpiritualDebt(context, character.GetLocation().AreaId, (short)Math.Clamp(DomainManager.Extra.GetAreaSpiritualDebt(character.GetLocation().AreaId) + value,0,30000));
        }
    }
    [DisplayName("void_消除人物1欠恩失义")]
    public static void 消除人物1欠恩失义(GameData.Domains.Character.Character character){
        if(check(DebtsToTaiwu.enable) && DomainManager.Character.GetFavorabilityMaxValueToTaiwuWithDebt(character.GetId())<30000){
            DebtsToTaiwu.remove_element(DomainManager.Character, character.GetId(), context);
        }
    }
    [DisplayName("void_消除太吾失败的突破格")]
    public static void 消除太吾失败的突破格(){
        if (check(_skillBreakPlateDict.nofin_usable)){
            foreach(var kv in _skillBreakPlateDict.get(DomainManager.Taiwu)){
                var skill=kv.Value;
                bool flag=false;
                if(skill.Failed){
                    flag=true;
                    skill.Failed=false;
                    skill.Finished=false;
                } else if(skill.Finished){
                    continue;
                }
                for(int i=(skill.SelectStepLog?.Count??0)-1;i>=0;i--){
                    var grid=skill.SelectStepLog[i];
                    var (x,y) = skill.GetGridPos(grid);
                    if(skill.Grids[x][y].State==3){
                        // logwarn($"skill.CurrentPoint=({skill.CurrentPoint.Item1},{skill.CurrentPoint.Item2}), xy=({x},{y})");
                        skill.Grids[x][y].State=skill.IsNeighbor((skill.CurrentPoint.Item2,skill.CurrentPoint.Item1), (y,x))?(sbyte)1:(sbyte)0;
                        skill.SelectStepLog.RemoveAt(i);
                        skill.CostedStepCount-=1;
                        flag=true;
                    }
                }
                // List<(byte,byte)> curNeighbors=new();
                // skill.GetBreakGridNeighbour(skill.CurrentPoint.Item2, skill.CurrentPoint.Item1, curNeighbors);
                // foreach(var grid in curNeighbors){
                //     if(skill.Grids[grid.Item1][grid.Item2].State==0){
                //         skill.Grids[grid.Item1][grid.Item2].State=1;
                //     }
                // }
                if(flag){
                    _skillBreakPlateDict.set(DomainManager.Taiwu, kv.Key, skill, context);
                }
            }
        }
    }
    static void set_skill_break_plate(short templateId, GameData.Domains.Taiwu.SkillBreakPlate skill){
        if(!check(_skillBreakPlateDict.usable)){
            return;
        }
        if(skill.Failed){
            skill.Failed=false;
        } else if(skill.Finished){
            return;
        }
        if((skill.SelectedPages & (1 << 8)) == 0 ){
            skill.CostedStepCount = 100;// 入魔加威力
        } else {
            skill.CostedStepCount = 1;// 不入魔加威力
        }
        skill.SelectStepLog?.Clear();
        foreach(var grids in skill.Grids){
            foreach(var grid in grids){
                if(grid.TemplateId==2){
                    grid.State=2;
                } else if(grid.State!=-1){
                    grid.State=0;
                }
            }
        }
        _skillBreakPlateDict.fin(DomainManager.Taiwu, context, templateId, skill);
        _skillBreakPlateDict.set(DomainManager.Taiwu, templateId, skill, context);
    }
    [DisplayName("void_全黄点突破所有突破中的功法")]
    public static void 全黄点突破所有突破中的功法(){
        if (check(_skillBreakPlateDict.usable)){
            foreach(var kv in _skillBreakPlateDict.get(DomainManager.Taiwu)){
                // check(false,$"processing {kv.Key}");
                set_skill_break_plate(kv.Key, kv.Value);
            }
        }
    }
    [DisplayName("int_获取目标1与拜访寺庙数的差值")]
    public static int GetTemples(int target){
        if (EventHelper.IsProfessionalSkillUnlocked(40) && DomainManager.Extra.GetProfessionData(12)?.SkillsData is not null and GameData.Domains.Taiwu.Profession.SkillsData.TravelingBuddhistMonkSkillsData tbm) {
            return target-tbm.GetVisitedTempleCount();
        }
        return -1;
    }
    [DisplayName("Location_Array_获取寺庙位置")]
    public static GameData.Domains.Map.Location[] GetTemples(){
        var pd=DomainManager.Extra.GetProfessionData(12);
        if (pd?.SkillsData is not null and GameData.Domains.Taiwu.Profession.SkillsData.TravelingBuddhistMonkSkillsData tbm) {
            return Enumerable.Range(0, 15).Where(x=>!tbm.IsStateTempleVisited((sbyte)x)).Select(x=>tbm.GetStateTempleLocation((sbyte)x)).ToArray();
        }
        return new GameData.Domains.Map.Location[0];
    }
    [DisplayName("string_获取位置数组1的具体名称")]
    public static string GetTemples(IEnumerable<GameData.Domains.Map.Location> GetTemples)=>string.Join('、', GetTemples.Select(GetLocationName));
    [DisplayName("string_获取位置1的具体名称")]
    public static string GetLocationName(GameData.Domains.Map.Location x){
        var (sname,aname)=DomainManager.Map.GetStateAndAreaNameByAreaId(x.AreaId);
        if(DomainManager.Organization.GetSettlementByLocation(x) is not null and GameData.Domains.Organization.Settlement settlement){
            var randomNameId = (short)((settlement is GameData.Domains.Organization.CivilianSettlement cs) ? cs.GetRandomNameId() : (-1));
            var block = DomainManager.Map.GetBlock(settlement.GetLocation()).GetRootBlock();
            string settlementName = ((randomNameId != -1) ? Config.LocalTownNames.Instance.TownNameCore[randomNameId].Name : ((block.TemplateId != -1) ? Config.MapBlock.Instance[block.TemplateId].Name : Config.Organization.Instance[(sbyte)0].Name));
            return $"<color=#GradeColor_7>{sname}-{aname}-{settlementName}</color>";
        } else {
            return $"<color=#GradeColor_7>{sname}-{aname}-{DomainManager.Map.GetBlock(x).GetConfig().Name}</color>";
        }
    }
    [DisplayName("void_茶马帮满补给")]
    public static void 茶马帮满补给(){
        if(check(_teaHorseCaravanData.enable)){
            var thcd=_teaHorseCaravanData.get(DomainManager.Building);
            thcd.CaravanReplenishment = 100;
            _teaHorseCaravanData.set(DomainManager.Building,thcd,context);
        }
    }

    [DisplayName("void_远程收取茶马帮物品")]
    public static void 远程收取茶马帮物品(){
        if(check(_teaHorseCaravanData.enable)){
            var thcd=_teaHorseCaravanData.get(DomainManager.Building);
            if (thcd != null) {
                foreach(var itemKey in thcd.ExchangeGoodsList){
                    DomainManager.Taiwu.CreateWarehouseItem(context, itemKey.ItemType, itemKey.TemplateId, 1);
                }
                thcd.ExchangeGoodsList.Clear();
                _teaHorseCaravanData.set(DomainManager.Building,thcd,context);
            }
        }
    }

    [DisplayName("bool_有元鸡未捕捉")]
    public static bool 有元鸡未捕捉()=>获取最高级元鸡位置与tid().Length>0;
    [DisplayName("Location_tid_Array_获取最高级元鸡位置与tid")]
    public static (GameData.Domains.Map.Location, short)[] 获取最高级元鸡位置与tid(){
        if(check(_chicken.enable)){
            var twid=DomainManager.Taiwu.GetTaiwuVillageSettlementId();
            var chicken=default(GameData.Domains.Building.Chicken);
            chicken.CurrentSettlementId=twid;
            return _chicken.get(DomainManager.Building).Values
            .GroupBy(x=>Config.Chicken.Instance[x.TemplateId].PersonalityType,(key,x)=>
                x.Where(x=>x.CurrentSettlementId!=twid)
                .OrderBy(x=>Config.Chicken.Instance[x.TemplateId].Grade)
                .LastOrDefault(chicken)
            )
            .Where(x=>x.CurrentSettlementId!=twid)
            .Select(x=>(DomainManager.Organization.GetSettlement((short)x.CurrentSettlementId).GetLocation(), x.TemplateId))
            .ToArray();
        } else {
            return new(GameData.Domains.Map.Location, short)[0];
        }
    }
    static readonly string[] chicken_bless_pid = new string[]{"冷静", "聪颖", "热情", "勇壮", "坚毅", "福缘", "合道"};
    [DisplayName("string_解析元鸡位置与tid")]
    public static string 解析元鸡位置与种类(IEnumerable<(GameData.Domains.Map.Location, short)> res)=>string.Join('\n',res.Select(res=>$"<color=#GradeColor_{Config.Chicken.Instance[res.Item2].Grade}>{chicken_bless_pid[Config.Chicken.Instance[res.Item2].PersonalityType]}</color>鸡在{GetLocationName(res.Item1)}"));
    [DisplayName("void_设置太吾村元鸡好感度为1")]
    public static void 设置太吾村元鸡好感度为1(sbyte val){
        if(check(_chicken.enable)){
            var twid=DomainManager.Taiwu.GetTaiwuVillageSettlementId();
            var ch=_chicken.get(DomainManager.Building).Values.Where(x=>x.CurrentSettlementId==twid).Select(x=>x.Id).ToArray();
            foreach(var cid in ch){
                DomainManager.Building.SetChickenHappiness(context, cid, val);
            }
        }
    }
    [DisplayName("void_设置物品列表1中工具耐久为满值")]
    public static void 设置物品列表1中工具耐久为满值(IEnumerable<GameData.Domains.Item.ItemKey> Keys){
        var wikd=Keys.Where(x=>x.ItemType==6 && Config.CraftTool.Instance[x.TemplateId].MaxDurability>0).ToArray();
        foreach(var x in wikd){
            if(DomainManager.Item.GetBaseItem(x) is GameData.Domains.Item.CraftTool ib){
                var maxdur=Config.CraftTool.Instance[x.TemplateId].MaxDurability;
                ib.SetMaxDurability(maxdur, context);
                ib.SetCurrDurability(maxdur, context);
            }
        }
    }
    [DisplayName("void_量子速读物品列表1中所有书")]
    public static void 量子速读物品列表1中所有书(IEnumerable<GameData.Domains.Item.ItemKey> Keys){
        var wikd=Keys.Where(x=>x.ItemType==10).ToArray();
        foreach(var x in wikd){
            if(DomainManager.Item.GetBaseItem(x) is GameData.Domains.Item.SkillBook book){
                量子速读(book);
            }
        }
    }

    [DisplayName("void_设置物品列表1中装备耐久为满值")]
    public static void 设置物品列表1中装备耐久为满值(IEnumerable<GameData.Domains.Item.ItemKey> Keys){
        foreach(var x in Keys.Where(x=>((1<<x.ItemType)&0b1_0_111)>0)){
            var ib = DomainManager.Item.GetBaseItem(x);
            short maxdur=0;
            switch(x.ItemType){
                case 4:if(ib is GameData.Domains.Item.Carrier){maxdur=Config.Carrier.Instance[x.TemplateId].MaxDurability;}break;
                case 3:break;
                case 2:if(ib is GameData.Domains.Item.Accessory){maxdur=Config.Accessory.Instance[x.TemplateId].MaxDurability;}break;
                case 1:if(ib is GameData.Domains.Item.Armor){maxdur=Config.Armor.Instance[x.TemplateId].MaxDurability;}break;
                case 0:if(ib is GameData.Domains.Item.Weapon){maxdur=Config.Weapon.Instance[x.TemplateId].MaxDurability;}break;
                default:break;
            }
            if(ib is GameData.Domains.Item.EquipmentBase eb && Config.EquipmentEffect.Instance.GetItem(eb.GetEquipmentEffectId()) is Config.EquipmentEffectItem cei and not null){
                maxdur=(short)(maxdur*(100+cei.MaxDurabilityChange)/100);
            }
            var dur=ib.GetMaxDurability();
            if(maxdur>dur){
                ib.SetMaxDurability(maxdur, context);
                ib.SetCurrDurability(maxdur, context);
            } else if (dur>0) {
                ib.SetCurrDurability(dur, context);
            }
        }
    }


    // [DisplayName("void_进化物品列表1中神一品装备为对应的传家宝神一")]
    // public static void 设置物品列表1中装备耐久为满值(IEnumerable<GameData.Domains.Item.ItemKey> Keys){
    //     foreach(var x in Keys.Where(x=>x.ItemType==2 && Config.Accessory.Instance[x.TemplateId].Grade==8){
    //         var ib = DomainManager.Item.GetBaseItem(x);
    //         if(ib is GameData.Domains.Item.Accessory){
    //             maxdur=Config.Accessory.Instance[x.TemplateId].Grade;
    //         } else {
    //             logwarn($"{x} 不是饰品");
    //         }
    //     }
    // }

    [DisplayName("ItemKey_Array_获取太吾装备")]
    public static GameData.Domains.Item.ItemKey[] 获取太吾装备物品()=>DomainManager.Taiwu.GetTaiwu().GetEquipment();
    [DisplayName("ItemKey_Array_获取太吾携带物品")]
    public static GameData.Domains.Item.ItemKey[] 获取太吾携带物品()=>DomainManager.Taiwu.GetTaiwu().GetInventory().Items.Keys.ToArray();
    [DisplayName("ItemKey_Array_获取太吾仓库物品")]
    public static GameData.Domains.Item.ItemKey[] 获取太吾仓库物品()=>DomainManager.Taiwu.WarehouseItems.Keys.ToArray();
    [DisplayName("ItemKey_Array_获取太吾饲槽物品")]
    public static GameData.Domains.Item.ItemKey[] 获取太吾饲槽物品()=>DomainManager.Extra.TroughItems.Keys.ToArray();
    [DisplayName("ItemKey_Array_获取太吾公库物品")] // 被标记弃用
    public static GameData.Domains.Item.ItemKey[] 获取太吾公库物品(){
        var arr = DomainManager.Extra.GetStockStorage()?.Inventories;
        if ((arr?.Length??0)>0) {
            return arr[0].Items.Keys.ToArray();
        } else {
            return new GameData.Domains.Item.ItemKey[0];
        }
    }
    [DisplayName("void_补满人物1内功2内力")]
    public static void 补满人物1内功2内力(GameData.Domains.Character.Character self, short TemplateId){
        if(TemplateId>-1){
            int[] il;
            if(DomainManager.Taiwu.GetTaiwuCharId() == self.GetId()){
                var z=100 * GlobalConfig.Instance.ExtraNeiliAllocationFromProgressRatio * GlobalConfig.Instance.MaxExtraNeiliAllocation;
                il = new int[]{z,z,z,z};
            } else if(DomainManager.Extra.TryGetExtraNeiliAllocationProgress(self.GetId(), out var result)){
                il=result.Items.ToArray();
            } else {
                il=new int[]{0,0,0,0};
            }
            // {
            //     DomainManager.Extra.TryGetExtraNeiliAllocationProgress(self.GetId(), out var result);
            //     logwarn($"got char alloc = {result.Items[0]} {result.Items[1]} {result.Items[2]} {result.Items[3]} il = {il[0]} {il[1]} {il[2]} {il[3]}");
            // }
            DomainManager.CombatSkill.ApplyNeigongLoopingEffect(context, self, TemplateId, 10000, il);
        }
    }
    [DisplayName("void_将人物1的额外真气进度设置为2点")]
    public static void 将人物1的额外真气进度设置为2点(GameData.Domains.Character.Character self, int val) {
        if(val > 0 && val < 10000) {
            // curr val
            var nl = default(GameData.Domains.Character.NeiliAllocation);
            unsafe {
                nl.Items[0]=nl.Items[1]=nl.Items[2]=nl.Items[3]=(short)val;
            }
            // curr progress
            val = GameData.Domains.CombatSkill.CombatSkillDomain.GetExtraNeiliAllocationProgressByExtraNeiliAllocation(val)-1;
            if(DomainManager.Extra.TryGetExtraNeiliAllocationProgress(self.GetId(), out var il)) {
                for(int i=0;i<4;i++) {
                    il.Items[i] = val;
                }
            } else {
                il = GameData.Utilities.IntList.Create();
                il.Items.Add(val);
                il.Items.Add(val);
                il.Items.Add(val);
                il.Items.Add(val);
            }
            DomainManager.Extra.SetExtraNeiliAllocationProgress(context, self.GetId(), il);
            self.SetExtraNeiliAllocation(nl, context);
        } else {
            logwarn($"参数2 = {val} 不在区间(0,10000)之间，请检查参数调用是否正确。");
        }
    }
    [DisplayName("short_获取人物1运转内功")]
    public static short 获取人物1运转内功(GameData.Domains.Character.Character self)=>self.GetLoopingNeigong();

    [DisplayName("void_将全部志向升到满级")]
    public static void 将全部志向升到满级(){
        // if(check(_taiwuCurrProfessionId.enable)){
        //     DomainManager.Extra.ChangeProfessionSeniority(context, _taiwuCurrProfessionId.get(DomainManager.Extra), 3000000, false, false);
        // }
        Config.Profession.Instance.Iterate((x)=>{if(DomainManager.Extra.TryGetElement_TaiwuProfessions(x.TemplateId, out var professionData) && professionData.Seniority < 3000000)DomainManager.Extra.ChangeProfessionSeniority(context, x.TemplateId, 3000000 - professionData.Seniority, false, false); return true;});
    }
    [DisplayName("void_取消志向CD")]
    public static void 取消志向CD() {
        DomainManager.Extra.NoProfessionChangeCoolDown=true;
        DomainManager.Extra.NoProfessionSkillCooldown=true;
    }
    [DisplayName("void_触发全部失效志向事件")]
    public static void 触发全部失效志向事件() {
        for(int i=0;i<42;i++) {
            if(i==37){i+=2;}// 跳过37与38
            if(!EventHelper.IsOneShotEventHandled(i)){EventHelper.SetOneShotEventHandled(i);}
        }
    }
    [DisplayName("string_检查书本1阅读情况")]
    public static string 检查书本1阅读情况(GameData.Domains.Item.ItemKey book){
        if(!book.IsValid()){
            return "\n<color=#lightgrey>……手里的书已经被啃干净了……</color>";
        }
        if(book.ItemType != 10){
            var x=$"string_检查书本1阅读情况 收到了一个不是书的ItemKey: book.ItemType = {book.ItemType}";
            logwarn(x);
            return x;
        } else {
            var bookItem = DomainManager.Item.GetElement_SkillBooks(book.Id);
            byte readingPage=0;
            sbyte readingProgress=0;
            var strategies=DomainManager.Taiwu.GetCurReadingStrategies();
            int speed=0;
            if (GameData.Domains.Character.SkillGroup.FromItemSubType(bookItem.GetItemSubType()) == 0) {
                if(DomainManager.Taiwu.TryGetTaiwuLifeSkill(bookItem.GetLifeSkillTemplateId(), out var skill)
                || DomainManager.Taiwu.TryGetNotLearnLifeSkillReadingProgress(bookItem.GetLifeSkillTemplateId(), out skill)
                ){
                    readingPage = DomainManager.Taiwu.GetCurrentReadingPage(bookItem, strategies, skill);
                    readingProgress = readingPage==5?(sbyte)125:skill.GetBookPageReadingProgress(readingPage);
                    speed = readingPage==5?0:DomainManager.Taiwu.GetBaseReadingSpeed(readingPage) * DomainManager.Taiwu.GetReadingSpeedBonus(readingPage, false) /100;
                }
            } else {
                if(DomainManager.Taiwu.TryGetTaiwuCombatSkill(bookItem.GetCombatSkillTemplateId(), out var skill)
                || DomainManager.Taiwu.TryGetNotLearnCombatSkillReadingProgress(bookItem.GetLifeSkillTemplateId(), out skill)
                ){
                    readingPage = DomainManager.Taiwu.GetCurrentReadingPage(bookItem, strategies, skill);
                    readingProgress = readingPage==6
                        ? (sbyte)125
                        : skill.GetBookPageReadingProgress(
                            GameData.Domains.CombatSkill.CombatSkillStateHelper.GetPageInternalIndex(
                                GameData.Domains.Item.SkillBookStateHelper.GetOutlinePageType(bookItem.GetPageTypes()),
                                GameData.Domains.Item.SkillBookStateHelper.GetNormalPageType(bookItem.GetPageTypes(), readingPage),
                                readingPage
                            )
                        );
                    speed = readingPage==6?0:DomainManager.Taiwu.GetBaseReadingSpeed(readingPage) * DomainManager.Taiwu.GetReadingSpeedBonus(readingPage, false) /100;
                }
            }
            return $"<color=#GradeColor_{Config.SkillBook.Instance[book.TemplateId].Grade}>{Config.SkillBook.Instance[book.TemplateId].Name}</color>已{(readingProgress==125?"读完":$"读至第{readingPage+1}页，进度为{readingProgress}%\n当前吃书速度约为{speed}%，书籍剩余耐久{bookItem.GetCurrDurability()}")}";
        }
    }
    [DisplayName("string_参考书状态")]
    public static string 参考书状态()=>string.Join('\n',DomainManager.Taiwu.GetReferenceBooks().Select(book=>book.IsValid()?$"<color=#GradeColor_{Config.SkillBook.Instance[book.TemplateId].Grade}>{Config.SkillBook.Instance[book.TemplateId].Name}</color>({DomainManager.Item.GetElement_SkillBooks(book.Id).GetCurrDurability()}/{DomainManager.Item.GetElement_SkillBooks(book.Id).GetMaxDurability()})":"<color=#GradeColor_0>无参考</color>"));
    [DisplayName("void_量子速读")]
    public static void 量子速读(GameData.Domains.Item.SkillBook book) {
        short skillTemplateId=book.GetLifeSkillTemplateId();
        if(skillTemplateId>-1 && check(QRead.enable)){
            var skill = QRead.getl(DomainManager.Taiwu, skillTemplateId);
            for(byte i=0;i<5;i++){
                var flag = QRead.add(DomainManager.Taiwu, skill,i,100);
                QRead.setl(DomainManager.Taiwu, context, skillTemplateId, skill);
                if(flag){
                    QRead.finl(DomainManager.Taiwu, context, book,i);
                }
            }
        }
        skillTemplateId=book.GetCombatSkillTemplateId();
        if(skillTemplateId>-1 && check(QRead.enable)){
            var skill = QRead.getc(DomainManager.Taiwu, skillTemplateId);
            var skillKey = new GameData.Domains.CombatSkill.CombatSkillKey(GameData.Domains.DomainManager.Taiwu.GetTaiwuCharId(), skillTemplateId);
            for(byte i=0;i<15;i++){
                var flag = QRead.add(DomainManager.Taiwu, skill,i,100);
                QRead.setc(DomainManager.Taiwu, context, skillTemplateId, skill);
                if(flag){
                    QRead.finc(DomainManager.Taiwu, context, book,i);
                }
            }
            if(GameData.Domains.DomainManager.CombatSkill.TryGetElement_CombatSkills(skillKey, out var s) && s.GetActivationState()==0 && s.GetPracticeLevel()<100){
                s.SetPracticeLevel(100,context);
            }
        }
    }

    [DisplayName("void_蛐蛐1进化")]
    public static void 蛐蛐1进化(GameData.Domains.Item.ItemKey ququKey){
        // if(!check(SetModifiedAndInvalidateInfluencedCache.enable)){return;}
        if(DomainManager.Item.GetBaseItem(ququKey) is GameData.Domains.Item.Cricket ququ){
            ququ.SetInjuries(new short[]{-800, -800, -800, -800, -800}, context);
        } else {
            logwarn($"ququKey {ququKey} 不是蛐蛐 (ququKey.ItemType={ququKey.ItemType})");
            return;
        }
    }
    [DisplayName("void_蛐蛐1万胜")]
    public static void 蛐蛐1万胜(GameData.Domains.Item.ItemKey ququKey){
        if(DomainManager.Item.GetBaseItem(ququKey) is GameData.Domains.Item.Cricket ququ){
            ququ.SetWinsCount(10000, context);
        } else {
            logwarn($"ququKey {ququKey} 不是蛐蛐 (ququKey.ItemType={ququKey.ItemType})");
            return;
        }
    }
    [DisplayName("void_触发灵光一闪")]
    public static void 触发灵光一闪(){
        var _curReadingBook=DomainManager.Taiwu.GetCurReadingBook();
        if(_curReadingBook.IsValid()){
            DomainManager.World.GetMonthlyNotificationCollection().AddReadingEvent((ulong)_curReadingBook);
            DomainManager.Extra.AddReadingEventBookId(context, _curReadingBook.Id);
        }
    }
    static readonly ushort[] del_type = new ushort[]{GameData.Domains.Character.Relation.RelationType.Enemy , GameData.Domains.Character.Relation.RelationType.Adored , GameData.Domains.Character.Relation.RelationType.Friend , GameData.Domains.Character.Relation.RelationType.SwornBrotherOrSister};
    [DisplayName("void_id1与id2一笔勾销")]
    public static void id1与id2一笔勾销(int selfid, int cid){
        foreach (var type in del_type) {
            if(DomainManager.Character.HasRelation(selfid, cid, type)){
                DomainManager.Character.ChangeRelationType(context, selfid, cid, type, 0);
            }
            if(DomainManager.Character.HasRelation(cid, selfid, type)){
                DomainManager.Character.ChangeRelationType(context, cid, selfid, type, 0);
            }
        }
    }
    [DisplayName("ushort_敌对关系")]
    public static ushort 敌对关系()=>GameData.Domains.Character.Relation.RelationType.Enemy;
    [DisplayName("void_id1与id2有关系3")]
    public static bool id1与id2有关系(int selfid, int cid, ushort rel_type)=>DomainManager.Character.HasRelation(selfid, cid, rel_type) || DomainManager.Character.HasRelation(cid, selfid, rel_type);
    // [DisplayName("void_以token1与argbox2触发相枢化身3的战斗")]
    // public static void 以token1与argbox2触发相枢化身3的战斗(string token, GameData.Domains.TaiwuEvent.EventArgBox argBox, short templateId){
    //     Character character = ArgBox.GetCharacter("OutSwordCharId"); // ?
    // short xiangshuTemplateId = XiangshuAvatarIds.GetCurrentLevelXiangshuTemplateId(xiangshuAvatarId, xiangshuLevel, isWeakened: true);
    // GameData.Domains.Character.Character xiangshuCharacter = GetOrCreateFixedCharacterByTemplateId(xiangshuTemplateId); // ?
    //     int id = character.GetId();
    //     short templateId = character.GetTemplateId();
    //     switch (XiangshuAvatarIds.GetXiangshuAvatarIdByCharacterTemplateId(templateId))
    //     {
    //         case 0:
    //             EventHelper.StartCombat(id, 45, "0f3b9dce-be7c-41e0-b5e3-c5d5120a5be2", ArgBox, noGuard: true);
    //             break;
    //         case 1:
    //             EventHelper.StartCombat(id, 46, "0f3b9dce-be7c-41e0-b5e3-c5d5120a5be2", ArgBox, noGuard: true);
    //             break;
    //         case 2:
    //             EventHelper.StartCombat(id, 47, "0f3b9dce-be7c-41e0-b5e3-c5d5120a5be2", ArgBox, noGuard: true);
    //             break;
    //         case 3:
    //             EventHelper.StartCombat(id, 48, "0f3b9dce-be7c-41e0-b5e3-c5d5120a5be2", ArgBox, noGuard: true);
    //             break;
    //         case 4:
    //             EventHelper.StartCombat(id, 49, "0f3b9dce-be7c-41e0-b5e3-c5d5120a5be2", ArgBox, noGuard: true);
    //             break;
    //         case 5:
    //             EventHelper.StartCombat(id, 50, "0f3b9dce-be7c-41e0-b5e3-c5d5120a5be2", ArgBox, noGuard: true);
    //             break;
    //         case 6:
    //             EventHelper.StartCombat(id, 51, "0f3b9dce-be7c-41e0-b5e3-c5d5120a5be2", ArgBox, noGuard: true);
    //             break;
    //         case 7:
    //             EventHelper.StartCombat(id, 52, "0f3b9dce-be7c-41e0-b5e3-c5d5120a5be2", ArgBox, noGuard: true);
    //             break;
    //         case 8:
    //             EventHelper.StartCombat(id, 53, "0f3b9dce-be7c-41e0-b5e3-c5d5120a5be2", ArgBox, noGuard: true);
    //             break;
    //     }
    // }
    [DisplayName("void_打开奇书突破栏")]
    public static void 打开奇书突破栏(){
        for(sbyte i=0;i<14;i++){
            DomainManager.Extra.UnlockLegendaryBookBreakPlate(context, i, true);
        }
    }
    // public static bool slot_is_empty(short skillId, int slot){
    //     if(!check(_combatSkillBreakPlateList.enable)){
    //         return false;
    //     }
    //     if(_combatSkillBreakPlateList.get(DomainManager.Extra).TryGetValue(skillId, out var lst)){
    //         return lst.Items[slot] is null || !lst.Items[slot].Finished;
    //     }
    //     if(check(CombatSkill.enable_add)){
    //         var newPlateList = GameData.Domains.Taiwu.SkillBreakPlateList.Create();
    //         var newClearTimeList = GameData.Utilities.IntList.Create();
    //         var newForceStepsList = GameData.Utilities.IntList.Create();
    //         for (int i = 0; i < 3; i++){
    //             newPlateList.Items.Add(null);
    //             newClearTimeList.Items.Add(-1000);
    //             newForceStepsList.Items.Add(-1);
    //         }
    //         CombatSkill.AddElement_CombatSkillCurrBreakPlateIndex(DomainManager.Extra, skillId, 0, context);
    //         CombatSkill.AddElement_CombatSkillBreakPlateList(DomainManager.Extra, skillId, newPlateList, context);
    //         CombatSkill.AddElement_CombatSkillBreakPlateLastClearTimeList(DomainManager.Extra, skillId, newClearTimeList, context);
    //         CombatSkill.AddElement_CombatSkillBreakPlateLastForceBreakoutStepsCount(DomainManager.Extra, skillId, newForceStepsList, context);
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

    public static GameData.Domains.Taiwu.SkillBreakPlate get_slot(short skillId, int slot){
        // logwarn($"skillId={skillId}slot={slot}");
        if(!check(_combatSkillBreakPlateList.enable)){
            return null;
        }
        if(_combatSkillBreakPlateList.get(DomainManager.Extra).TryGetValue(skillId, out var newPlateList)){
            return newPlateList.Items[slot];
        }
        if(check(CombatSkill.enable_add)){
            newPlateList = GameData.Domains.Taiwu.SkillBreakPlateList.Create();
            var newClearTimeList = GameData.Utilities.IntList.Create();
            var newForceStepsList = GameData.Utilities.IntList.Create();
            for (int i = 0; i < 3; i++){
                newPlateList.Items.Add(null);
                newClearTimeList.Items.Add(-1000);
                newForceStepsList.Items.Add(-1);
            }
            CombatSkill.AddElement_CombatSkillCurrBreakPlateIndex(DomainManager.Extra, skillId, 0, context);
            CombatSkill.AddElement_CombatSkillBreakPlateList(DomainManager.Extra, skillId, newPlateList, context);
            CombatSkill.AddElement_CombatSkillBreakPlateLastClearTimeList(DomainManager.Extra, skillId, newClearTimeList, context);
            CombatSkill.AddElement_CombatSkillBreakPlateLastForceBreakoutStepsCount(DomainManager.Extra, skillId, newForceStepsList, context);
            return newPlateList.Items[slot]; // null
        } else {
            return null;
        }
    }
    public static void set_slot(short skillId, int slot, GameData.Domains.Taiwu.SkillBreakPlate plate){
        // SAFETY: MUST CALLED AFTER slot_is_empty(skillId, slot)/get_slot
        if(check(CombatSkill.enable_get_set)){
            var newPlateList = CombatSkill.GetElement_CombatSkillBreakPlateList(DomainManager.Extra, skillId);
            var newClearTimeList = CombatSkill.GetElement_CombatSkillBreakPlateLastClearTimeList(DomainManager.Extra, skillId);
            var newForceStepsList = CombatSkill.GetElement_CombatSkillBreakPlateLastForceBreakoutStepsCount(DomainManager.Extra, skillId);

            newPlateList.Items[slot]=plate;
            newClearTimeList.Items[slot]=-1000;

            var forceBreakoutSteps = Math.Max(0,plate.CostedStepCount - plate.TotalStepCount);
            newForceStepsList.Items[slot]=forceBreakoutSteps;

            CombatSkill.SetElement_CombatSkillBreakPlateList(DomainManager.Extra, skillId, newPlateList, context);
            CombatSkill.SetElement_CombatSkillBreakPlateLastClearTimeList(DomainManager.Extra, skillId, newClearTimeList, context);
            CombatSkill.SetElement_CombatSkillBreakPlateLastForceBreakoutStepsCount(DomainManager.Extra, skillId, newForceStepsList, context);
        }
    }
    public static bool is_curr_slot(short skillId, int slot){
        if(!check(_combatSkillCurrBreakPlateIndex.enable)){
            return false;
        }
        _combatSkillCurrBreakPlateIndex.get(DomainManager.Extra).TryGetValue(skillId, out var idx); // 拿不到的时候idx会被置0，正好~
        return idx==slot;
    }
    public static GameData.Domains.Taiwu.SkillBreakPlate new_skill_break_plate(short skillTemplateId, ushort selectedPages){
        if(check(InitSkillBreakPlate.enable)){
            var plate = new GameData.Domains.Taiwu.SkillBreakPlate(selectedPages);
            InitSkillBreakPlate.invoke(DomainManager.Taiwu, context, skillTemplateId, plate);
            plate.CurrentPoint = plate.StartPoint;
            plate.Grids[plate.CurrentPoint.Item1][plate.CurrentPoint.Item2].State = 2;
            if (plate.ExtraPointType == 1) {
                plate.ExtraCurrentPoint = plate.ExtraPoint;
                plate.Grids[plate.ExtraPoint.Item1][plate.ExtraPoint.Item2].State = 2;
            }
            return plate;
        } else {
            return null;
        }
    }
    public static void all_yellow_break(GameData.Domains.Taiwu.SkillBreakPlate plate){
        for(int row=0;row<plate.Grids.Length;row++){
            for(int col=0;col<plate.Grids[row].Length-(1&~row);col++){
                if(plate.Grids[row][col].BonusType >= 0){
                    plate.Grids[row][col].State=2;
                }
            }
        }
        if((plate.SelectedPages & (1 << 8)) == 0 ){
            plate.CostedStepCount = 100;// 入魔加威力
        } else {
            plate.CostedStepCount = 1;// 不入魔加威力
        }
        plate.Finished=true;
    }
    [DisplayName("void_按指令1自动突破功法")]
    public static void 自动突破(string 指令){
        foreach(var sp in 指令.Split(",", StringSplitOptions.RemoveEmptyEntries)){
            short SkillTemplateId=-1; //日后再写parse
            var GradeMin=0;
            var GradeMax=8;
            var EquipType=31;// 1 | 2 | 4 | 8 | 16 = 内功 催破/摧破 轻灵 护体 奇窍
            ushort state=996;
            var jie=false;
            var maxout=false;
            sbyte inner=-1;
            sbyte slot=0;
            foreach(var sub in sp.Split("&", StringSplitOptions.RemoveEmptyEntries)){
                var item=sub.Split("=", StringSplitOptions.RemoveEmptyEntries);
                if(item.Length<2){
                    logwarn($"在指令 `{指令}` 的 `{sp}` 部分中，针对 `{sub}` 的处理出错：此部份无法被分解为`A=B`的形式，因此忽略 `{sub}`");
                    continue;
                }
                var key=item[0];
                var value=item[1];
                switch(key){
                    case "功法":
                    case "功法类型":
                        EquipType=0;
                        if(value.Contains("内功")){EquipType|=1;}
                        if(value.Contains("摧破")){EquipType|=2;}
                        if(value.Contains("催破")){EquipType|=2;}
                        if(value.Contains("身法")){EquipType|=4;}
                        if(value.Contains("轻灵")){EquipType|=4;}
                        if(value.Contains("护体")){EquipType|=8;}
                        if(value.Contains("奇窍")){EquipType|=16;}
                        if(EquipType==0){
                            logwarn("请至少填写 内功/摧破/身法/护体/奇窍 之一。由于 `{sub}` 找不到合适的功法类型，指令的 `{sp}` 部分被忽略");
                        }
                        break;
                    case "品级":
                    case "功法品级":
                        var values=value.Split("-");
                        if (values.Length>1){
                            if(int.TryParse(values[1], out GradeMax) && int.TryParse(values[0], out GradeMin)) {
                                //
                            } else {
                                logwarn($"{value} 不能被识别成min-max的形状");
                            }
                        } else {
                            if(int.TryParse(values[1], out GradeMax)){
                                GradeMin=GradeMax;
                            } else {
                                logwarn($"{value} 不能被识别成功法品级");
                            }
                        }
                        break;
                    case "精解":
                    case "精解状态":
                        jie=value.ToLower()=="true" || value == "是";
                        break;
                    case "内功比例":
                    case "提气比例":
                        if(!sbyte.TryParse(value, out inner)){
                            inner=-1;
                            logwarn($"{value}并不是一个合适的内功比例");
                        }
                        break;
                    case "突破槽":
                        if(!sbyte.TryParse(value, out slot)){
                            slot=0;
                            logwarn($"{value}并不是一个合适的突破槽数字");
                        } else {
                            if(slot>2 || slot<0) {
                                logwarn($"{slot}需要介于0-2之间");
                                slot=0;
                            }
                        }
                        break;
                    case "书页":
                        state=0;
                        unchecked {
                            if(value.Contains("承")){state&=(ushort)~31;state|=1;}
                            if(value.Contains("合")){state&=(ushort)~31;state|=2;}
                            if(value.Contains("解")){state&=(ushort)~31;state|=4;}
                            if(value.Contains("异")){state&=(ushort)~31;state|=8;}
                            if(value.Contains("独")){state&=(ushort)~31;state|=16;}
                            if(value.Contains("修")){state|=32;state&=(ushort)~1024;}
                            if(value.Contains("思")){state|=64;state&=(ushort)~2048;}
                            if(value.Contains("源")){state|=128;state&=(ushort)~4096;}
                            if(value.Contains("参")){state|=256;state&=(ushort)~8192;}
                            if(value.Contains("藏")){state|=512;state&=(ushort)~16384;}
                            if(value.Contains("用")){state|=1024;state&=(ushort)~32;}
                            if(value.Contains("奇")){state|=2048;state&=(ushort)~64;}
                            if(value.Contains("巧")){state|=4096;state&=(ushort)~128;}
                            if(value.Contains("化")){state|=8192;state&=(ushort)~256;}
                            if(value.Contains("绝")){state|=16384;state&=(ushort)~512;}
                        }
                        if(System.Numerics.BitOperations.PopCount(state)!=6 || System.Numerics.BitOperations.PopCount((uint)(state&31))!=1 || ((state | (state >> 5)) & 0b1111100000) != 0b1111100000){
                            logwarn($"书页字符串{value}并不能被识别为总纲+5书页，使用默认的state=996进行突破\n书页的话建议6字，1总纲+5其他书页。中子曾经作死地尝试过「承源巧参藏绝」/「承源巧化藏绝」这一设置……后端炸得很安详");
                            state=996;
                        }
                        break;
                    case "强制突破":
                        maxout=true;
                        break;
                    default:
                        logwarn($"不支持以{key}作为左值，目前仅支持以 功法/功法类型（过滤用）, 内功比例/提气比例（将突破结果设置为此数值）, 精解/精解状态（取值：是/否/true/false）, 书页（） 为左值");
                        break;
                }
            }
            foreach(var skillTemplateId in DomainManager.CombatSkill.GetCharCombatSkills(DomainManager.Taiwu.GetTaiwuCharId()).Keys){
                var skillKey = new GameData.Domains.CombatSkill.CombatSkillKey(DomainManager.Taiwu.GetTaiwuCharId(), skillTemplateId);
                // {
                //     var flag = DomainManager.CombatSkill.TryGetElement_CombatSkills(skillKey, out var skill) && !(get_slot(skillTemplateId, slot)?.Finished??false);
                //     var _readingState=skill.GetReadingState();
                //     logger($"try breakout {skillTemplateId}, flag = {flag}, skill is null: {skill is null}, !(get_slot(skillTemplateId, slot)?.Finished??false) = {!(get_slot(skillTemplateId, slot)?.Finished??false)}, _readingState = {skill.GetReadingState()}, HasReadOutlinePages = {GameData.Domains.CombatSkill.CombatSkillStateHelper.HasReadOutlinePages(_readingState)}, DomainManager.Taiwu.CanBreakOut(skillTemplateId) = {DomainManager.Taiwu.CanBreakOut(skillTemplateId)}");
                //
                // }
                if(DomainManager.CombatSkill.TryGetElement_CombatSkills(skillKey, out var skill) && !(get_slot(skillTemplateId, slot)?.Finished??false)){
                    var _readingState=skill.GetReadingState();
                    var configData=Config.CombatSkill.Instance[skillTemplateId];
                    if((EquipType&(1<<configData.EquipType))==0){
                        continue; // 功法类型
                    }
                    if(configData.Grade>GradeMax || configData.Grade<GradeMin){
                        continue; // 功法品级
                    }
                    if(skill.GetPracticeLevel()<100){
                        if(maxout){
                            skill.SetPracticeLevel(100,context);
                        } else {
                            continue;
                        }
                    }
                    ushort outline=state;
                    if(GameData.Domains.CombatSkill.CombatSkillStateHelper.HasReadOutlinePages(_readingState) && skill.CanBreakout() ){
                        // logwarn($"break out {skillTemplateId}");
                        if(skill.GetRevoked())skill.SetRevoked(false,context);
                        if(inner!=-1 && skill.GetInnerRatio()!=inner){
                            skill.SetInnerRatio(inner,context);
                        }
                        // if(DomainManager.Extra.TryGetElement_CombatSkillCurrBreakPlateIndex(skillTemplateId, out var x) && x!=0)
                        if((_readingState & outline)==outline){
                            var plate = get_slot(skillTemplateId, slot);
                            if(plate is null || plate.Finished==false){
                                // logwarn($"curr status: skillTemplateId {skillTemplateId} with slot {slot}");
                                if(is_curr_slot(skillTemplateId, slot)){
                                    // logwarn($"  entered skillTemplateId {skillTemplateId} is_curr_slot with slot {slot}");
                                    if(GameData.Domains.CombatSkill.CombatSkillStateHelper.IsBrokenOut(skill.GetActivationState())) {
                                        continue;
                                    } else {
                                        if(plate is null){
                                            plate=DomainManager.Taiwu.EnterSkillBreakPlate(context, skillTemplateId, outline);
                                        }
                                        set_skill_break_plate(skillTemplateId, plate);
                                        set_slot(skillTemplateId, slot, plate);
                                    }
                                } else {
                                    if(plate is null){
                                        plate=new_skill_break_plate(skillTemplateId, outline);
                                    }
                                    if(plate is not null){
                                        // logwarn($"  entered skillTemplateId {skillTemplateId} all_yellow_break with slot {slot}");
                                        all_yellow_break(plate);
                                        set_slot(skillTemplateId, slot, plate);
                                    }
                                }
                            }
                            if(jie && ((int)configData.Grade)>2){
                                var curr=DomainManager.Taiwu.GetCurrCombatSkillPlanId();
                                if(check(MasteredCombatSkillPlans.enable)){
                                    for(int plan=0;plan<9;plan++){
                                        var list=MasteredCombatSkillPlans.get(DomainManager.Extra, plan);
                                        if(list.Items==null){list.Items=new();}
                                        if(!list.Items.Contains(skillTemplateId)){
                                            list.Items.Add(skillTemplateId);
                                            MasteredCombatSkillPlans.set(DomainManager.Extra, plan, list, context);
                                        }
                                    }
                                    DomainManager.Extra.AddCharacterMasteredCombatSkill(context, DomainManager.Taiwu.GetTaiwuCharId(), skillTemplateId);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    [DisplayName("int_世界中蛟代步数")]
    public static int 世界中蛟代步数()=>DomainManager.Extra.Jiaos.Count;
    [DisplayName("int_世界中龙代步数")]
    public static int 世界中龙代步数()=>DomainManager.Extra.ChildrenOfLoong.Count;
    [DisplayName("void_获取满属性九子代步1")]
    public static void 获取九子(string names){
        foreach(var name in names.Split(',',StringSplitOptions.RemoveEmptyEntries)){
            if(Parser.Name2Id(name,Config.Jiao.Instance, out short templateId)){
                var key = DomainManager.Item.CreateItem(context, 4, DomainManager.Extra.GetJiaoCarrierTemplateIdByJiaoTemplateId(templateId));
                var loong = new GameData.DLC.FiveLoong.ChildrenOfLoong() {
                    Id = key.Id,
                    Key = key,
                    NameId = 0,
                    Properties = new(){
                        TravelTimeReduction = (Inherited: 100*GlobalConfig.Instance.JiaoMaxProperty[0], Fostered: 0),
                        MaxInventoryLoadBonus = (Inherited: 100*GlobalConfig.Instance.JiaoMaxProperty[1], Fostered: 0),
                        DropRateBonus = (Inherited: 100*GlobalConfig.Instance.JiaoMaxProperty[2], Fostered: 0),
                        CaptureRateBonus = (Inherited: 100*GlobalConfig.Instance.JiaoMaxProperty[3], Fostered: 0),
                        MaxKidnapSlotAbilityBonus = (Inherited: 100*GlobalConfig.Instance.JiaoMaxProperty[4], Fostered: 0),
                        Value = (Inherited: 100*GlobalConfig.Instance.JiaoMaxProperty[5], Fostered: 0),
                        Price = (Inherited: 100*GlobalConfig.Instance.JiaoMaxProperty[6], Fostered: 0),
                        HappinessChange = (Inherited: 100*GlobalConfig.Instance.JiaoMaxProperty[7], Fostered: 0),
                        FavorabilityChange = (Inherited: 100*GlobalConfig.Instance.JiaoMaxProperty[8], Fostered: 0)
                    },
                    Behavior = DomainManager.Taiwu.GetTaiwu().GetBehaviorType(),
                    JiaoTemplateId = templateId,
                    LoongTemplateId = templateId,
                    Gender = DomainManager.Taiwu.GetTaiwu().GetGender()==1
                };
                DomainManager.Extra.SetChildOfLoongKeyToId(loong.Key, loong.Id);
                DomainManager.Extra.SetCarrierTamePoint(context, loong.Key.Id, GlobalConfig.Instance.MaxCarrierTamePoint);
                DomainManager.Extra.SetChildrenOfLoong(context, loong);
                DomainManager.Taiwu.GetTaiwu().AddInventoryItem(context, key, 1);
            } else {
                logwarn($"不存在名称为「{name}」的蛟");
            }
        }
    }
    [DisplayName("void_高速轮回")]
    public static void 高速轮回() {
        if(check(samsaraPlatform.enable)){
            var arr = samsaraPlatform.get(DomainManager.Building);
            for(int i=0;i<(arr?.Length??0);i++){
                var ip = arr[i];
                ip.Second = GlobalConfig.Instance.SamsaraPlatformMaxProgress;
                samsaraPlatform.seti(DomainManager.Building, i, ip, context);
            }
        }
    }
    [DisplayName("void_与商人1交易后按ArgBox2跳转")]
    public static void 与商人1交易后按ArgBox2跳转(GameData.Domains.Character.Character merchant, GameData.Domains.TaiwuEvent.EventArgBox argBox){
        // EventHelper.StartMerchantAction(merchant.GetId(), "966d20e2-a5c8-40e4-a2cd-a5e3991ae53a", argBox);
        EventHelper.StartMerchantAction(merchant.GetId(), "4e657574-726f-6e20-3335-323900000000", argBox);
    }
    [DisplayName("int_坟墓1物品数")]
    public static int 坟墓1物品数(int targetGraveId) => DomainManager.Character.TryGetElement_Graves(targetGraveId, out var grave)?grave?.GetInventory()?.Items?.Count??0:0;
    [DisplayName("bool_搬空坟墓1")]
    public static bool 搬空坟墓1(int targetGraveId){
        if(DomainManager.Character.TryGetElement_Graves(targetGraveId, out var grave)) {
            var selfChar = DomainManager.Taiwu.GetTaiwu();
            var lifeRecordCollection = DomainManager.LifeRecord.GetLifeRecordCollection();
            var instantNotificationCollection = DomainManager.World.GetInstantNotificationCollection();
            int currDate = DomainManager.World.GetCurrDate();
            var location = selfChar.GetLocation();
            foreach(var (targetItemKey, amount) in grave.GetInventory().Items) {
                grave.RemoveInventoryItem(context, targetItemKey, amount);
                if(GameData.Domains.Item.ItemTemplateHelper.IsTransferable(targetItemKey.ItemType, targetItemKey.TemplateId)) {
                    lifeRecordCollection.AddRobItemFromGraveSucceed(selfChar.GetId(), currDate, targetGraveId, location, targetItemKey.ItemType, targetItemKey.TemplateId);
                    instantNotificationCollection.AddGetItem(selfChar.GetId(), targetItemKey.ItemType, targetItemKey.TemplateId);
                    selfChar.AddInventoryItem(context, targetItemKey, amount);
                } else {
                    lifeRecordCollection.AddReleaseCarrier(selfChar.GetId(), currDate, targetItemKey.ItemType, targetItemKey.TemplateId, location);
                    DomainManager.Item.RemoveItem(context, targetItemKey);
                }
            }
            var graveResources = grave.GetResources();
            bool setGrave = false;
            for (sbyte i = 0; i < 8; i++) unsafe {
                ref var amount = ref graveResources.Items[i];
                if (amount>0) {
                    selfChar.ChangeResource(context, i, amount);
                    lifeRecordCollection.AddRobResourceFromGraveSucceed(selfChar.GetId(), currDate, targetGraveId, location, i, amount);
                    amount -= amount;
                    setGrave = true;
                }
            }
            if(setGrave) {
                grave.SetResources(ref graveResources, context);
            }
            return true;
        } else {
            return false;
        }
    }
    [DisplayName("void_商人1可堆叠货物变为2个")]
    public static void 商人1可堆叠货物变为2个(GameData.Domains.Character.Character merchant, int count){
        var mdata = DomainManager.Merchant.GetMerchantData(context, merchant.GetId());
        int final = 7;
        if (mdata.GoodsList6 == null) {
            final--;
            if (mdata.GoodsList5 == null) {
                final--;
                if (mdata.GoodsList4 == null) {
                    final--;
                    if (mdata.GoodsList3 == null) {
                        final--;
                        if (mdata.GoodsList2 == null) {
                            final--;
                            if (mdata.GoodsList1 == null) {
                                final--;
                                if (mdata.GoodsList0 == null) {
                                    final--;
                                }
                            }
                        }
                    }
                }
            }
        }
        for(int i=0;i<final;i++){
            var tdata = GameData.Domains.Merchant.MerchantType.GetMerchantLevelData(Config.Merchant.Instance[mdata.MerchantTemplateId].MerchantType, (sbyte)i);
            var inv = mdata.GetGoodsList(i);
            if (tdata is not null) {
                for(var j=0;j<tdata.GoodsRate.Length;j++){
                    if (tdata.GoodsRate[j]!=0){
                        foreach(var preset in GameData.Domains.Merchant.MerchantData.GetGoodsPreset(tdata, j)) {
                            if (GameData.Domains.Item.ItemTemplateHelper.IsStackable(preset.ItemType, preset.StartId)) {
                                var key = DomainManager.Item.CreateItem(context, preset.ItemType, preset.StartId);
                                if(count>0) {
                                    // if(!inv.Items.ContainsKey(key)){
                                        inv.Items[key]=count;
                                    // }
                                    mdata.PriceChangeData[key] = -50;
                                } else {
                                    inv.Items.Remove(key);
                                    mdata.PriceChangeData.Remove(key);
                                }
                            }
                        }
                    }
                }
            }
            // if(count>0){
            //     foreach(var key in inv.Items.Keys.Where(x=>GameData.Domains.Item.ItemTemplateHelper.IsStackable(x.ItemType, x.TemplateId))) {
            //         inv.Items[key]=count;
            //     }
            // }
        }
    }
    [DisplayName("void_同道集体退队")]
    public static void 同道集体退队(){
        var cid = DomainManager.Taiwu.GetTaiwuCharId();
        var arr = DomainManager.Taiwu.GetGroupCharIds().GetCollection().Where(x=>x!=cid).ToArray();
        foreach(var x in arr){
            if(DomainManager.Taiwu.IsInGroup(x)){
                DomainManager.Taiwu.LeaveGroup(context, x, true, false, false);
            }
        }
    }
    [DisplayName("int_Array_地格1村民")]
    public static int[] 地格1村民(GameData.Domains.Map.Location loc){
        if (loc.IsValid() && DomainManager.Map.GetBlock(loc)?.CharacterSet is var set and not null) {
            return set.Where(x=>DomainManager.Character.TryGetElement_Objects(x, out var character) && character.GetOrganizationInfo().OrgTemplateId == 16).ToArray();
        } else {
            return new int[0];
        }
    }
    [DisplayName("int_地格1村民数")]
    public static int 地格1村民数(GameData.Domains.Map.Location loc)=>地格1村民(loc).Length;
    [DisplayName("void_同地格村民集体入队")]
    public static void 同地格村民集体入队(){
        var cid = DomainManager.Taiwu.GetTaiwuCharId();
        var arr = 地格1村民(DomainManager.Taiwu.GetTaiwu().GetLocation());

        foreach(var x in arr){
            if(!DomainManager.Taiwu.IsInGroup(x)){
                DomainManager.Taiwu.JoinGroup(context, x, false);
            }
        }
    }
    [DisplayName("void_数组1中人物立场设为2")] // 刚正=500
    public static void 数组1中人物立场设为2(IEnumerable<GameData.Domains.Character.Character> characters, short value){
        foreach(var character in characters){
            character.SetBaseMorality(value, context);
        }
    }
    [DisplayName("void_数组1中人物消去仇恨方向flag2")] // 1:自己->其他人 2:其他人->自己 3:双向
    public static void 数组1中人物消去双向仇恨(IEnumerable<GameData.Domains.Character.Character> characters, int flag){
        var remove_rel = 敌对关系();
        foreach(var character in characters){
            var ch = character.GetId();
            if ((flag&1)!=0) {
                foreach(var ch2 in DomainManager.Character.GetRelatedCharIds(ch, remove_rel).ToArray()){
                    DomainManager.Character.ChangeRelationType(context, ch, ch2, remove_rel, 0);
                }
            }
            if ((flag&2)!=0) {
                foreach(var ch2 in DomainManager.Character.GetReversedRelatedCharIds(ch, remove_rel).GetCollection().ToArray()){
                    DomainManager.Character.ChangeRelationType(context, ch2, ch, remove_rel, 0);
                }
            }
        }
    }

    [DisplayName("Int_Array_当前地格NPCid")]
    public static int[] 当前地格NPCid() {
        var loc = DomainManager.Taiwu.GetTaiwu().GetLocation();
        if(loc.IsValid()) {
            return DomainManager.Map.GetBlock(loc).CharacterSet.ToArray();
        } else {
            return new int[0];
        }
    }
    [DisplayName("Character_Array_将人物id列表1转化为人物列表")]
    public static GameData.Domains.Character.Character[] 将人物id列表1转化为人物列表(IEnumerable<int> arr)=>arr.Select(x=>DomainManager.Character.TryGetElement_Objects(x, out var ch)?ch:null).Where(c=>c is not null).ToArray();
    [DisplayName("Int_Array_将人物列表1转化为人物id列表")]
    public static int[] 将人物列表1转化为人物id列表(IEnumerable<GameData.Domains.Character.Character> arr)=>arr.Select(x=>x.GetId()).ToArray();
    [DisplayName("Int_Array_将人物id列表1排序")]
    public static int[] 将人物id列表1排序(IEnumerable<int> arr) => 将人物列表1转化为人物id列表(将人物列表1排序(将人物id列表1转化为人物列表(arr)));
    [DisplayName("Character_Array_将人物列表1排序")]
    public static GameData.Domains.Character.Character[] 将人物列表1排序(IEnumerable<GameData.Domains.Character.Character> arr) {
        var array = arr.ToArray();
        Array.Sort(array, (GameData.Domains.Character.Character x, GameData.Domains.Character.Character y)=>{
            if (x.GetOrganizationInfo().Grade != y.GetOrganizationInfo().Grade) {
                return x.GetOrganizationInfo().Grade < y.GetOrganizationInfo().Grade ? 1:-1; // grade降序排列
            }
            if (x.GetAgeGroup() != y.GetAgeGroup()) {
                return x.GetAgeGroup() < y.GetAgeGroup() ? 1:-1; // GetAgeGroup降序排列
            }
            if (x.GetGender() != y.GetGender()) {
                return x.GetGender() < y.GetGender() ? 1:-1; // GetGender降序排列
            }
            if (x.GetTransgender() != y.GetTransgender()) {
                return (x.GetGender() * 2 - 1)*(x.GetTransgender() ? 1 :-1); // GetTransgender按男性降序女性升序排列，这样trans会出于男女之间
            }
            if (x.GetAttraction() != y.GetAttraction()) {
                return x.GetAttraction() < y.GetAttraction() ? 1:-1; // GetConsummateLevel降序排列
            }
            if (x.GetConsummateLevel() != y.GetConsummateLevel()) {
                return x.GetConsummateLevel() < y.GetConsummateLevel() ? 1:-1; // GetConsummateLevel降序排列
            }
            if (x.GetCurrAge() != y.GetCurrAge()) {
                return x.GetCurrAge() < y.GetCurrAge() ? 1:-1; // GetCurrAge降序排列
            }
            if (x.GetId() != y.GetId()) {
                return x.GetId() < y.GetId() ? 1:-1; // GetId降序排列
            }
            return 0;
        });
        return array;
    }
    [DisplayName("Character_Array_太吾与同道")]
    public static GameData.Domains.Character.Character[] 太吾与同道()=>将人物id列表1转化为人物列表(DomainManager.Taiwu.GetGroupCharIds().GetCollection());
    [DisplayName("Int_Array_太吾同道id")]
    public static int[] 太吾与同道id()=>DomainManager.Taiwu.GetGroupCharIds().GetCollection().Where(x=>x!=DomainManager.Taiwu.GetTaiwuCharId()).ToArray();
    [DisplayName("Int_Array_人物1关押id列表")]
    public static int[] 人物1关押id列表(int char_id)=>DomainManager.Character.TryGetKidnappedCharacters(char_id, out var chs)?chs.GetCollection().Select(x=>x.CharId).ToArray():new int[0];
    [DisplayName("Int_Array_合并列表1与列表2")]
    public static int[] 合并列表1与列表2(IEnumerable<int> lst1, IEnumerable<int> lst2) {
        var hs = new HashSet<int>(lst1);
        return lst1.Concat(lst2.Where(x=>!hs.Contains(x))).ToArray();
    }
    [DisplayName("Character_Array_合并列表1与列表2")]
    public static GameData.Domains.Character.Character[] 合并列表1与列表2(IEnumerable<GameData.Domains.Character.Character> lst1, IEnumerable<GameData.Domains.Character.Character> lst2) {
        var hs = new HashSet<int>(lst1.Select(x=>x.GetId()));
        return lst1.Concat(lst2.Where(x=>!hs.Contains(x.GetId()))).ToArray();
    }
    [DisplayName("Int_Array_筛选与门派1有仇的人物id列表2")]
    public static int[] 筛选与门派1有仇的人物id列表2(short settlement_id, IEnumerable<int> lst) => lst.Where(x=>EventHelper.IsCharacterSectFugitive(x, settlement_id) || EventHelper.IsCharacterSectEnemy(x, settlement_id)).ToArray();
    [DisplayName("Int_List_列表1转化为List")]
    public static List<int> 列表1转化为List(IEnumerable<int> lst) => lst.ToList();
    [DisplayName("int_列表1长度")]
    public static int int_列表1长度(IEnumerable<int> lst)=>lst.Count();

    [DisplayName("void_使用ArgBox1收监有罪人物id列表2")]
    public static void 使用ArgBox1收监无罪人物id列表2(GameData.Domains.TaiwuEvent.EventArgBox ab, IEnumerable<int> lst) {
        short set=0;
        if(ab.Get("SettlementId", ref set)) {
            foreach(var ch in lst) {
                EventHelper.SendCharacterIntoPrison(ch, set, ab);
            }
        } else {
            logwarn("调用函数`void_使用ArgBox1收监人物id列表2`时，需要在ArgBox中设置`SettlementId`");
        }
    }
    [DisplayName("void_使用ArgBox1收监无罪人物id列表2持续3月")]
    public static void 使用ArgBox1收监无罪人物id列表2持续3月(GameData.Domains.TaiwuEvent.EventArgBox ab, IEnumerable<int> lst, int duration) {
        short set=0;
        if(ab.Get("SettlementId", ref set)) {
            foreach(var ch in lst) {
                EventHelper.SendCharacterIntoPrison(ch, set, ab, duration);
            }
        } else {
            logwarn("调用函数`void_使用ArgBox1收监人物id列表2`时，需要在ArgBox中设置`SettlementId`");
        }
    }

    [DisplayName("Character_Array_当前地格NPC")]
    public static GameData.Domains.Character.Character[] 当前地格NPC()=>将人物id列表1转化为人物列表(当前地格NPCid());
    [DisplayName("void_用ArgBox1战斗后人物2抓捕人物3")]
    public static void 用ArgBox1战斗后人物2抓捕人物3(GameData.Domains.TaiwuEvent.EventArgBox ab, GameData.Domains.Character.Character self, GameData.Domains.Character.Character victim) {
        if(!ab.Get("ItemKeySeizeCharacterInCombat", out GameData.Domains.Item.ItemKey item)) {
            item = DomainManager.Item.CreateMisc(context, 73);
            DomainManager.Taiwu.GetTaiwu().AddInventoryItem(context, item, 1, false);
        }
        EventHelper.AddPrisonerToCharacter(victim.GetId(), self.GetId(), item);
    }
    [DisplayName("void_用ArgBox1选至多2个在人物id列表3中的人")] // 确认自动选择选项1，取消自动选择选项2，需确保两个选项都可见可用
    public static void 用ArgBox1选至多2个在人物id列表3中的人(GameData.Domains.TaiwuEvent.EventArgBox argBox, int total, IEnumerable<int> list) {
        if (!argBox.Get("SelectCharacterData", out GameData.Domains.TaiwuEvent.DisplayEvent.EventSelectCharacterData data)) {
            data = new GameData.Domains.TaiwuEvent.DisplayEvent.EventSelectCharacterData();
            argBox.Set("SelectCharacterData", data);
        }
        var lst = list.Select(DomainManager.Character.GetCharacterDisplayData).ToList();
        data.FilterList = Enumerable.Range(0,total).Select(x=>{
            argBox.Remove<int>($"NeutronSelectKey_{x}");
            return new GameData.Domains.TaiwuEvent.DisplayEvent.CharacterSelectFilter {
                FilterTemplateId=-1,
                SelectKey=$"NeutronSelectKey_{x}",
                AvailableCharactersDisplayDataList=lst
            };
        }).ToList();
        data.UseOrOperate = true;
        argBox.Set("NeutronSelectCount", total);
    }

    [DisplayName("void_用ArgBox1选在人物id列表2中的人")] // 确认自动选择选项1，取消自动选择选项2，需确保两个选项都可见可用
    public static void 用ArgBox1选在人物id列表2中的人(GameData.Domains.TaiwuEvent.EventArgBox argBox, IEnumerable<int> list) {
        if (!argBox.Get("SelectCharacterData", out GameData.Domains.TaiwuEvent.DisplayEvent.EventSelectCharacterData data)) {
            data = new GameData.Domains.TaiwuEvent.DisplayEvent.EventSelectCharacterData();
            argBox.Set("SelectCharacterData", data);
        }
        int total=0;
        if(list.Count()>0) {
            data.FilterList = list.Select(DomainManager.Character.GetCharacterDisplayData).Select(key=>{
                argBox.Remove<int>($"NeutronSelectKey_{total}");
                return new GameData.Domains.TaiwuEvent.DisplayEvent.CharacterSelectFilter {
                    FilterTemplateId=-1,
                    SelectKey=$"NeutronSelectKey_{total++}",
                    AvailableCharactersDisplayDataList=new List<GameData.Domains.Character.Display.CharacterDisplayData>(){key}
                };
            }).ToList();
        } else {
            data.FilterList = new List<GameData.Domains.TaiwuEvent.DisplayEvent.CharacterSelectFilter>(){
                new GameData.Domains.TaiwuEvent.DisplayEvent.CharacterSelectFilter {
                    FilterTemplateId=-1,
                    SelectKey=$"NeutronSelectKey_0",
                    AvailableCharactersDisplayDataList=new List<GameData.Domains.Character.Display.CharacterDisplayData>(){}
                }
            };
        }
        data.UseOrOperate = true;
        argBox.Set("NeutronSelectCount", total);
    }


    [DisplayName("void_开小窗用ArgBox1选在人物id列表2中的人")] // 确认自动选择选项1，取消自动选择选项2，需确保两个选项都可见可用
    public static void 开小窗用ArgBox1选在人物id列表2中的人(GameData.Domains.TaiwuEvent.EventArgBox argBox, IEnumerable<int> list) {
        argBox.Set("ShowCountLimit", false);
        argBox.Set("CanSelectInfectedChar", true);
        argBox.Set("QuickSelect", true);
        throw new Exception("TODO");
    }


    [DisplayName("void_整理ArgBox1中的选取结果")]
    public static void 整理ArgBox1中的选取结果(GameData.Domains.TaiwuEvent.EventArgBox argBox) {
        int total = 0;
        var res = GameData.Utilities.IntList.Create();
        if(!argBox.Get("NeutronSelectCount", ref total)) {
            logwarn("Warning: 找不到NeutronSelectCount，疑似选取错误，返回空结果");
            argBox.Set("NeutronSelectIntResults", res);
            return;
        }
        int cid = 0;
        for(var i=0; i<total; i++) {
            if (argBox.Get($"NeutronSelectKey_{i}", ref cid)) {
                res.Items.Add(cid);
                argBox.Remove<int>($"NeutronSelectKey_{i}");
            //     logwarn($"第{i}个人的id为{cid}");
            // } else {
            //     logwarn($"第{i}个人未选中");
            }
        }
        argBox.Set("NeutronSelectIntResults", res);
    }
    [DisplayName("Int_Array_读取ArgBox1中选取的人物id")]
    public static int[] 读取ArgBox1中选取的人物id(GameData.Domains.TaiwuEvent.EventArgBox argBox) {
        if(argBox.Get("NeutronSelectIntResults", out GameData.Utilities.IntList res)) {
            return res.Items.ToArray();
        } else {
            logwarn("Error: 未找到NeutronSelectIntResults");
        }
        return new int[0];
    }

    [DisplayName("Int_Array_接管ArgBox1中选取的人物id")] // 会删除id
    public static int[] 接管ArgBox1中选取的人物id(GameData.Domains.TaiwuEvent.EventArgBox argBox) {
        if(argBox.Get("NeutronSelectIntResults", out GameData.Utilities.IntList res)) {
            argBox.Remove<GameData.Utilities.IntList>("NeutronSelectIntResults");
            return res.Items.ToArray();
        } else {
            logwarn("Error: 未找到NeutronSelectIntResults");
        }
        return new int[0];
    }
    [DisplayName("void_人物1将人物列表2中全部人物收监")]
    public static void 读取之前在ArgBox1中选取的人物(GameData.Domains.Character.Character self, IEnumerable<GameData.Domains.Character.Character> chs) {
        var item = DomainManager.Item.CreateMisc(context, 73);
        foreach(var victim in chs) {
            self.AddInventoryItem(context, item, 1, false);
            EventHelper.AddPrisonerToCharacter(victim.GetId(), self.GetId(), item);
        }
    }
    [DisplayName("DEBUG：读取之前在ArgBox1中选取的人物")] // 确认自动选择选项1，取消自动选择选项2，需确保两个选项都可见可用
    public static void 读取之前在ArgBox1中选取的人物(GameData.Domains.TaiwuEvent.EventArgBox argBox) {
        if(argBox.Get("NeutronSelectIntResults", out GameData.Utilities.IntList res)) {
            logwarn($"选取了：{string.Join(" ", res.Items.Select(x=>x.ToString()))}");
        } else {
            logwarn("Error: 未找到NeutronSelectIntResults");
        }
    }
    [DisplayName("void_人物1与人物列表2中人物结婚")] // 确认自动选择选项1，取消自动选择选项2，需确保两个选项都可见可用
    public static void 人物1与人物列表2中人物结婚(GameData.Domains.Character.Character target, IEnumerable<GameData.Domains.Character.Character> clist){
        var target_oid = target.GetOrganizationInfo().OrgTemplateId;
        var target_loc = target.GetLocation();
        var taiwu = DomainManager.Taiwu.GetTaiwu();
        var taiwuCharId = DomainManager.Taiwu.GetTaiwuCharId();
        var organizationInfo = target.GetOrganizationInfo();
        foreach(var victim in clist) {
            if(victim.GetKidnapperId() != -1) {
                DomainManager.Character.RemoveKidnappedCharacter(context, victim.GetId(), victim.GetKidnapperId(), true);
            }
            if(victim.GetOrganizationInfo().OrgTemplateId != target_oid && EventHelper.CharacterIsInTaiwuGroup(victim.GetId())) {
                EventHelper.RequestRoleLeaveGroup(victim.GetId());
            }
            EventHelper.AddHusbandOrWifeRelations(target.GetId(), victim.GetId());
            int metaDataId = EventHelper.CreateSecretInformationMetaData(EventHelper.GetSecretInformationCollection().AddBecomeHusbandAndWife(target.GetId(), victim.GetId()));
            EventHelper.DistributeSecretInformationToCharacter(metaDataId, taiwuCharId);
            // EventHelper.JoinSpouseOrganization(victim, target);
            // if (!EventHelper.IsInAnySect(target.GetId()) && organizationInfo.OrgTemplateId != 16 && EventHelper.GetRoleGrade(victim) == 0 && EventHelper.GetRoleGrade(target) != 0) {
            //     int changeValue = EventHelper.GetRoleGrade(target) - EventHelper.GetRoleGrade(victim);
            //     EventHelper.ChangeOrganizationGrade(victim.GetId(), changeValue);
            // }
            {
                var spouseOrgInfo = target.GetOrganizationInfo();
                var spouseOrgMemberCfg = GameData.Domains.Organization.OrganizationDomain.GetOrgMemberConfig(spouseOrgInfo);
                if (spouseOrgInfo.OrgTemplateId == 16) {
                    var selfNewOrgInfo = new GameData.Domains.Character.OrganizationInfo(spouseOrgInfo.OrgTemplateId, 0, principal: true, spouseOrgInfo.SettlementId);
                    DomainManager.Organization.ChangeOrganization(context, victim, selfNewOrgInfo);
                } else {
                    var selfNewOrgInfo = new GameData.Domains.Character.OrganizationInfo(spouseOrgInfo.OrgTemplateId, (sbyte)((!spouseOrgMemberCfg.RestrictPrincipalAmount || spouseOrgMemberCfg.DeputySpouseDowngrade >= 0) ? spouseOrgInfo.Grade : 0), spouseOrgMemberCfg.DeputySpouseDowngrade < 0, spouseOrgInfo.SettlementId);
                    DomainManager.Organization.ChangeOrganization(context, victim, selfNewOrgInfo);
                }
            }
            EventHelper.MoveIntelligentCharacter(victim, target_loc);
            int num = (1000 + EventHelper.GetRoleFame(victim) * 25 + EventHelper.GetRoleCharm(victim) / 100 * (EventHelper.GetRoleCharm(victim) / 100) * 25) * (120 - EventHelper.GetRoleAge(victim)) / 100;
            int num2 = (1000 + EventHelper.GetRoleFame(target) * 25 + EventHelper.GetRoleCharm(target) / 100 * (EventHelper.GetRoleCharm(target) / 100) * 25) * (120 - EventHelper.GetRoleAge(target)) / 100;
            if (EventHelper.GetRoleAge(victim) >= 120) {
                num = 0;
            }
            if (EventHelper.GetRoleAge(target) >= 120) {
                num2 = 0;
            }
            if (victim.GetTransgender()) {
                num = num * 3 / 2;
            }
            if (target.GetTransgender()) {
                num2 = num2 * 3 / 2;
            }
            EventHelper.ChangeFavorabilityOptionalRepeatedEvent(target, taiwu, EventHelper.ClampFavorabilityChangeValue(num));
            EventHelper.ChangeFavorabilityOptionalRepeatedEvent(victim, taiwu, EventHelper.ClampFavorabilityChangeValue(num2));
        }
    }
}
