// chmod -R 755 ../../csvData 2>/dev/null && rm -r ../../csvData && echo deleting ../../csvData || echo ../../csvData has been deleted
// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using System.Linq;
#if BACKEND
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;
#else
using Newtonsoft.Json;
#endif

using static Utils.Logger;

using Config;
using Config.ConfigCells.Character;

namespace DataDumper;
using Utils;
using static Utils.Logger;

public class DataDumper {
    public static string SPLIT_EXT=".csv";
    public static string SPLIT_CHAR=",";
    public static string SPLIT_CHAR_REPLACE="，";
    public static string ModId;
    public static string PATH="csvData";
    public static bool QUOTE=false;
    public static string parse_string(string x){
        if(QUOTE && (x.Contains('\n') || x.Contains(SPLIT_CHAR))){
            x="\""+x.Replace("\"","\"\"")+"\"";
        }
        return x.Replace(SPLIT_CHAR,SPLIT_CHAR_REPLACE).Replace("\n","\\n");
    }
    static bool enable(string key){
        try {
            return _enable(key);
        } catch(Exception ex){
            logwarn("enable 出错，出错键值为\""+key+"\"，错误原因是",ex);
            return false;
        }
    }
    static bool _enable(string key){
        bool enabled=false;
        return
        #if BACKEND
        GameData.Domains.DomainManager.Mod.GetSetting(ModId, key, ref enabled)
        #else
        ModManager.GetSetting(ModId, key,ref enabled)
        #endif
        && enabled;
    }
    #if BACKEND
    static JsonSerializerOptions options = new JsonSerializerOptions { IncludeFields = true , WriteIndented = false, Encoder = JavaScriptEncoder.Create(UnicodeRanges.All)};
    #endif
    public void Initialize(string id){
        ModId=id;
        if(!
            #if BACKEND
            GameData.Domains.DomainManager.Mod.GetSetting
            #else
            ModManager.GetSetting
            #endif
            (ModId, "DumpDir", ref PATH)
        ){
            return;
        }
        if(enable("tsv")){
            SPLIT_CHAR="\t";
            SPLIT_CHAR_REPLACE="    ";
            SPLIT_EXT=".tsv";
        }
        QUOTE=enable("quote");
        #if BACKEND
        if(enable("CheckVersion")){
            PATH=PATH+"-"+GameData.Domains.DomainManager.Global.GetGameVersion();
        }
        #else
        //TODO: 前端的CheckVersion事实上是可用的
        #endif
        if(string.IsNullOrEmpty(PATH) || System.IO.Directory.Exists(PATH)){return;}
        System.IO.Directory.CreateDirectory(PATH);

        System.IO.File.WriteAllText(PATH+"/SkillBreakGridList"+SPLIT_EXT,"#"+SPLIT_CHAR+"名称"+SPLIT_CHAR+"承"+SPLIT_CHAR+"合"+SPLIT_CHAR+"解"+SPLIT_CHAR+"异"+SPLIT_CHAR+"独\n");
        foreach(SkillBreakGridListItem item in SkillBreakGridList.Instance){
            System.IO.File.AppendAllText(PATH+"/SkillBreakGridList"+SPLIT_EXT,item.TemplateId+SPLIT_CHAR+CombatSkill.Instance[item.TemplateId].Name+SPLIT_CHAR+breakGrid(item.BreakGridListJust)+SPLIT_CHAR+breakGrid(item.BreakGridListKind)+SPLIT_CHAR+breakGrid(item.BreakGridListEven)+SPLIT_CHAR+breakGrid(item.BreakGridListRebel)+SPLIT_CHAR+breakGrid(item.BreakGridListEgoistic)+"\n");
        }
        System.IO.File.WriteAllText(PATH+"/SkillBreakPlateGridBonusType"+SPLIT_EXT,"#"+SPLIT_CHAR+"名称"+SPLIT_CHAR+"人物修正"+SPLIT_CHAR+"技能修正"+"\n");
        foreach(SkillBreakPlateGridBonusTypeItem item in SkillBreakPlateGridBonusType.Instance){
            System.IO.File.AppendAllText(PATH+"/SkillBreakPlateGridBonusType"+SPLIT_EXT,item.TemplateId.ToString()+SPLIT_CHAR+item.Name+SPLIT_CHAR+CharacterPropertyAndValue(item.CharacterPropertyBonusList)+SPLIT_CHAR+CombatPropertyAndValue(item.CombatSkillPropertyBonusList)+"\n");
        }
        var q = typeof(Config.Adventure).Assembly.GetTypes().Where(t=>t.IsClass && t.Namespace == typeof(Config.Adventure).Namespace);

        var nm=q.Select(q=>q.Name).ToArray();
        #if export_NameList
        string str="";
        foreach(var target in q){
            //            str+="//        search for type "+target.Name+"\n";
            if(nm.Contains(target.Name+"Item")){
                //                str+="//        found legal type "+target.Name+"\n";
                if(target.GetField("Instance") !=null){
                    //                    str+="//        ProcessInstance("+target.Name+".Instance);\n";
                    var v=target.GetField("Instance").GetValue(null);
                    if(v!=null)str+="        ProcessInstance(Config."+target.Name+".Instance);\n";
                }
            }
        }
        System.IO.File.WriteAllText(PATH+"/NameList[auto].txt",str);
        #endif
        var processInstance=typeof(DataDumper).GetMethod("ProcessInstance");
        System.Threading.Tasks.Parallel.ForEach(q,target=>{
            //            str+="//        search for type "+target.Name+"\n";
            if(nm.Contains(target.Name+"Item")){
                //                str+="//        found legal type "+target.Name+"\n";
                if(target.GetField("Instance") !=null){
                    //                    str+="//        ProcessInstance("+target.Name+".Instance);\n";
                    Type type=typeof(object);
                    try {
                        type=(new List<Type>(from t in target.GetMethods((BindingFlags)(-1)) where t.Name == "GetItem" select t.ReturnType))[0];//target.GetMethod("GetItem").ReturnType;
                    } catch(Exception ex){
                        logwarn("这是搜索"+target.Name+"数据时出了问题，此BUG不影响其他数据的导出工作",ex);
                        return;
                    }
                    var v=target.GetField("Instance").GetValue(null);
                    try{
                        if(v!=null){
                            processInstance.MakeGenericMethod(type).Invoke(null, new object[]{v,null});
                        }
                    } catch(Exception ex){
                        logwarn("导出"+target.Name+"数据存在问题，此BUG不影响其他数据的导出工作",ex);
                        return;
                    }
                } else {
                    System.IO.File.WriteAllText("missing.instance.of."+target.Name,"");
                }
            } else try{
                if(target == typeof(Config.LocalMonasticTitles)){
                    var type=typeof(Config.MonasticTitleItem);
                    processInstance.MakeGenericMethod(type).Invoke(null, new object[]{((Config.LocalMonasticTitles)(target.GetField("Instance").GetValue(null))).MonasticTitles,type});
                } else if(target == typeof(Config.LocalNames)){
                    var type=typeof(Config.HanNameItem);
                    processInstance.MakeGenericMethod(type).Invoke(null, new object[]{((Config.LocalNames)(target.GetField("Instance").GetValue(null))).AllNamesCore,type});
                } else if(target == typeof(Config.LocalSurnames)){
                    var type=typeof(Config.SurnameItem);
                    processInstance.MakeGenericMethod(type).Invoke(null, new object[]{((Config.LocalSurnames)(target.GetField("Instance").GetValue(null))).SurnameCore,type});
                } else if(target == typeof(Config.LocalTownNames)){
                    var type=typeof(Config.TownNameItem);
                    processInstance.MakeGenericMethod(type).Invoke(null, new object[]{((Config.LocalTownNames)(target.GetField("Instance").GetValue(null))).TownNameCore,type});
                } else if(target == typeof(Config.LocalZangNames)){
                    var type=typeof(Config.ZangNameItem);
                    processInstance.MakeGenericMethod(type).Invoke(null, new object[]{((Config.LocalZangNames)(target.GetField("Instance").GetValue(null))).ZangNameCore,type});
                } else if(typeof(Config.Common.IConfigData).IsAssignableFrom(target)){
                    throw new Exception("missing impl.");
                }
            } catch (Exception e){
                System.IO.File.WriteAllText("missing."+target.Name,_strexception(e));
            }
        });

        System.Threading.Tasks.Parallel.ForEach(typeof(Config.Adventure).Assembly.GetTypes().Where(ty=>ty.Name.EndsWith("Helper")),ty=>{
            foreach (var str in new String[]{"FieldIds","MethodIds","DataIds"}){
                save($"{ty.Name}[{str}]", false, content=>{
                    if(ty.GetNestedType(str) is not null){
                        content.AppendLine($"{str[..^1]}{SPLIT_CHAR}Name");
                        var fids=ty.GetNestedType(str);
                        foreach(var field in fids.GetFields(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public)){
                            content.Append(field?.GetValue(null)?.ToString()??string.Empty);
                            content.Append(SPLIT_CHAR);
                            content.AppendLine(field.Name);
                        }
                    }
                });
            }
        });
        if(enable("readonly")){
            foreach(var file in System.IO.Directory.GetFiles(PATH)){
                System.IO.File.SetAttributes(file, System.IO.File.GetAttributes(file) | System.IO.FileAttributes.ReadOnly);
            }
        }
    }

    public static void ProcessInstance<T>(IEnumerable<T> instance, Type indexType=null){ // instance[0].GetType()
        var name=instance.GetType().Name;
        indexType=indexType
            ?? instance.GetType().GetMethod("GetItem", new Type[]{typeof(short)})?.ReturnType
            ?? instance.GetType().GetMethod("GetItem", new Type[]{typeof(int)})?.ReturnType
            ?? instance.GetType().GetMethod("GetItem", new Type[]{typeof(byte)})?.ReturnType
            ?? instance.GetType().GetMethod("GetItem", new Type[]{typeof(sbyte)})?.ReturnType
            ?? instance.GetType().GetMethod("GetItem", new Type[]{typeof(ushort)})?.ReturnType;
        if(indexType==null){
            foreach(var x in instance.GetType().GetProperties((BindingFlags)(-1))){
                if(x.Name=="Item"){
                    indexType=x.GetGetMethod().ReturnType;
                }
                break;
            }
            if(indexType==null){
                throw new Exception("类"+name+"中不存在合适的get_Item函数，应自行写入合适的type");
            }
        }
        save(name+"[auto]",(content)=>{
            //var instance=Character.Instance;
            var fields=indexType.GetFields();
            foreach(var field in fields){
                content.Append(field.Name+SPLIT_CHAR);
            }
            content.Append("\n");
            int line=0;
            foreach(var item in instance){
                if(item!=null){
                    foreach(var field in fields){
                        content.Append(ItemField(item,field,line));
                    }
                    if(item is Config.SpecialEffectItem it){
                        // logger($"Config.SpecialEffectItem is {it.GetType().Name}");
                        string clsnm="GameData.Domains.SpecialEffect."+it.ClassName;
                        var cls=typeof(Config.SpecialEffectItem).Assembly.GetType(clsnm);
                        // logger($"cls got {clsnm} as {cls?.Name??"null"}");
                        if(cls!=null){
                            // FieldInfo dict=null;
                            foreach(var x in cls.GetFields(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy)){
                                if(x.Name!="CalcInterruptOddsFuncDict"){
                                    // var val=x.GetValue(null).ToString().Replace(',','，').Replace("\n","\\n");
                                    var val=ItemField(null, x, line);
                                    content.Append($"{x.Name} = {val}");
                                } //  else {dict=x;}
                            }
                            // if(dict!=null){
                            //     var val=ItemField(null, dict, line);
                            //     content.Append($"{dict.Name} = {val},");
                            // }
                            // 无法获取类实例，因为类实例需要绑定NPC以判定正逆练。
                            // foreach(var x in cls.GetFields(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy)){
                            //     var val=x.GetValue(null).ToString().Replace(',','，').Replace("\n","\\n");
                            //     content.Append($"{x.Name} = {val},");
                            // }
                        }
                    }
                } else {
                    content.Append($"{line},null readed.");
                }
                line++;
                content.Append("\n");
            }
        });
    }
    public static void save(string name, Action<System.Text.StringBuilder> act)=>save(name,true,act);
    public static void save(string name, bool warn, Action<System.Text.StringBuilder> act){
        System.Text.StringBuilder content=new();
        try{
            act(content);
        }catch(Exception e){
            System.IO.File.WriteAllText(name+".err.txt",_strexception(e));
            throw;
        }finally{
            if(content.Length>0){
                if(enable("gbk")){
                    System.IO.File.WriteAllBytes(PATH+"/"+name+".gbk"+SPLIT_EXT, System.Text.Encoding.GetEncoding("GBK").GetBytes(content.Replace(SPLIT_CHAR+"\n","\n").ToString()));
                } else {
                    System.IO.File.WriteAllText(PATH+"/"+name+SPLIT_EXT,content.Replace(SPLIT_CHAR+"\n","\n").ToString());
                }
            } else if(warn){
                logwarn($"并没有获取到关于 {name} 的任何书籍");
            }
        }
    }
    public static MethodInfo _convert=typeof(DataDumper).GetMethod("convert");
    public static MethodInfo _Sconvert=typeof(DataDumper).GetMethod("Sconvert");
    public static string ItemField(object item, FieldInfo field, int Line){
        var x=field.GetValue(item);
        if(x==null){
            return SPLIT_CHAR;
        }else if (x is GameData.Domains.Item.PoisonsAndLevels pois){
            return PoisonsAndLevels(pois)+SPLIT_CHAR;
        } else if(x is string str){
            return parse_string(str)+SPLIT_CHAR;
        }
        foreach(var ifce in new Type[]{typeof(byte),typeof(sbyte),typeof(ushort),typeof(short),typeof(uint),typeof(int)}){
            if (typeof(IEnumerable<>).MakeGenericType(ifce).IsAssignableFrom(field.FieldType)){
                var ele=field.FieldType.GetElementType();
                if(ele==null)ele=field.FieldType.GetGenericArguments()[0];
                return ((string)_convert.MakeGenericMethod(ele).Invoke(null,new object[]{x}))+SPLIT_CHAR;
            }
        }
        foreach(var ifce in new Type[]{typeof(string),typeof(Config.ConfigCells.Character.PropertyAndValue),typeof(Config.ConfigCells.Character.PresetInventoryItem),typeof(Config.ConfigCells.Character.PresetEquipmentItem),typeof(Config.ConfigCells.Character.PresetEquipmentItemWithProb)}){
            if (typeof(IEnumerable<>).MakeGenericType(ifce).IsAssignableFrom(field.FieldType)){
                var ele=field.FieldType.GetElementType();
                if(ele==null)ele=field.FieldType.GetGenericArguments()[0];
                return ((string)_Sconvert.MakeGenericMethod(ele).Invoke(null,new object[]{x}))+SPLIT_CHAR;
            }
        }

        if(field.FieldType.GetField("Items") != null){
            var y=(System.Runtime.CompilerServices.FixedBufferAttribute)Attribute.GetCustomAttribute(field.FieldType.GetField("Items"),typeof(System.Runtime.CompilerServices.FixedBufferAttribute));
            var ser=field.FieldType.GetMethod("Serialize",(BindingFlags)(-1),null,new Type[]{typeof(byte*)},null);
            var mgm=typeof(cvtr<>).MakeGenericType(new Type[]{y.ElementType});
            if(y!=null && ser!=null && mgm!=null){
                var cvt=mgm.GetMethod("structConvert");
                if(cvt!=null){
                    return (string)(cvt.Invoke(null, new object[]{x,y.Length,ser}))+SPLIT_CHAR;
                }
            }
        }
        try {
            #if BACKEND
            string ret = parse_string(JsonSerializer.Serialize(x, options));
            #else
            var ret=parse_string(JsonConvert.SerializeObject(x));
            #endif
            if(field.FieldType.IsEnum){
                ret=x.ToString()+" = "+ret;
            }
            return ret+SPLIT_CHAR;
        } catch {
            return x.ToString()+SPLIT_CHAR;
        }
    }
    static string[] POISON_LEVEL={"无毒","生毒","剧毒","奇毒"};
    public static unsafe string PoisonsAndLevels(GameData.Domains.Item.PoisonsAndLevels x){
        var s="";
        short* ptr = x.Values;
        sbyte* ptr2 = x.Levels;
        for(int i=0;i<5;i++)s+=ptr[i].ToString()+"("+POISON_LEVEL[ptr2[i]]+")，";
        s+=ptr[5].ToString()+"("+POISON_LEVEL[ptr2[5]]+")";
        return s;
    }

    public static Dictionary<int,string> item_dict =
    (from i in Weapon.Instance select (((byte)i.ItemType<<16)|(ushort)i.TemplateId, i.Name)).Concat
    (from i in Armor.Instance select (((byte)i.ItemType<<16)|(ushort)i.TemplateId, i.Name)).Concat
    (from i in Accessory.Instance select (((byte)i.ItemType<<16)|(ushort)i.TemplateId, i.Name)).Concat
    (from i in Clothing.Instance select (((byte)i.ItemType<<16)|(ushort)i.TemplateId, i.Name)).Concat
    (from i in Carrier.Instance select (((byte)i.ItemType<<16)|(ushort)i.TemplateId, i.Name)).Concat
    (from i in Config.Material.Instance select (((byte)i.ItemType<<16)|(ushort)i.TemplateId, i.Name)).Concat
    (from i in CraftTool.Instance select (((byte)i.ItemType<<16)|(ushort)i.TemplateId, i.Name)).Concat
    (from i in Food.Instance select (((byte)i.ItemType<<16)|(ushort)i.TemplateId, i.Name)).Concat
    (from i in Medicine.Instance select (((byte)i.ItemType<<16)|(ushort)i.TemplateId, i.Name)).Concat
    (from i in TeaWine.Instance select (((byte)i.ItemType<<16)|(ushort)i.TemplateId, i.Name)).Concat
    (from i in SkillBook.Instance select (((byte)i.ItemType<<16)|(ushort)i.TemplateId, i.Name)).Concat
    (from i in Cricket.Instance select (((byte)i.ItemType<<16)|(ushort)i.TemplateId, i.Name)).Concat
    (from i in Misc.Instance select (((byte)i.ItemType<<16)|(ushort)i.TemplateId, i.Name)).ToDictionary(i=>i.Item1,i=>i.Item2);

    public static string item_name(int x){
        string res;
        if (item_dict.TryGetValue(x,out res)){return res;}
        if((x&65535)==65535){
            return "无";
        }else{
            logwarn($"找不到item_name为{x.ToString("X8")}的物品");
            return "failed";
        }
    }
    public static unsafe string PresetEquipmentItem(Config.ConfigCells.Character.PresetEquipmentItem x){
        return $"{item_name((x.Type<<16) | (ushort)x.TemplateId)}";
    }
    public static unsafe string PresetEquipmentItemWithProb(Config.ConfigCells.Character.PresetEquipmentItemWithProb x){
        return $"{item_name((x.Type<<16) | (ushort)x.TemplateId)}({x.Prob}%)";
    }
    public static unsafe string PresetInventoryItem(Config.ConfigCells.Character.PresetInventoryItem x){
        return $"{item_name((x.Type<<16) | (ushort)x.TemplateId)}x{x.Amount}({x.SpawnChance}%)";
    }

    public static class cvtr<T> where T: unmanaged {
        public static string structConvert(object item, int count, MethodInfo ser){
            if(item==null)return "";
            string s="";
            T[] array=new T[count+100];
            ser.Invoke(item,new object[]{System.Runtime.InteropServices.Marshal.UnsafeAddrOfPinnedArrayElement(array,0)});
            for(int i=0;i<count;i++){
                s+=array[i].ToString()+" ";
            }
            return s;
        }
    }
    static readonly char[] _trim_chars=new char[]{' ','\n','\t'};
    public static string Sconvert<T>(IEnumerable<T> item){
        if(item==null)return "";
        string s="";
        if(item is IEnumerable<Config.ConfigCells.Character.PropertyAndValue> pav)foreach(var x in pav){
            s+=((Config.ConfigCells.Character.PropertyAndValue)(object)x).PropertyId.ToString()+"="+((Config.ConfigCells.Character.PropertyAndValue)(object)x).Value+" ";
        } else if(item is IEnumerable<string> it)foreach(var x in it){
            s+=parse_string(x)+" ";
        } else if(item is IEnumerable<Config.ConfigCells.Character.PresetEquipmentItem> pei)foreach(var x in pei){
            s+=PresetEquipmentItem(x)+" ";
        } else if(item is IEnumerable<Config.ConfigCells.Character.PresetEquipmentItemWithProb> peiwp)foreach(var x in peiwp){
            s+=PresetEquipmentItemWithProb(x)+" ";
        } else if(item is IEnumerable<Config.ConfigCells.Character.PresetInventoryItem> pii)foreach(var x in pii){
            s+=PresetInventoryItem(x)+" ";
        }
        return s.Trim(_trim_chars);
    }
    public static string convert<T>(IEnumerable<T> item){
        if(item==null)return "";
        string s="";
        foreach(var x in item){
            s+=x.ToString()+" ";
        }
        return s.Trim();
    }
    public static string CharacterPropertyAndValue(Config.ConfigCells.Character.PropertyAndValue[] arr){
        string result="";
        foreach(Config.ConfigCells.Character.PropertyAndValue g in arr){
            result+=" "+CharacterPropertyDisplay.Instance[g.PropertyId].Name+(g.Value>0?"+":"")+g.Value+(CharacterPropertyDisplay.Instance[g.PropertyId].IsPercent?"%":"");
        }
        return result;
    }
    public static string CombatPropertyAndValue(Config.ConfigCells.Character.PropertyAndValue[] arr){
        string result="";
        foreach(Config.ConfigCells.Character.PropertyAndValue g in arr){
            result+=" "+CombatSkillProperty.Instance[g.PropertyId].Name+(g.Value>0?"+":"")+g.Value+(CombatSkillProperty.Instance[g.PropertyId].IsPercent?"%":"");
        }
        return result;
    }
    public static string breakGrid(List<BreakGrid> lst){
        string result="";
        foreach(BreakGrid g in lst){
            result+=" "+SkillBreakPlateGridBonusType.Instance[g.BonusType].Name+"x"+g.GridCount;
        }
        return result;
    }
}
