#!/bin/python

Max=10
content=r'''#if Expr
using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.CompilerServices;
namespace Utils {{
    namespace Expr {{
        public static partial class Checker {{
            public static bool check(bool ret, [CallerLineNumber] int sourceLineNumber = 0, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerArgumentExpression("ret")] string checker=""){{
                if(ret){{
                    return true;
                }} else {{
                    Utils.Logger.logwarn($"{{checker}} 为 {{ret}}， 这导致了 {{memberName}} 不能正常启用\n出错函数位于 {{sourceFilePath}} : line {{sourceLineNumber}}");
                    return false;
                }}
            }}
            public static bool check(System.Collections.Generic.IEnumerable<bool> checkers, [CallerLineNumber] int sourceLineNumber = 0, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerArgumentExpression("checkers")] string checker=""){{
                foreach(var b in checkers){{
                    if(b){{
                        continue;
                    }} else {{
                        Utils.Logger.logwarn($"{{checker}} 为 [{{string.Join(",",checkers)}}]， 这导致了 {{memberName}} 不能正常启用\n出错函数位于 {{sourceFilePath}} : line {{sourceLineNumber}}");
                        return false;
                    }}
                }}
                return true;
            }}
            public static bool check(bool ret, string informations, [CallerLineNumber] int sourceLineNumber = 0, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerArgumentExpression("ret")] string checker=""){{
                if(ret){{
                    return true;
                }} else {{
                    Utils.Logger.logwarn($"{{checker}} 为 {{ret}}， 这导致了 {{informations}} 不能正常启用\n出错函数{{memberName}}位于 {{sourceFilePath}} : line {{sourceLineNumber}}");
                    return false;
                }}
            }}
            public static bool check(System.Collections.Generic.IEnumerable<bool> checkers, string informations, [CallerLineNumber] int sourceLineNumber = 0, [CallerMemberName] string memberName = "", [CallerFilePath] string sourceFilePath = "", [CallerArgumentExpression("checkers")] string checker=""){{
                foreach(var b in checkers){{
                    if(b){{
                        continue;
                    }} else {{
                        Utils.Logger.logwarn($"{{checker}} 为 [{{string.Join(",",checkers)}}]， 这导致了 {{informations}} 不能正常启用\n出错函数{{memberName}}位于 {{sourceFilePath}} : line {{sourceLineNumber}}");
                        return false;
                    }}
                }}
                return true;
            }}
            public static bool[] checker(params bool[] results)=>All(results)?new bool[0]:results;
            static bool All(System.Collections.Generic.IEnumerable<bool> res){{
                foreach(var b in res){{if(!b){{return false;}}}}
                return true;
            }}
        }}
    }}
#nullable enable{checked}
#nullable disable{expr}
}}
#endif
'''
checked_expr=r'''
&&&&namespace {Name} {{{fpgetter}
public static class Met<T> {{{mets}
}}
public static class SMet<T, Instance> {{{smets}
}}{fieldprop}{bodies}
&&&&}}'''
fpgetter=r'''
public static class FP<FieldOrProperty> {
    public static Func<Instance, TOut> field<Instance, TOut>(out Func<Instance, TOut> res)=>Expr.FP<FieldOrProperty>.field(out res) ? res : throw new Exception($"Field {typeof(FieldOrProperty)} does not exist or does not yield {typeof(TOut)} in {typeof(Instance)}.");
    public static Action<Instance, TOut> field<Instance, TOut>(out Action<Instance, TOut> res)=>Expr.FP<FieldOrProperty>.field(out res) ? res : throw new Exception($"Field {typeof(FieldOrProperty)} does not exist in {typeof(Instance)} or it cannot accept {typeof(TOut)}.");
    public static Func<Instance, TOut> prop<Instance, TOut>(out Func<Instance, TOut> res)=>Expr.FP<FieldOrProperty>.prop(out res) ? res : throw new Exception($"Property {typeof(FieldOrProperty)} does not exist or does not yield {typeof(TOut)} in {typeof(Instance)}.");
    public static Action<Instance, TOut> prop<Instance, TOut>(out Action<Instance, TOut> res)=>Expr.FP<FieldOrProperty>.prop(out res) ? res : throw new Exception($"Property {typeof(FieldOrProperty)} does not exist in {typeof(Instance)} or it cannot accept {typeof(TOut)}.");
}
public static class SFP<FieldOrProperty,Instance> {
    public static Func<TOut> field<TOut>(out Func<TOut> res)=>Expr.SFP<FieldOrProperty,Instance>.field(out res) ? res : throw new Exception($"Field {typeof(FieldOrProperty)} does not exist or does not yield {typeof(TOut)} in {typeof(Instance)}.");
    public static Action<TOut> field<TOut>(out Action<TOut> res)=>Expr.SFP<FieldOrProperty,Instance>.field(out res) ? res : throw new Exception($"Field {typeof(FieldOrProperty)} does not exist in {typeof(Instance)} or it cannot accept {typeof(TOut)}.");
    public static Func<TOut> prop<TOut>(out Func<TOut> res)=>Expr.SFP<FieldOrProperty,Instance>.prop(out res) ? res : throw new Exception($"Property {typeof(FieldOrProperty)} does not exist or does not yield {typeof(TOut)} in {typeof(Instance)}.");
    public static Action<TOut> prop<TOut>(out Action<TOut> res)=>Expr.SFP<FieldOrProperty,Instance>.prop(out res) ? res : throw new Exception($"Property {typeof(FieldOrProperty)} does not exist in {typeof(Instance)} or it cannot accept {typeof(TOut)}.");
}'''
bfpgetter=r'''
public static class FP<FieldOrProperty> {
    public static bool field<Instance, TOut>([MaybeNullWhen(false)] out Func<Instance, TOut> res)=>(res=Field<FieldOrProperty,Instance,TOut>.get) is not null;
    public static bool field<Instance, TOut>([MaybeNullWhen(false)] out Action<Instance, TOut> res)=>(res=Field<FieldOrProperty,Instance,TOut>.set) is not null;
    public static bool prop<Instance, TOut>([MaybeNullWhen(false)] out Func<Instance, TOut> res)=>(res=Property<FieldOrProperty,Instance,TOut>.get) is not null;
    public static bool prop<Instance, TOut>([MaybeNullWhen(false)] out Action<Instance, TOut> res)=>(res=Property<FieldOrProperty,Instance,TOut>.set) is not null;
}
public static class SFP<FieldOrProperty,Instance> {
    public static bool field<TOut>([MaybeNullWhen(false)] out Func<TOut> res)=>(res=SField<FieldOrProperty,Instance,TOut>.get) is not null;
    public static bool field<TOut>([MaybeNullWhen(false)] out Action<TOut> res)=>(res=SField<FieldOrProperty,Instance,TOut>.set) is not null;
    public static bool prop<TOut>([MaybeNullWhen(false)] out Func<TOut> res)=>(res=SProperty<FieldOrProperty,Instance,TOut>.get) is not null;
    public static bool prop<TOut>([MaybeNullWhen(false)] out Action<TOut> res)=>(res=SProperty<FieldOrProperty,Instance,TOut>.set) is not null;
}'''
fp=r'''
public static class {Name}<FieldOrProperty,Instance,TOut> {{
    public static Func<{StaticOrInstance}TOut>? get;
    public static Action<{StaticOrInstance}TOut>? set;
    static {Name}() {{{varinstance}
        var f = typeof(Instance).Get{Type}(typeof(FieldOrProperty).Name,(BindingFlags)(-1));
        var val = Expression.Parameter(typeof(TOut));
        if(f is not null && typeof(TOut).IsAssignableFrom(f.{Type}Type){read}){{
            get = Expression.Lambda<Func<{StaticOrInstance}TOut>>(Expression.{Type}({instance},f),{instance}).Compile();
        }} else {{
            get = null;
        }}
        if(f is not null && f.{Type}Type.IsAssignableFrom(typeof(TOut)){write}){{
            set = Expression.Lambda<Action<{StaticOrInstance}TOut>>(Expression.Assign(Expression.{Type}({instance},f),val), {setter}).Compile();
        }} else {{
            set = null;
        }}
    }}
}}'''


varinstance_i="\n        var instance=Expression.Parameter(typeof(Instance));"
varinstance_s=""
StaticOrInstance_i="Instance, "
StaticOrInstance_s=""
instance_i="instance"
instance_s="null"
Type_f="Field"
Type_p="Property"
read_f=""
read_p=" && f.CanRead"
write_f=" && !f.IsInitOnly"
write_p=" && f.CanWrite"

field  =fp.format(Name="Field"    ,Type=Type_f, read=read_f, write=write_f, StaticOrInstance=StaticOrInstance_i, varinstance=varinstance_i, instance=instance_i, setter="instance, val")
prop   =fp.format(Name="Property" ,Type=Type_p, read=read_p, write=write_p, StaticOrInstance=StaticOrInstance_i, varinstance=varinstance_i, instance=instance_i, setter="instance, val")
sfield =fp.format(Name="SField"   ,Type=Type_f, read=read_f, write=write_f, StaticOrInstance=StaticOrInstance_s, varinstance=varinstance_s, instance=instance_s, setter=          "val")
sprop  =fp.format(Name="SProperty",Type=Type_p, read=read_p, write=write_p, StaticOrInstance=StaticOrInstance_s, varinstance=varinstance_s, instance=instance_s, setter=          "val")


body_fun=r'''
public static class {Name}<Method, {NameGenerics}> {{
    public static {Delegate}<{DelegateGenerics}>? {dele};
    static {Name}(){{
        var type = new Type[]{{{parameters}}};
        var parameter = Array.ConvertAll(type,x=>Expression.Parameter(x, x.Name));
        var mi = typeof(Instance).GetMethod(typeof(Method).Name,(BindingFlags)(-1), type[1..]);
        if(mi is not null{check_func_ret}){{
            {dele} = Expression.Lambda<{Delegate}<{DelegateGenerics}>>(Expression.Call({instance}, mi, parameter[1..]),parameter{caller_idx}).Compile();
        }} else {{
            {dele} = null;
        }}
    }}
}}'''

dic=[
{"Name": "Act", "NameGenerics":       "Instance, {Ts}", "Delegate":"Action", "dele":"act", "DelegateGenerics":      "Instance, {Ts}", "instance":"parameter[0]", "parameters":"typeof(Instance), {typeofTs}", "caller_idx":      "", "check_func_ret":""},
{"Name": "Fun", "NameGenerics": "Instance, {Ts}, TOut", "Delegate":"Func"  , "dele":"fun", "DelegateGenerics":"Instance, {Ts}, TOut", "instance":"parameter[0]", "parameters":"typeof(Instance), {typeofTs}", "caller_idx":      "", "check_func_ret":" && typeof(TOut).IsAssignableFrom(mi.ReturnType)"},
{"Name":"SAct", "NameGenerics":       "Instance, {Ts}", "Delegate":"Action", "dele":"act", "DelegateGenerics":                "{Ts}", "instance":        "null", "parameters":"typeof(Instance), {typeofTs}", "caller_idx": "[1..]", "check_func_ret":""},
{"Name":"SFun", "NameGenerics": "Instance, {Ts}, TOut", "Delegate":"Func"  , "dele":"fun", "DelegateGenerics":          "{Ts}, TOut", "instance":        "null", "parameters":"typeof(Instance), {typeofTs}", "caller_idx": "[1..]", "check_func_ret":" && typeof(TOut).IsAssignableFrom(mi.ReturnType)"},
    ]
body_full = "".join(body_fun.replace("{{","{{{{").replace("}}","}}}}").format(**d) for d in dic)
bodies = "".join(body_full.format(Ts=", ".join("T"+str(j) for j in range(1,i+1)), typeofTs=", ".join("typeof(T{})".format(j) for j in range(1,i+1))) for i in range(0,Max)).replace("<, ","<").replace(", ,",",").replace(", }","}").replace(", >",">").replace("<>","")

met="""
    public static Action<Instance, {Ts}> get<Instance, {Ts}>(out Action<Instance, {Ts}> res)=>Expr.Met<T>.get(out res) ? res : throw new Exception($"Instance method {{typeof(T).Name}}({list_bracket_typeof_ts}) does not exist in instance {{typeof(Instance)}}");
    public static Func<Instance, {Ts}, TOut> get<Instance, {Ts}, TOut>(out Func<Instance, {Ts}, TOut> res)=>Expr.Met<T>.get(out res) ? res : throw new Exception($"Instance method {{typeof(TOut)}} {{typeof(T).Name}}({list_bracket_typeof_ts}) does not exist in instance {{typeof(Instance)}}");"""
smet="""
    public static Action<{Ts}> get<{Ts}>(out Action<{Ts}> res)=>Expr.SMet<T ,Instance>.get(out res) ? res : throw new Exception($"Static method {{typeof(T).Name}}({list_bracket_typeof_ts}) does not exist in instance {{typeof(Instance)}}");
    public static Func<{Ts}, TOut> get<{Ts}, TOut>(out Func<{Ts}, TOut> res)=>Expr.SMet<T, Instance>.get(out res) ? res : throw new Exception($"Static method {{typeof(TOut)}} {{typeof(T).Name}}({list_bracket_typeof_ts}) does not exist in instance {{typeof(Instance)}}");"""

bmet="""
    public static bool get<Instance, {Ts}>([MaybeNullWhen(false)] out Action<Instance, {Ts}> res)=>(res=Act<T, Instance, {Ts}>.act) is not null;
    public static bool get<Instance, {Ts}, TOut>([MaybeNullWhen(false)] out Func<Instance, {Ts}, TOut> res)=>(res=Fun<T, Instance, {Ts}, TOut>.fun) is not null;"""
bsmet="""
    public static bool get<{Ts}>([MaybeNullWhen(false)] out Action<{Ts}> res)=>(res=SAct<T, Instance, {Ts}>.act) is not null;
    public static bool get<{Ts}, TOut>([MaybeNullWhen(false)] out Func<{Ts}, TOut> res)=>(res=SFun<T, Instance, {Ts}, TOut>.fun) is not null;"""


mets="".join(met.format(Ts=", ".join("T"+str(j) for j in range(1,i+1)), list_bracket_typeof_ts=", ".join("{{typeof(T{})}}".format(j) for j in range(1,i+1))) for i in range(0,Max)).replace("<, ","<").replace(", ,",",").replace(", }","}").replace(", >",">").replace("<>","")
smets="".join(smet.format(Ts=", ".join("T"+str(j) for j in range(1,i+1)), list_bracket_typeof_ts=", ".join("{{typeof(T{})}}".format(j) for j in range(1,i+1))) for i in range(0,Max)).replace("<, ","<").replace(", ,",",").replace(", }","}").replace(", >",">").replace("<>","")

bmets="".join(bmet.format(Ts=", ".join("T"+str(j) for j in range(1,i+1)), list_bracket_typeof_ts=", ".join("{{typeof(T{})}}".format(j) for j in range(1,i+1))) for i in range(0,Max)).replace("<, ","<").replace(", ,",",").replace(", }","}").replace(", >",">").replace("<>","")
bsmets="".join(bsmet.format(Ts=", ".join("T"+str(j) for j in range(1,i+1)), list_bracket_typeof_ts=", ".join("{{typeof(T{})}}".format(j) for j in range(1,i+1))) for i in range(0,Max)).replace("<, ","<").replace(", ,",",").replace(", }","}").replace(", >",">").replace("<>","")

checked=checked_expr.format(Name="Expr",fpgetter=bfpgetter,mets=bmets,smets=bsmets,fieldprop=field+prop+sfield+sprop,bodies=bodies).replace("\n","\n    ").replace("    &&&&","")
expr=checked_expr.format(Name="FastExpr",fpgetter=fpgetter,mets=mets,smets=smets,fieldprop="",bodies="").replace("\n","\n    ").replace("    &&&&","")
import os
if os.path.exists("UTILS"):
    with open("UTILS/Utils.Expr.python.CS",'w') as f:
        f.write(content.format(checked=checked.replace("\n","\n    "),expr=expr.replace("\n","\n    ")))
