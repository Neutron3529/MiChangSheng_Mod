// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../The Scroll of Taiwu_Data/Managed/System.dll" -r:"../../The Scroll of Taiwu_Data/Managed/netstandard.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Reflection.Emit.ILGeneration.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../The Scroll of Taiwu_Data/Managed/mscorlib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Assembly-CSharp.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Unity.TextMeshPro.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.CoreModule.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.UI.dll" -r:"../../The Scroll of Taiwu_Data/Managed/spine-unity.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.dll" -r:"../../The Scroll of Taiwu_Data/Managed/DOTween.dll" -r:"../../The Scroll of Taiwu_Data/Managed/DOTweenPro.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Assembly-CSharp-firstpass.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.UIModule.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.AudioModule.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.PhysicsModule.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.Physics2DModule.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll" -optimize -deterministic -debug OptimizedFrontend.cs ../UTILS/*.CS *.CS -out:OptimizedFrontend.dll -define:FRONTEND
// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll"
/**
 *  Neutron's Taiwu Collections
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;
using Utils;
using HarmonyLib.Tools;
using DG.Tweening;
using static Utils.Logger;
[assembly: AssemblyVersion("0.0.0.3529")]
namespace CopiedFragment;
[TaiwuModdingLib.Core.Plugin.PluginConfig("CopiedFragment","Neutron3529","0.5.0")]
public partial class CopiedFragment : TaiwuModdingLib.Core.Plugin.TaiwuRemakePlugin {
    public override void Initialize()=>this.HarmonyInstance = new RobustHarmonyInstance(this.GetGuid());
    public override void Dispose()=>this.HarmonyInstance.UnpatchSelf();
    // public static void logverbose(string s)=>logwarn(s);
    public static void logverbose(string s){}
    public static int F01=0;
    public static float F01f=0;
    public static int F02=0;
    public static int F03=0;
    public static int F05=0;
    public static float F05f=0f;
    public static int F06=1000;
    public static string F06s="";
    public RobustHarmonyInstance HarmonyInstance;
    private bool enable(string key){
        try {
            var x=_enable(key);
            // logwarn($"{key}的启用状态为{x}");
            return x;
        } catch(Exception ex){
            logwarn("enable 出错，出错键值为\""+key+"\"，错误原因是"+ex.Message);
            return false;
        }
    }
    private bool _enable(string key){
        bool enable=false;
        if(key=="F01"){
            if(ModManager.GetSetting(this.ModIdStr, key,ref F01) && F01>0){
                F01f=100f/F01;
                return true;
            }else{return false;}
        } else if(key=="F02") {
            return ModManager.GetSetting(this.ModIdStr, key,ref F02) && F02>0;
        } else if(key=="F03") {
            return ModManager.GetSetting(this.ModIdStr, key,ref F03) && F03>0;
        // } else if(key=="F04") {
        //     return ModManager.GetSetting(this.ModIdStr, key,ref F04) && F04>0;
        // } else if(key=="F05") {
        //     return ModManager.GetSetting(this.ModIdStr, key,ref F05) && F05>0;
        } else if(key=="F06") {
            return ModManager.GetSetting(this.ModIdStr, key,ref F06) && ModManager.GetSetting(this.ModIdStr, "F06s",ref F06s) && F06>0 && F06s.Length>1;
        }
        return ModManager.GetSetting(this.ModIdStr, key,ref enable) && enable;
    }
    // static bool F01_enabled=false;
    public override void OnModSettingUpdate(){
        this.HarmonyInstance.UnpatchSelf();
        if(enable("NForceOFF")){return;}
        if(enable("F01")){
            // F01_enabled=true;
            // var carrier=typeof(UI_Adventure).GetField("_doMoveCarrierSpeedRate",(BindingFlags)(-1));
            // if(carrier!=null){
            //     YaUI_AdventureDoMove.carrier=carrier;
            //     this.HarmonyInstance.PatchAll(typeof(YaUI_AdventureDoMove));
            //     // this.HarmonyInstance.PatchAll(typeof(UI_AdventureDoMove));
            // } else {
            //     this.HarmonyInstance.PatchAll(typeof(UI_AdventureDoMove));
            // }
            this.HarmonyInstance.PatchAll(typeof(UI_AdventureInfoMoveRealDuration));
        }
        if(enable("F02"))this.HarmonyInstance.PatchAll(typeof(UI_AdventureDoTrunkUnfoldAnim));
        if(enable("F03")){
            this.HarmonyInstance.PatchAll(typeof(UI_CombatResultOnInit));
            this.HarmonyInstance.PatchAll(typeof(UI_AdventureResultOnInit));
        }
        if(enable("F04")){
            // this.HarmonyInstance.PatchAll(typeof(UI_WorldmapPlayMoveAnim));
            this.HarmonyInstance.PatchAll(typeof(UI_WorldmapCoPlayMoveAnim));
        }
        // if(enable("F05")){
        //     F05f=100f/F05;
        //     this.HarmonyInstance.PatchAll(typeof(ProfessionSkillControllerGetInvokes));
        // }
        if(enable("F06")){
            this.HarmonyInstance.PatchAll(typeof(UI_SkillBreakPlateGenerateAnimationSequence));
            // this.HarmonyInstance.PatchAll(typeof(ProfMain));
        }
        if(enable("F07"))this.HarmonyInstance.PatchAll(typeof(UI_ProfessionMaskAnimSpineByCached));
        if(enable("F08"))this.HarmonyInstance.PatchAll(typeof(UI_ProfessionMask_NoWaitForSeconds));
        try {
            if(enable("F09") && typeof(UI_MainMenu).GetMethod("ShowDevelopingDialog",(BindingFlags)(-1)) is not null)this.HarmonyInstance.PatchAll(typeof(UI_MainMenu_ShowDevelopingDialog));
        } catch (Exception e){
            logwarn("为尊重螺舟展示DevelopingDialog的意愿，这里不再拦截DevelopingDialog的显示，请关闭F09开关以阻止黄字的产生，错误原因（盲猜AmbiguousMethod）：",e);
        }
        if(enable("F10"))this.HarmonyInstance.PatchAll(typeof(UI_PartWorldMapUpdateCheckContinue));
    }
    // [HarmonyPatch(typeof(UI_Adventure), "DoMove")]
    // public static class UI_AdventureDoMove {
    //     public static void Prefix() {
    //         UI_Adventure.DoMoveAnimationTimeScale = 2f / F01 * 100;
    //         YaUI_AdventureDoMove.carrier.SetValue(null,(float)(((float)(YaUI_AdventureDoMove.carrier.GetValue(null)))*F01*0.01f));
    //     }
    // }
    // [HarmonyPatch(typeof(UI_Adventure), "StartMove")]
    // public static class YaUI_AdventureDoMove {
    //     public static FieldInfo carrier=null;
    //     public static void Postfix() {
    //         carrier.SetValue(null,(float)(((float)(carrier.GetValue(null)))*F01*0.01f));
    //     }
    // }
    [HarmonyPatch(typeof(UI_Adventure), "DoTrunkUnfoldAnim")]
    public static class UI_AdventureDoTrunkUnfoldAnim {
        public static void Prefix() {
            // if(F01_enabled){
            //     UI_AdventureInfo.MoveSpeedScale=F01*0.01f;
            // }
            UI_Adventure.UnfoldAnimationTimeScale = 1f / F02 * 100;
        }
    }
    [HarmonyPatch(typeof(UI_CombatResult), "OnInit")]
    public static class UI_CombatResultOnInit {
        public static void Prefix(UI_CombatResult __instance) {
            Spine.Unity.SkeletonGraphic resultAni = __instance.CGet<Spine.Unity.SkeletonGraphic>("ResultAni");
            resultAni.timeScale=F03/100f;
        }
        public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
            instructions = new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(i=>i.opcode==OpCodes.Call && (((MethodInfo)i.operand).Name == "SetDelay" || ((MethodInfo)i.operand).Name == "AppendInterval"))
                ).Repeat( matcher => // Do the following for each match
                    matcher
                    .InsertAndAdvance(
                        new CodeInstruction(OpCodes.Ldc_R4,100f/F03),
                        new CodeInstruction(OpCodes.Mul)
                    ).Advance(1)
                ).InstructionEnumeration();
            return instructions;
        }
    }
    [HarmonyPatch(typeof(UI_AdventureResult), "OnInit")]
    public static class UI_AdventureResultOnInit {
        public static void Prefix(UI_AdventureResult __instance) {
            Spine.Unity.SkeletonGraphic resultAni = __instance.CGet<Spine.Unity.SkeletonGraphic>("ResultAnim");
            resultAni.timeScale=F03/100f;
        }
        public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
            instructions = new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(i=>i.opcode==OpCodes.Call && (((MethodInfo)i.operand).Name == "SetDelay" || ((MethodInfo)i.operand).Name == "AppendInterval"))
                ).Repeat( matcher => // Do the following for each match
                    matcher
                    .InsertAndAdvance(
                        new CodeInstruction(OpCodes.Ldc_R4,100f/F03),
                        new CodeInstruction(OpCodes.Mul)
                    ).Advance(1)
                ).InstructionEnumeration();
            return instructions;
        }
    }
    [HarmonyPatch(typeof(UI_Worldmap), "CoPlayMoveAnim")]
    public static class UI_WorldmapCoPlayMoveAnim {
        static MethodInfo ProcessBlock=typeof(UI_Worldmap).GetMethod("ProcessBlock",(BindingFlags)(-1));
        static MethodInfo ClearMovePath=typeof(UI_Worldmap).GetMethod("ClearMovePath",(BindingFlags)(-1));
        static MethodInfo SetShowingArea=typeof(UI_Worldmap).GetMethod("SetShowingArea",(BindingFlags)(-1));
        static MethodInfo OnMoveAniComplete=typeof(UI_Worldmap).GetMethod("OnMoveAniComplete",(BindingFlags)(-1));
        public static bool Prefix(ref System.Collections.IEnumerator __result, short fromBlockId, short toBlockId, UI_Worldmap __instance, WorldMapModel ____mapModel, bool ____movingByController/*, HashSet<GameData.Domains.Map.Location> ____normalColorBlockSet, HashSet<GameData.Domains.Map.Location> ____waitingUpdateBlockSet*/){
            __result=fast_impl(fromBlockId, toBlockId, __instance, ____mapModel, ____movingByController/*, ____normalColorBlockSet, ____waitingUpdateBlockSet*/);
            return false;
        }
        // public static System.Collections.IEnumerator Postfix(System.Collections.IEnumerator result, short fromBlockId, short toBlockId, UI_Worldmap __instance, WorldMapModel ____mapModel){
        //     var ie=Register.NoWaitForSeconds(result);
        //     while(ie.MoveNext()){/*yield return ie.Current;*/}
        //     __instance.SetPlayerLocationWithRefreshAllBlockVisibility(new GameData.Domains.Map.Location(____mapModel.CurrentAreaId, toBlockId), default(GameData.Domains.Map.Location), -1f, true);
        //     __instance.RefreshAllBlockVisibilityWithForce(true, false, false);
        //     yield break;
        // }
        public static System.Collections.IEnumerator fast_impl(short fromBlockId, short toBlockId, UI_Worldmap that, WorldMapModel ____mapModel, bool ____movingByController/*, HashSet<GameData.Domains.Map.Location> ____normalColorBlockSet, HashSet<GameData.Domains.Map.Location> ____waitingUpdateBlockSet*/){
            ____mapModel.ChangeTaiwuMoveState(WorldMapModel.MoveState.PerformMove);
            GameData.Domains.Map.Location fromLocation = new GameData.Domains.Map.Location(that.PlayerAtBlock.AreaId, fromBlockId);
            GameData.Domains.Map.Location toLocation = new GameData.Domains.Map.Location(that.PlayerAtBlock.AreaId, toBlockId);
            bool eventPerforming = UIElement.EventWindow.Exist;
            bool flag = ____mapModel.ShowingAreaId != that.PlayerAtBlock.AreaId;
            if (flag) {
                SetShowingArea.Invoke(that, new object[]{that.PlayerAtBlock.AreaId});
            }
            // ____waitingUpdateBlockSet.UnionWith(____normalColorBlockSet);
            // 这里将动画yield return掉，以避免多线程问题。我为了加速，直接把DOVirtual.Float换成了我的加速版本，请不要照抄。
            yield return DG_Tweening_DOVirtual.Float(0f, 1f, 0.4f, delegate(float stepVal)
            {
                that.SetPlayerLocationWithRefreshAllBlockVisibility(fromLocation, toLocation, stepVal, false);
            }).WaitForCompletion();
            // float halfMoveInterval = 0.05f;
            // 这里直接return null，意思是剩下的内容放到下一帧计算，不必等待，因此不必定义上面的halfMoveInterval
            // yield return null;
            // ____waitingUpdateBlockSet.UnionWith(____normalColorBlockSet);
            // foreach (GameData.Domains.Map.Location location in ____waitingUpdateBlockSet)
            // {
            //     var block = ____mapModel.GetBlockData(location);
            //     ProcessBlock.Invoke(that, new object[]{block});
            //     block = null;
            // }
            // 反编译出来一个这个，但这段代码没有用，故删去
            // HashSet<GameData.Domains.Map.Location>.Enumerator enumerator = default(HashSet<GameData.Domains.Map.Location>.Enumerator);
            // ____waitingUpdateBlockSet.Clear();
            // 这里直接return null，意思是剩下的内容放到下一帧计算，不必等待
            // 或许这里的等待是不必要的
            yield return null;
            if (eventPerforming || !____movingByController) {
                GameData.GameDataBridge.GameDataBridge.AddMethodCall<GameData.Domains.Map.Location, GameData.Domains.Map.Location>(-1, 2, 8, fromLocation, toLocation);
                ClearMovePath.Invoke(that, null);
            }
            OnMoveAniComplete.Invoke(that, new object[]{fromBlockId});
            yield break;
        }
    }
    [HarmonyPatch(typeof(UI_Worldmap), "PlayMoveAni")]
    public static class UI_WorldmapPlayMoveAnim {
        static readonly FieldInfo _teleportMoving=typeof(UI_Worldmap).GetField("_teleportMoving",(BindingFlags)(-1));
        static readonly MethodInfo ClearMovePath=typeof(UI_Worldmap).GetMethod("ClearMovePath",(BindingFlags)(-1));
        static readonly MethodInfo SelectBlock=typeof(UI_Worldmap).GetMethod("SelectBlock",(BindingFlags)(-1));
        static UI_WorldmapPlayMoveAnim(){
            if(_teleportMoving==null || SelectBlock==null){
                throw new Exception("UI_WorldmapCoPlayMoveAnim出错，导致游戏不能正常取消大地图动画，请联系作者修复。");
            }
        }
        public static void Prefix(UI_Worldmap __instance) {
            _teleportMoving.SetValue(__instance,true);
        }
        public static void Postfix(UI_Worldmap __instance, short fromBlockId, short toBlockId, bool ____movingByController, WorldMapModel ____mapModel) {
            if (UIElement.EventWindow.Exist || !____movingByController) {
                // GameData.GameDataBridge.GameDataBridge.AddMethodCall<GameData.Domains.Map.Location, GameData.Domains.Map.Location>(-1, GameData.Domains.DomainHelper.DomainName2DomainId["Map"], GameData.Domains.Map.MapDomainHelper.MethodName2MethodId["MoveFinish"], new GameData.Domains.Map.Location(__instance.PlayerAtBlock.AreaId, fromBlockId), new GameData.Domains.Map.Location(__instance.PlayerAtBlock.AreaId, toBlockId));
                // ClearMovePath.Invoke(__instance, new object[] { });
                SelectBlock.Invoke(__instance, new object[] {
                    ____mapModel.GetBlockData(new GameData.Domains.Map.Location(__instance.PlayerAtBlock.AreaId, toBlockId))
                });
            }
        }
        // static IEnumerable<MethodBase> TargetMethods(){
        //     // yield return EnumeratorMoveNext(typeof(UI_Worldmap).GetMethod("CoPlayMoveAnim"));
        //     yield return typeof(UI_Worldmap).GetMethod("CoPlayMoveAnim");
        //     // yield return typeof(UI_Worldmap).GetMethod("CoPlayMoveAnim",(BindingFlags)(-1)).ReturnType.GetMethod("MoveNext",(BindingFlags)(-1));
        // }
        // public static readonly Type wfs=typeof(UnityEngine.WaitForSeconds);
        // public static readonly FieldInfo sec=typeof(UnityEngine.WaitForSeconds).GetField("m_Seconds");
        // public static IEnumerable<object> Postfix(IEnumerable<object> result){
        //     foreach(object x in result){
        //         if (x != null && x.GetType()==wfs){
        //             var F04=300;
        //             sec.SetValue(x,100f/F04*(float)sec.GetValue(x));
        //         }
        //         yield return x;
        //     }
        // }
        // public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
        //     instructions = new CodeMatcher(instructions)
        //         .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
        //             new CodeMatch(OpCodes.Ldc_R4,(float)typeof(UI_Worldmap).GetField("MoveStepTime",((BindingFlags)(-1))).GetValue(null))
        //         ).Repeat( matcher => // Do the following for each match
        //             matcher
        //             .SetAndAdvance(
        //                 OpCodes.Ldc_R4,(float)typeof(UI_Worldmap).GetField("MoveStepTime",((BindingFlags)(-1))).GetValue(null)*100f/F04
        //             )
        //         ).InstructionEnumeration();
        //     return instructions;
        // }
    }
    // [HarmonyPatch(typeof(ProfessionSkillController), "GetInvokes", MethodType.Enumerator)]
    // public static class ProfessionSkillControllerGetInvokes {
    //     static IEnumerable<MethodBase> TargetMethods(){
    //         yield return typeof(ProfessionSkillController).GetMethod("GetInvokes",(BindingFlags)(-1)).ReturnType.GetMethod("MoveNext",(BindingFlags)(-1));
    //     }
    //     public static IEnumerable<ValueTuple<ProfessionSkillController.OnceProfessionSkillInvoke, float>> Postfix(IEnumerable<ValueTuple<ProfessionSkillController.OnceProfessionSkillInvoke, float>> result){
    //         foreach(var x in result){
    //             yield return x with {Item2=x.Idtem2*F05f};
    //         }
    //     }
    // }
    // public static class UI_SkillBreakPlateGenerateAnimationSequence {
    //     static IEnumerable<MethodBase> TargetMethods(){
    //         foreach(var x in typeof(UI_SkillBreakPlate).GetMethods((BindingFlags)(-1))){
    //             if(x.Name.Contains("GenerateAnimationSequence")){
    //                 yield return x;
    //             }
    //         }
    //         foreach(var y in typeof(UI_SkillBreakPlate).GetNestedTypes((BindingFlags)(-1))){
    //             foreach(var x in y.GetMethods((BindingFlags)(-1))){
    //                 if(x.Name.Contains("GenerateAnimationSequence")){
    //                     yield return x;
    //                 }
    //             }
    //         }
    //     }
    //     public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
    //         // logverbose("中子debug："+__originalMethod.Name+"的patch正在进行");
    //         Register.speed_factor=100f/F06;
    //         return Register.register(instructions);
    //     }
    // }
    public static class UI_SkillBreakPlateGenerateAnimationSequence {
        static IEnumerable<MethodBase> Targets(Type type){
            foreach(var x in type.GetMethods((BindingFlags)(-1))){
                // if(x.Name.Contains("GenerateAnimationSequence")){
                    yield return x;
                // }
            }
            foreach(var y in type.GetNestedTypes((BindingFlags)(-1))){
                foreach(var x in y.GetMethods((BindingFlags)(-1))){
                    // if(x.Name.Contains("GenerateAnimationSequence")){
                        yield return x;
                    // }
                }
            }
        }
        static IEnumerable<MethodBase> TargetMethods(){
            var ass=typeof(UI_SkillBreakPlate).Assembly;
            foreach(var target in F06s.Split(',')){
                var tf=target.Split('|');
                var type=ass.GetType(tf[0],false,false);
                if(type!=null){
                    // logger($"DEBUG: got type {type}");
                    foreach(var method in Targets(type)){
                        if(method.Name.Contains(tf[1])){
                            logger($"  DEBUG:method {method} optimized");
                            yield return method;
                        }
                    }
                } else {
                    logwarn("找不到"+tf[0]+"因而无法对此进行优化");
                }
            }
        }
        public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
            // logverbose("中子debug：正在patch"+__originalMethod.DeclaringType.Name+"的方法"+__originalMethod.Name);
            Register.speed_factor=100f/F06;
            return Register.register(__originalMethod, instructions);
        }
    }
    [HarmonyPatch(typeof(UI_PartWorldMap),"UpdateCheckContinue")]
    public static class UI_PartWorldMapUpdateCheckContinue {
        public static void Prefix(ref float ____continueWaitingSeconds){
            if (____continueWaitingSeconds>0){____continueWaitingSeconds=0;}
        }
    }
    [HarmonyPatch(typeof(ProfessionSkillController),"MainProcess")]
    public static class ProfMain {
        public static readonly FieldInfo sec=typeof(UnityEngine.WaitForSeconds).GetField("m_Seconds");
        public static System.Collections.IEnumerator Postfix(System.Collections.IEnumerator result){
            while(result.MoveNext()){
                var ret=result.Current;
                if(ret.GetType()==typeof(UnityEngine.WaitForSeconds)){
                    sec.SetValue(ret,(float)sec.GetValue(ret)*Register.speed_factor);
                }
                yield return ret;
            }
        }
    }
    public static class UI_ProfessionMaskAnimSpineByCached {
        [HarmonyPrefix, HarmonyPatch(typeof(UI_ProfessionMask),"AnimSpineByCached")]
        public static bool Prefix1()=>false;
        [HarmonyPrefix, HarmonyPatch(typeof(UI_ProfessionMask),"AnimEffectByCached",new Type[]{typeof(UnityEngine.GameObject), typeof(UnityEngine.Vector2), typeof(UnityEngine.Transform), typeof(Action<UnityEngine.GameObject>)})]
        public static bool Prefix2()=>false;
    }
    public static class UI_ProfessionMask_NoWaitForSeconds {
        static IEnumerable<MethodBase> TargetMethods(){
            foreach(var m in typeof(UI_ProfessionMask).GetMethods((BindingFlags)(-1))){
                if(typeof(System.Collections.IEnumerator).IsAssignableFrom(m.ReturnType)){
                    // logwarn("start yield "+m.Name);
                    yield return m;
                    // logwarn("finish yield "+m.Name);
                }
            }
        }
        public static System.Collections.IEnumerator Postfix(System.Collections.IEnumerator result)=>Register.NoWaitForSeconds(result);
    }
    [HarmonyPatch(typeof(UI_AdventureInfo),"MoveRealDuration",MethodType.Getter)]
    public static class UI_AdventureInfoMoveRealDuration {
        public static float Postfix(float res)=>res*F01f;
    }
    [HarmonyPatch(typeof(UI_MainMenu),"ShowDevelopingDialog")]
    public static class UI_MainMenu_ShowDevelopingDialog {
        public static bool Prefix()=>false;
    }
}
