﻿// echo partial file
#if FRONTEND
using Config;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.Linq;
using static NpcScan.Const;
namespace NpcScan
{
    public static class Utils
    {
        public static WorldMapModel map;
        // public static Dictionary<int,string> map_name_dict;
        public static void update_map(){
            map = SingletonObject.getInstance<WorldMapModel>();
        }
        // public static string[][] item_dict; // init later.
        public static Dictionary<int,string> /*item_dict, */item_dict_nocolor;
        public static void Init(){ // called by Frontend.
            // item_dict = new Dictionary<int,string>();
            // var ass=typeof(GameData.Domains.Item.ItemDomainHelper).GetAssembly();
            // foreach(var str in GameData.Domains.Item.ItemDomainHelper.FieldName2DataId.Keys){
            //     var itembase=ass.GetType("Config."+str);
            //     var itembaseitem=ass.GetType("Config."+str+"Item");
            //     if(itembase!=null && itembaseitem!=null){
            //         if
            //     }
            // }

            item_dict_nocolor = (from i in Weapon.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            (from i in Armor.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            (from i in Accessory.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            (from i in Clothing.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            (from i in Carrier.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            (from i in Config.Material.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            (from i in CraftTool.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            (from i in Food.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            (from i in Medicine.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            (from i in TeaWine.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            (from i in SkillBook.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            (from i in Cricket.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            (from i in Misc.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).ToDictionary(i=>i.Item1&(item_id_mask|(item_type_mask<<item_type_shift)),i=>i.Item2);
            // item_dict = (from i in Weapon.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            // (from i in Armor.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            // (from i in Accessory.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            // (from i in Clothing.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            // (from i in Carrier.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            // (from i in Config.Material.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            // (from i in CraftTool.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            // (from i in Food.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            // (from i in Medicine.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            // (from i in TeaWine.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            // (from i in SkillBook.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            // (from i in Cricket.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).Concat
            // (from i in Misc.Instance select (ToItem(i.Grade, i.ItemType, i.TemplateId), i.Name)).ToDictionary(i=>i.Item1&(item_id_mask|(item_type_mask<<item_type_shift)),i=>$"<color={Colors.Instance.GradeColors[i.Item1>>item_grade_shift].ColorToHexString()}>{i.Item2}</color>");
        }
        public static string GetMariageStatu(int marriage)
        {
            if (marriage == 0)
                return "未婚";
            else if (marriage == 1)
                return "已婚";
            else
                return "丧偶";
        }

        public static string GetItemName(int item, bool color=false) {
            string str;
            if(item_dict_nocolor.TryGetValue(item&(item_id_mask|(item_type_mask<<item_type_shift)),out str)){return str;}else{return "未知物品\n"+item.ToString("X8");}
        }

        public static string GetQualificationGrowth(int age, int growthType)
        {
            AgeEffectItem ageEffectItem = AgeEffect.Instance[Mathf.Min(age, AgeEffect.Instance.Count - 1)];
            int num = 0;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Clear();
            switch (growthType)
            {
                case 0:
                    stringBuilder.Append(LocalStringManager.Get("LK_Qualification_Growth_Average"));
                    num = ageEffectItem.SkillQualificationAverage;
                    break;
                case 1:
                    stringBuilder.Append(LocalStringManager.Get("LK_Qualification_Growth_Precocious"));
                    num = ageEffectItem.SkillQualificationPrecocious;
                    break;
                case 2:
                    stringBuilder.Append(LocalStringManager.Get("LK_Qualification_Growth_LateBlooming"));
                    num = ageEffectItem.SkillQualificationLateBlooming;
                    break;
            }
            if (num > 0)
            {
                stringBuilder.Append($"+{num}".SetColor("lightblue"));
            }
            else if (num == 0)
            {
                stringBuilder.Append("+0".SetColor("lightgrey"));
            }
            else
            {
                stringBuilder.Append($"{num}".SetColor("red"));
            }
            return stringBuilder.ToString();
        }

        public static string SetColor(string input, Color color)
        {
            return "<color=" + color.ColorToHexString() + ">" + input + "</color>";
        }
    }
}
#endif
