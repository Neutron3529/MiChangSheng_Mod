﻿// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../The Scroll of Taiwu_Data/Managed/System.dll" -r:"../../The Scroll of Taiwu_Data/Managed/netstandard.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Reflection.Emit.ILGeneration.dll" -r:"../../The Scroll of Taiwu_Data/Managed/mscorlib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Assembly-CSharp.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Unity.TextMeshPro.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.CoreModule.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.UI.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.InputLegacyModule.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Newtonsoft.Json.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.AssetBundleModule.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.UIModule.dll" -optimize -deterministic -debug NPCScan-Frontend.cs ../UTILS/*.CS *.CS -out:NPCScan-Frontend.dll -debug -unsafe -define:CPY -define:NO_HARMONY -define:FRONTEND # -define:SPEEDTEST -define:Json

using NpcScan.Controller;
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using TaiwuModdingLib.Core.Plugin;
using UnityEngine;
using static Utils.Logger;
using static Utils.Cpy;

namespace NpcScan
{
    [PluginConfig("NPCScan-Copy", "Neutron3529, 发射的熟鸡蛋", "0.2")]
    public class MainEntry : TaiwuRemakePlugin
    {
        GameObject gameObject;
        public static GameData.Domains.Mod.ModInfo modInfo;
        public static KeyCode mainFormKey = KeyCode.F3;

        public override void Dispose()
        {
            if (gameObject != null)
                UnityEngine.Object.Destroy(gameObject);
        }
        public static string modIdStr;
        public override void Initialize()
        {
            modIdStr=ModIdStr;
            ModManager.GetSetting(ModIdStr, "ROW_MAX", ref NpcScan.Const.ROW_MAX);
            // System.Diagnostics.Stopwatch w=System.Diagnostics.Stopwatch.StartNew();
            if (!ABResourceManager.IsInit)
                ABResourceManager.Init(ModIdStr);
            // w.Stop();
            // UnityEngine.Debug.LogError($"fe abinit cost {w.ElapsedMilliseconds}");
            // w.Restart();
            Utils.Init();
            // w.Stop();
            // UnityEngine.Debug.LogError($"fe init_util cost {w.ElapsedMilliseconds}");
            // w.Restart();
            ((Dictionary<string, GameData.Domains.Mod.ModInfo>)(typeof(ModManager).GetField("_localMods",(BindingFlags)(-1))).GetValue(null)).TryGetValue(this.ModIdStr, out modInfo);
            { // check dependencies
                int i=0;
                foreach(var mod in ModManager.EnabledMods)if(mod.FileId == 2884609294)i++;
                if(i>1)logwarn("……不知道为什么前端的通信Mod多于一个，理论上不影响游戏的使用，但……这种事情谁说得清楚呢？");
                if(i<1)logwarn("前端未发现通信Mod，请启用中子的原生前后端通信Mod并重启太吾绘卷以保证Mod正常工作。如果你一定要继续游戏，请不要点击NpcScan的“更新数据”按钮，否则游戏几乎必然红字。");
            }
            if (gameObject == null)
            {
                gameObject = new GameObject("NpcScanEmpty");
                gameObject.AddComponent<NpcScanController>();
                UnityEngine.Object.DontDestroyOnLoad(gameObject);
                BindKeyCode();
            }
            // w.Stop();
            // UnityEngine.Debug.LogError($"fe unity and warning_gen cost {w.ElapsedMilliseconds}");
        }

        public override void OnModSettingUpdate() {
            ((Dictionary<string, GameData.Domains.Mod.ModInfo>)(typeof(ModManager).GetField("_localMods",(BindingFlags)(-1))).GetValue(null)).TryGetValue(this.ModIdStr, out modInfo);
            BindKeyCode();
            if(!ModManager.GetSetting(ModIdStr, "Scale", ref View.result_scale)){View.result_scale=100;}
            ModManager.GetSetting(ModIdStr, "Attraction", ref View.Attraction);
        }

        public void BindKeyCode()
        {
            string Key = "F3";

            if (ModManager.GetSetting(ModIdStr, "MainFormKey", ref Key) && Enum.TryParse(Key, out KeyCode keycode)) {
                NpcScan.MainEntry.mainFormKey = keycode;
            }
        }
    }
}
