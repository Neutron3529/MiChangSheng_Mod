// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../Backend/System.dll" -r:"../../Backend/System.Collections.dll" -r:"../../Backend/mscorlib.dll" -r:"../../Backend/netstandard.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../Backend/System.Private.CoreLib.dll" -r:"../../Backend/System.Linq.dll" -r:"../../Backend/System.Runtime.dll" -r:"../../Backend/System.Text.Json.dll" -r:"../../Backend/System.Text.Encodings.Web.dll" -r:"../../Backend/System.Threading.Tasks.Parallel.dll" -r:"../../Backend/System.Collections.Concurrent.dll" -unsafe -deterministic NeutronPseudoEvent-DataDumper.cs DataDumper.cs ../UTILS/*.CS -out:NeutronPseudoEvent-DataDumper.dll -define:BACKEND -define:NO_HARMONY -define:export_NameList -debug #-define:TEST_DUP #-optimize
// -r:"../../Backend/System.IO.FileSystem.dll"

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法

// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll" -r:"../../Backend/System.Runtime.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。

#define export_NameList

using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using static Utils.Logger;

using Config;
using Config.EventConfig;
using GameData.Domains.TaiwuEvent;
using GameData.Domains.TaiwuEvent.Enum;


namespace DataDumper;
public partial class Backend : EventPackage {
    public static string ModId {get=>"1_2947737828";}
    public Backend() {
        // logger("into DataDumper");
        this.NameSpace = "Taiwu.Event.Neutron3529.DataDumper";
        this.Author = "Neutron3529";
        this.Group = "DataDumper";
#if TEST_DUP
        this.EventList = new List<TaiwuEventItem>{new BypassEvent("test","4e657574-726f-6e20-3335-323900020000")}; // test only
#else
        this.EventList = new List<TaiwuEventItem>();
        (new DataDumper()).Initialize(ModId);
#endif
    }
}
#if TEST_DUP
public class BypassEvent:TaiwuEventItem {
    public BypassEvent(string name, string guid){
        this.EventGroup = name;
        this.Guid = Guid.Parse(guid);
        this.IsHeadEvent = true;
        this.EventType = EEventType.ModEvent;
        this.TriggerType = EventTrigger.NewGameMonth;
        this.EventOptions = new TaiwuEventOption[0];
    }
    public override bool OnCheckEventCondition(){
        logwarn("checking condition");
        return true;
    }
    public override void OnEventEnter()=>GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ToEvent(string.Empty);
    public override void OnEventExit(){}
    public override string GetReplacedContentString()=>string.Empty;
}
#endif
