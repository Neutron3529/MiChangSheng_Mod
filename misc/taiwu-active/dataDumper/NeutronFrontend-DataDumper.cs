// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../The Scroll of Taiwu_Data/Managed/System.dll" -r:"../../The Scroll of Taiwu_Data/Managed/netstandard.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Reflection.Emit.ILGeneration.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../The Scroll of Taiwu_Data/Managed/mscorlib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Assembly-CSharp.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Unity.TextMeshPro.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.CoreModule.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.UI.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Newtonsoft.Json.dll" -optimize -unsafe -deterministic -debug NeutronFrontend-DataDumper.cs DataDumper.cs ../UTILS/*.CS -define:export_NameList -out:NeutronFrontend-DataDumper.dll
// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#define export_NameList

using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using System.Linq;
#if !BACKEND
using Newtonsoft.Json;
#endif

using static Utils.Logger;

using Config;
using Config.ConfigCells.Character;

namespace DataDumper;
using Utils;
using static Utils.Logger;
[TaiwuModdingLib.Core.Plugin.PluginConfig("DataDumper","Neutron3529","0.1.0")]

public class Frontend : TaiwuModdingLib.Core.Plugin.TaiwuRemakePlugin {
    public override void Dispose(){}
    public override void Initialize()=>(new DataDumper()).Initialize(this.ModIdStr);
}
