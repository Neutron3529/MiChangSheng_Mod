/** @param {NS} ns **/
export async function main(ns) {
	if (ns.stanek.activeFragments().length == 0) return
	while (1) {
		for (let f of ns.stanek.activeFragments())
			if (f.id<100){
				await ns.stanek.chargeFragment(f.x,f.y)
			}
	}
}