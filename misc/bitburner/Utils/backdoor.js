/** @param {NS} ns */
export async function main(ns) {
  /*prevent arrays being passed by reference, 
    pass by value instead: array1.push( JSON.parse( JSON.stringify( array2 ) ) )*/

  ns.disableLog("ALL");
  ns.tail();

  var scannedServers;
  var server;
  var homePaths = [["home"]];
  var homePath = JSON.parse(JSON.stringify(homePaths[0]));
  var newNode;
  var path;
  var pathDuplicate;
  var pathNode;
  var nodeDuplicate;
  var terminalInput = eval('document.getElementById( "terminal-input" )'); /*bypass document*/
  var terminalHandler = Object.keys(terminalInput)[1];
  ns.atExit(() => {
    terminalInput = eval('document.getElementById( "terminal-input" )');
    if (terminalInput === null) /*exit script if focus is not on terminal window*/ {
      return
    }
    terminalInput.value = "home";
    terminalHandler = Object.keys(terminalInput)[1];
    terminalInput[terminalHandler].onChange({ target: terminalInput });
    terminalInput[terminalHandler].onKeyDown({ key: 'Enter', preventDefault: () => null });

    ns.tprint("Backdoors completed");
  })
  /*create an array of all paths where each path is also an array,
     all paths are from "home" to each server, example:
     [["home"], ["home", "n00dles"], ["home", "n00dles", "zer0"], ["home ", "foodnstuff"]]
  This structure will make terminal input easier.
  Example: terminal:>connect home;connect n00dles;connect zer0;
  Also, connect home; works from any server which make these paths universal*/

  while (homePath.length !== 0) {
    scannedServers = ns.scan(homePath[homePath.length - 1]);
    newNode = false;
    for (server of scannedServers) {
      nodeDuplicate = false;
      for (pathNode of homePath) {
        if (JSON.stringify(pathNode) === JSON.stringify(server)) {
          nodeDuplicate = true;
          break;
        }
      }

      if (nodeDuplicate === false) {
        homePath.push(server);
        pathDuplicate = false;
        for (path of homePaths) {
          if (JSON.stringify(path) === JSON.stringify(homePath)) {
            pathDuplicate = true;
            break;
          }
        }

        if (pathDuplicate === false) {
          homePaths.push(JSON.parse(JSON.stringify(homePath)));
          newNode = true;
          break;
        }
        else {
          homePath.pop();
        }
      }
    }

    if (newNode === false) {
      homePath.pop();
    }
  }
  var first = true;
  homePaths.sort(
    (x, y) =>
      ((x.length << 16) | ns.getServer(x[x.length - 1]).requiredHackingSkill)
      - ((y.length << 16) | ns.getServer(y[y.length - 1]).requiredHackingSkill)
  )
  for (path of homePaths) /*install backdoors on servers not owned which have root access*/ {
    server = ns.getServer(path[path.length - 1]);
    if (!server.backdoorInstalled && !server.purchasedByPlayer &&
      server.hasAdminRights && (server.requiredHackingSkill <= ns.getHackingLevel())) {
      terminalInput = eval('document.getElementById( "terminal-input" )');
      if (terminalInput === null) /*exit script if focus is not on terminal window*/ {
        ns.exit();
      }
      terminalHandler = Object.keys(terminalInput)[1];
      var z
      if (path.length >= 2 && ns.getServer(path[path.length - 2]).backdoorInstalled) {
        z = `connect ${path[path.length - 2]}; connect ${path[path.length - 1]}`;
      } else {
        z = "";
        for (pathNode of path) {
          z += "connect " + pathNode + ";"
        }
      }
      ns.tprint(`current depth=${path.length}, command=${z}`);
      terminalInput.value = z
      terminalInput[terminalHandler].onChange({ target: terminalInput });
      if (first) {
        await ns.sleep(100);
        first = false;
      }
      terminalInput[terminalHandler].onKeyDown({ key: 'Enter', preventDefault: () => null });
      await ns.sleep(100);

      terminalInput = eval('document.getElementById( "terminal-input" )');
      if (terminalInput === null) {
        ns.exit();
      }
      terminalInput.value = "backdoor";
      terminalInput[terminalHandler].onChange({ target: terminalInput });
      terminalInput[terminalHandler].onKeyDown({ key: 'Enter', preventDefault: () => null });
      /*backdoortime = hacktime / 4*/
      await ns.sleep(Math.ceil(ns.getHackTime(server.hostname) / 4) + 500);
    }
  }
}