#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin
#   Copyright (C) 2022 Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put this file and `utils.cs` in `steamapps`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

export GAME_NAME="${0%\.cs}"                                # might modify if the name mismatch.
export GAME_DIR="$GAME_NAME"                                # might be modified, but "$GAME_NAME" cover most of the cases.

export FILE_NAME="$0"
export ASSEMBLY="Timberborn"                           # might be modified
export UTILS="utils.cs"                                     # might be modified if you do not put utils.cs in the current dir.
export PLUGIN_ID="Neutron3529.Cheat"                        # should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # should be modified
export GAME_BASE_DIR="common/$GAME_DIR"                     # should modify GAME_DIR instead since GAME_DIR == GAME_NAME is almost always true.

export IFS=$'\n' # to disable the annoying space.
export DOTNET="dotnet" # the location of the DOTNET executable file.
[ -z "$DOTNET_CSC_DLL" ] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll

case $1 in
  V)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
  v)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
  VERBOSE) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
  verbose) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
  D)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
  d)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
  DEBUG)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
  debug)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
  *)       EXTRA_DEFINE=""                                               ;;
esac

_MODE__SELECT_=100

( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Harmony.dll" \
  `[ -e "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll\""` \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.Core.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.AIModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.CoreModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.UI.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/mscorlib.dll" \
  $(for i in "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
  -optimize $EXTRA_DEFINE \
  - $UTILS && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
  git add ${FILE_NAME}
  case $2 in
    R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
    *) git commit -am "$2" ;;
  esac
  git push
fi
exit





using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;
using Neutron3529;

namespace %%NAMESPACE_ID%%
{
    [BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
    public partial class Cheat : ModEntry {
        public Cheat():base("%%PLUGIN_ID%%"){}
        public override void Awake() {
            base.Awake();
            // 主要逻辑放在`utils.cs`中，这里的Awake只是为了以防万一
            // 目前来说，这个函数的唯一用途是用来“叮”……
            // 就像这样：
            logger("叮~修改器启动，请安心游戏");
        }
        [HarmonyDebug]
        [HarmonyPatch(typeof(Timberborn.BonusSystem.BonusManager), "Multiplier")]
        public class BonusManagerMultiplier : Entry {
            [Desc("额外工作属性乘数")]
            public static float val=5;
            public static float Postfix(float value)=>value*val;
        }

        [HarmonyDebug]
        public class NavigationDistance_Many : Entry {
            static IEnumerable<MethodBase> TargetMethods() {// used for several Distance methods.
                // var x=typeof(Timberborn.Navigation.NavigationDistance);
                // yield return x.GetProperty("Limited").GetGetMethod();
                // yield return x.GetProperty("ResourceBuildings").GetGetMethod();
                // yield return x.GetProperty("DistrictRoad").GetGetMethod();
                foreach(var x in typeof(Timberborn.Navigation.NavigationDistance).GetProperties()){
                    if(x.GetGetMethod().ReturnType==typeof(float)){
                        yield return x.GetGetMethod();
                    }
                }
            }
            [Desc("额外距离乘数")]
            public static float val=5;
            public static float Postfix(float value)=>value*val;
        }
        [HarmonyDebug]
        public class Int_Many : Entry {
            static IEnumerable<MethodBase> TargetMethods() {// used for several Distance methods.
                yield return typeof(Timberborn.Navigation.NavigationDistance).GetProperty("DistrictTerrain").GetGetMethod();
                yield return typeof(Timberborn.Carrying.GoodCarrier).GetProperty("LiftingCapacity").GetGetMethod();
            }
            [Desc("额外建设乘数与负重")]
            public static float val=5;
            public static int Postfix(int value)=>(int)(value*val);
        }
        [HarmonyDebug]
        [HarmonyPatch(typeof(Timberborn.ConstructionSites.BuildExecutor), "Tick")]
        public class BuildExecutorTick : Entry {
            [Desc("额外建筑速度乘数")]
            public static float val=45;
            public static void Prefix(ref float deltaTimeInHours)=>deltaTimeInHours*=val;
        }
        [HarmonyDebug]
        // [HarmonyPatch(typeof(Timberborn.WorkSystem.Worker), "SpeedMultiplier",MethodType.Setter)]
        [HarmonyPatch(typeof(Timberborn.WorkSystem.Worker), "WorkingSpeedMultiplier",MethodType.Setter)]
        public class WorkerSpeedMultiplier : Entry {
            [Desc("额外工作速度乘数")]
            public static float val=45;
            public static void Prefix(ref float value)=>value*=val;
        }
        [HarmonyDebug]
        [HarmonyPatch(typeof(Timberborn.NeedSpecifications.NeedSpecification), "DailyDelta",MethodType.Getter)]
        public class NeedSpecificationDailyDelta : Entry {
            // static HashSet<string> Id=new();
            [Desc("日常消耗系数")]
            public static float val=-0.01f;
            [Desc("异常恢复速率")]
            public static float val2=100f;
            public static float Postfix(float value, Timberborn.NeedSpecifications.NeedSpecification __instance){
                // if(Id.Add(__instance.Id)){
                //     logger($"got new `{__instance.Id}`: AffectsWellbeing:{__instance.AffectsWellbeing}, CharacterType:{__instance.CharacterType}, DailyDelta:{__instance.DailyDelta}, Effectiveness:{__instance.Effectiveness}, Order:{__instance.Order}, StartingValue:{__instance.StartingValue}, HideWhenInactive:{__instance.HideWhenInactive}, Wastable:{__instance.Wastable}");
                // }
                if(__instance.HideWhenInactive){
                    return val2;
                } else {
                    return value*val;
                }
            }
        }
        [HarmonyDebug]
        [HarmonyPatch(typeof(Timberborn.Growing.Growable), "Awake")]
        public class GrowableAwake : Entry {
            [Desc("生长时间系数")]
            public static float val=0.01f;
            static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
                logger("%%PLUGIN_ID%%-正在注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法");
                instructions = new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                              new CodeMatch(OpCodes.Ldarg_0),
                              new CodeMatch(i=> i.opcode==OpCodes.Ldfld && ((FieldInfo)i.operand).Name == "_growthTimeInDays")
                ).Repeat( matcher => // Do the following for each match
                matcher
                .Advance(2)
                .InsertAndAdvance(
                    new CodeInstruction(OpCodes.Ldc_R4,val),
                                  new CodeInstruction(OpCodes.Mul)
                )
                .Advance(1) // avoid conflict.
                ).InstructionEnumeration();
                return instructions;
            }
        }

        //         [HarmonyDebug]
        //         [HarmonyPatch(typeof(Timberborn.GoodStackSystem.GoodStack), "Enable", new Type[]{typeof(Timberborn.Goods.GoodAmount)})]
        //         public class GoodStackEnable { // not use.
        //             public static bool Prefix(ref Timberborn.Goods.GoodAmount goodAmount) {
        //                 goodAmount.Amount*=good_mul;
        //                 return true;
        //             }
        //         }
        //         [HarmonyDebug]
        //         [HarmonyPatch(typeof(Timberborn.Yielding.Yielder), "Initialize")]
        //         public class YielderInitialize {
        //             public static bool Prefix(ref Timberborn.Goods.GoodAmount yield) {
        //                 yield.Amount*=good_mul;
        //                 return true;
        //             }
        //         }
    }
}
