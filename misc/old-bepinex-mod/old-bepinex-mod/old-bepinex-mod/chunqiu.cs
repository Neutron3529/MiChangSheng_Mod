#!/bin/bash -e
#   * compile instructions: put this file in
#   *         `steamapps/common/Dyson Sphere Program/CustomMod`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

if [ -z "$__DOTNET_CSC" ]; then
    export __DOTNET_CSC="`find /usr/share/dotnet -type f -name dotnet` `find /usr/share/dotnet -name csc.dll`"
    echo '$'"__DOTNET_CSC not set yet, you should execute"
    echo "export __DOTNET_CSC='$__DOTNET_CSC'"
    echo "manually, or this alert will occur each time you execute this script."
fi

__MODE_VERBOSE=84 # is the line number of "#define VERBOSE", may be modified
__MODE_DEBUG__=$((__MODE_VERBOSE+1))
__MODE_RELEASE=$((__MODE_DEBUG__+1))

case $1 in
    V)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    v)       _MODE__SELECT_=$__MODE_VERBOSE     ;;
    VERBOSE) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    verbose) _MODE__SELECT_=$__MODE_VERBOSE     ;;
    D)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    d)       _MODE__SELECT_=$__MODE_DEBUG__     ;;
    DEBUG)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    debug)   _MODE__SELECT_=$__MODE_DEBUG__     ;;
    *)       _MODE__SELECT_=$__MODE_RELEASE     ;;
esac

# export GAME_NAME="${0%\.cs}" # now, file name should be equals to ${GAME_NAME}.cs, the benefit is, we could only change the name, rather than change filename and $GAME_NAME.
# export GAME_NAME="$(basename `pwd`)" # now, file name should be equals to ${GAME_NAME}.cs, the benefit is, we could only change the name, rather than change filename and $GAME_NAME.
export GAME_NAME="ChunQiu"
export FILE_NAME="$0"
export GAME_DIR="$GAME_NAME"                                # might be modified
export ASSEMBLY="Assembly-CSharp"                           # might be modified
export PLUGIN_ID="Neutron3529.Cheat"                        # should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # should be modified
export GAME_BASE_DIR="common/ChunQiu/ChunQiu"

( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_ ) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $__DOTNET_CSC -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Harmony.dll" \
  `[ -e "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll\""` \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.Core.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.AIModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.CoreModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.UI.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/mscorlib.dll" \
  $(for i in `printf "%q " ${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/$ASSEMBLY*.dll` ; do echo -r:"$i" ; done) \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
  -optimize `[ "$_MODE__SELECT_" == "$__MODE_DEBUG__" ] && echo -debug` \
  - && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
        R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        *) git commit -am "$2" ;;
    esac
    git push
fi
exit

#define VERBOSE // the line of __MODE_VERBOSE
#define DEBUG



using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace %%NAMESPACE_ID%%
{
    [BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
    public class Cheat : BaseUnityPlugin {
#if DEBUG
        public static Action<string> logger;
#else
        public static void logger(string s){}
#endif
        void Start() {
            var harmony=new Harmony("%%PLUGIN_ID%%");
#if DEBUG
            logger=Logger.LogInfo;
#endif
            logger("叮~");
            if(Config.Bind("config", "all_happy", true, "修改满赋税的惩罚").Value){
                harmony.PatchAll(typeof(IncomeACostStart));
                harmony.PatchAll(typeof(IncomeACostShowIncomeACost));
                logger("all_happy patched");
            }
            if(Config.Bind("config", "BuildACityTimeGoes", true, "玩家建城加速").Value){
                harmony.PatchAll(typeof(BuildACityTimeGoes));
                harmony.PatchAll(typeof(BuildACityDecideOrNot));
                logger("BuildACityTimeGoes patched");
            }
            if(Config.Bind("config", "ArmySystemShowEAA", true, "玩家战车变强").Value){
                harmony.PatchAll(typeof(ArmySystemShowEAA));
                harmony.PatchAll(typeof(ArmySystemArmyTowards));
                logger("ArmySystemShowEAA patched");
            }
            logger("您的修改器已启动");
        }
        [HarmonyPatch(typeof(IncomeACost), "Start")]
        class IncomeACostStart {
            static void Postfix(IncomeACost __instance) {
                __instance.tianfuSlider.onValueChanged.AddListener(delegate(float A_1){
                    if (__instance.tianfuSlider.value >= 1f) {
                    __instance.yingxiangStability = 300;
                    __instance.yingxiangGrowth = 150;
                    __instance.fushuiInfluenceT.text = "当前赋税影响：稳定度+300，出生率+0.150%";
                    }
                });
            }
        }
        [HarmonyPatch(typeof(IncomeACost), "ShowIncomeACost")]
        class IncomeACostShowIncomeACost {
            static void Postfix(IncomeACost __instance) {
                if (__instance.tianfuSlider.value >= 1f) {
                    __instance.yingxiangStability = 300;
                    __instance.yingxiangGrowth = 150;
                    __instance.fushuiInfluenceT.text = "当前赋税影响：稳定度+300，出生率+0.150%";
                }
            }
        }
        [HarmonyPatch(typeof(BuildACity), "TimeGoes")]
        class BuildACityTimeGoes {
            static bool Prefix(BuildACity __instance) {
                __instance.needBuildTime = 1;
                __instance.needprepareTime = 1;
                return true;
            }
        }
        [HarmonyPatch(typeof(BuildACity), "DecideOrNot")]
        class BuildACityDecideOrNot {
            static void Postfix(BuildACity __instance, GameObject theCity) {
                __instance.confirmONotButton.transform.GetChild(0).gameObject.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(delegate(){
                    if (theCity.GetComponent<CityController>().cityscale != 0)
                    {
                        return;
                    }
                    __instance.needBuildTime = 1;
                    __instance.needprepareTime = 1;
                    __instance.TimeGoes();
                    __instance.TimeGoes();
                });
            }
        }
        [HarmonyPatch(typeof(ArmySystem), "ShowEAA")]
        class ArmySystemShowEAA {
            static void Postfix(ArmySystem __instance) {
                logger("trigger ShowEAA");
                foreach(var zhanche in __instance.allZhancheObjs){
                    if(zhanche.activeSelf){
                        var che=zhanche.GetComponent<ZhancheController>();
                        if (che.owner == ChooseCountry.p){
                            che.health = 10000;
                            che.force += 10000;
                            che.force = Mathf.Min(1000000,che.force);
                            che.agentt.speed = 0.05f;
                        }
                    }
                }
            }
        }
        [HarmonyPatch(typeof(ArmySystem), "ArmyTowards")]
        class ArmySystemArmyTowards {
            static void Postfix(ArmySystem __instance) {
                logger("trigger ArmyTowards");
                foreach(var zhanche in __instance.allZhancheObjs){
                    if(zhanche.activeSelf){
                        var che=zhanche.GetComponent<ZhancheController>();
                        if (che.owner == ChooseCountry.p){
                            che.health = 10000;
                            che.agentt.speed = 0.05f;
                        }
                    }
                }
            }
        }
    }
}

