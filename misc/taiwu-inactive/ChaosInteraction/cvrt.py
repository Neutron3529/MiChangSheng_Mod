#!/bin/python
from sys import argv
from csv import reader
def cvrt(human_readable='Events.tsv',machine_friendly='Events.python.tsv',checks=('option','jumpto')):
    with open(human_readable) as f:r=[[r for r in r] for r in reader(f,delimiter='\t')]
    check_idx=[r[0].index(c) for c in checks]
    content_idx=r[0].index('content')
    token_idx=r[0].index('token')
    enter_idx=r[0].index('enter')
    esc_index=r[0].index('esc_index')
    r=r[1:]
    for item in r:
        item[token_idx]=item[token_idx].strip()
        item[content_idx]=item[content_idx].strip().replace('\n','\\n')
        for idx in (enter_idx,*check_idx):
            item[idx]=item[idx].replace('\n',',').strip()
        cnt=[item[idx].count(',') for idx in check_idx]
        if len(cnt)>0 and max(cnt)!=min(cnt):
            print(f"checks failed for item: cnt={cnt}")
            print(item)
            return
        if item[token_idx][:8]=='CondJump' and item[enter_idx].lower()!='dispatcher' :
            print(f"checks failed for item: token=`{item[token_idx]}` starts with CondJump but event ({item[enter_idx]}) is not `dispatcher`")
            print(item)
            return
        if item[content_idx]=='' and item[enter_idx]=='' :
            print(f"checks failed for item: both content and enter for token=`{item[token_idx]}` is empty.")
            print(item)
            return
        int(item[esc_index])
    with open(machine_friendly,'w') as f:f.writelines("\t".join(r)+'\n' for r in r)

if __name__=='__main__':
    if len(argv)>1:human_readable=argv[1]
    else:human_readable='Events.tsv'
    if len(argv)>2:machine_friendly=argv[2]
    else:machine_friendly='Events.python.tsv'
    cvrt(human_readable, machine_friendly)
