// echo aux function only, should not compile alone.
// -r:"../../Backend/System.IO.FileSystem.dll"

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法

// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll" -r:"../../Backend/System.Runtime.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using static Utils.Logger;

using Config.EventConfig;
using GameData.Domains.TaiwuEvent;
using GameData.Domains.TaiwuEvent.Enum;

namespace Neutron3529.ChaosEvents {
    public partial class ChaosEvents : EventPackage {
        public 富商.富商 E01(){
            var o=new 富商.富商();
            var taiwuEvent = GameData.Domains.DomainManager.TaiwuEvent.GetEvent("fb38f657-6ed0-41e4-a0c2-c82afb49762f"); // @人物互动-恩义互动
            var lst=taiwuEvent.EventConfig.EventOptions.ToList();
            for(int i=0;i<o.EventOptions.Length;i++){
                lst.Insert(lst.Count-1, o.EventOptions[i]);
            }
            taiwuEvent.EventConfig.EventOptions=lst.ToArray();
            return o;
        }
    }
    namespace 富商 {
        public class 富商 : EventContainer {
            public static string[] names={"服牛帮", "文山书海阁", "五湖商会", "大武魁商号", "回春堂", "公输坊", "奇货斋"};
            public 富商() :base("Prof.富商", "70b4d45d-ba15-423d-b94e-b40e348eb9c1"){
                this.EventOptions = new TaiwuEventOption[7];
                for(int i=0;i<7;i++){
                    var op=new TaiwuEventOption {
                        OptionKey = "Option_3529010"+(i+1).ToString(),
                        OnOptionSelect = new Func<string>(this.OnOptionSelect(i)),
                        OnOptionVisibleCheck = new Func<bool>(this.OnCheckOptionCondition(i)),
                        OnOptionAvailableCheck = new Func<bool>(this.OnCheckEventCondition),
                        GetReplacedContent = new Func<string>(this.Desc(i)),
                        GetExtraFormatLanguageKeys = new Func<List<string>>(this.GetExtraFormatLanguageKeys)
                    };
                    op.SetContent($"（向<color=#deadbeff>{names[i]}</color>写推荐信......）");
                    this.EventOptions[i]=op;
                }
            }

            private Func<bool> OnCheckOptionCondition(int i){
                return ()=>{
                    var pd=GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetCurrentProfessionData();
                    if(pd!=null && pd.TemplateId == 15 && pd.IsSkillUnlocked(1)){
                        var character = this.EventOptions[i].ArgBox.GetCharacter("CharacterId");
                        return character!=null && GameData.Domains.TaiwuEvent.EventHelper.EventHelper.CanInteractWithType(character, 4);
                    }
                    return false;
                };
            }
            private Func<string> OnOptionSelect(int i){
                return ()=>WriteLetter(i);
            }
            private string WriteLetter(int index){
                var ArgBox=this.EventOptions[index].ArgBox;
                var context=GameData.Domains.DomainManager.TaiwuEvent.MainThreadDataContext;
                int charId=0;
                if(ArgBox.Get("CharacterId", ref charId)){
                    GameData.Domains.DomainManager.Extra.SetMerchantCharToType(charId,(sbyte)index,context);
                }
                return "4e2e0a45-f2e1-4713-9a10-cf3ab4bca36b";
            }
        }
    }
}
