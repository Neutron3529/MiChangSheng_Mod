// echo aux function only, should not compile alone.
// -r:"../../Backend/System.IO.FileSystem.dll"

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法

// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll" -r:"../../Backend/System.Runtime.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using static Utils.Logger;

using Config.EventConfig;
using GameData.Domains.TaiwuEvent;
using GameData.Domains.TaiwuEvent.Enum;

namespace Neutron3529.ChaosEvents {
    public partial class ChaosEvents : EventPackage {
        public 云游僧.云游僧 E08(){
            var o=new 云游僧.云游僧();
            {
                var taiwuEvent = GameData.Domains.DomainManager.TaiwuEvent.GetEvent("fb38f657-6ed0-41e4-a0c2-c82afb49762f"); // @人物互动-恩义互动
                var lst=taiwuEvent.EventConfig.EventOptions.ToList();
                for(int i=0;i<o.EventOptions.Length;i++){
                    lst.Insert(lst.Count-1, o.EventOptions[i]);
                }
                taiwuEvent.EventConfig.EventOptions=lst.ToArray();
            }
            return o;
        }
    }
    namespace 云游僧 {
        public class 云游僧 : EventContainer {
            public 云游僧() :base("Prof.云游僧", "70b4d45d-ba15-423d-b94e-b40e348eb9c8"){
                this.EventOptions = new TaiwuEventOption[2];//诵经祈福/传道说法
                for(int i=0;i<this.EventOptions.Length;i++){
                    var op=new TaiwuEventOption {
                        OptionKey = "Option_352908"+(i+1).ToString("02"),
                        OnOptionSelect = new Func<string>(this.OnOptionSelect(i)),
                        OnOptionVisibleCheck = new Func<bool>(this.OnCheckOptionCondition(i)),
                        OnOptionAvailableCheck = new Func<bool>(this.OnCheckEventCondition),
                        GetReplacedContent = new Func<string>(this.Desc(i)),
                        GetExtraFormatLanguageKeys = new Func<List<string>>(this.GetExtraFormatLanguageKeys)
                    };
                    this.EventOptions[i]=op;
                }
            }
            private Func<bool> OnCheckOptionCondition(int index){
                switch(index){
                    default: return ()=>{
                        var pd=GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetCurrentProfessionData();
                        var character=this.EventOptions[index].ArgBox.GetCharacter("CharacterId");
                        return character!=null && pd!=null && pd.TemplateId == 12 && pd.IsSkillUnlocked(1);
                    };
                }
            }
            public static short DefKey_ProtectedByPrayer=typeof(Config.CharacterFeature.DefKey).GetField("ProtectedByPrayer",(BindingFlags)(-1))==null?Config.CharacterFeature.DefKey.ProtectedByPrayer:(short)typeof(Config.CharacterFeature.DefKey).GetField("ProtectedByPrayer",(BindingFlags)(-1)).GetValue(null);
            private unsafe Func<string> OnOptionSelect(int index){
                switch(index){
                    case 0: return ()=>{
                        GameData.Domains.TaiwuEvent.EventHelper.EventHelper.AddFeature(this.EventOptions[index].ArgBox.GetCharacter("CharacterId"),DefKey_ProtectedByPrayer,true);
                        return Goto("一月平安，二月平安，三月平安\n四月平安，五月平安，六月平安\n七月平安，八月平安，九月平安\n十月平安，十一月平安，十二月平安\n\n……好人一生平安……\n\n<color=#gray>（<Character key=RoleTaiwu str=Name/>长颂平安经，向<Character key=CharacterId str=Name/>致以最真挚的祝福）</color>","（意犹未满……）",index);
                    };
                    case 1: return ()=>{
                        var character=this.EventOptions[index].ArgBox.GetCharacter("CharacterId");
                        var bt=GameData.Domains.DomainManager.Taiwu.GetTaiwu().GetBehaviorType();
                        var blck=GameData.Domains.DomainManager.Map.GetBlock(character.GetLocation());
                        GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ChangeRoleBehaviorType(character,bt);
                        if(blck!=null && blck.CharacterSet!=null){
                            var cid=character.GetId();
                            foreach(var x in blck.CharacterSet){
                                if(x!=cid && GameData.Domains.DomainManager.Character.TryGetElement_Objects(x, out var ch)){
                                    GameData.Domains.TaiwuEvent.EventHelper.EventHelper.ChangeRoleBehaviorType(ch,bt);
                                }
                            }
                        }
                        return Goto($"「如是我闻，北冥有鱼，其名为鲲。鲲之大，大不过葛立恒数也……」\n\n<color=#gray>（<Character key=RoleTaiwu str=Name/>开坛讲法，劝戒<Character key=CharacterId str=Name/>以及周围一干人等，要做像<Character key=RoleTaiwu str=Name/>一样<color=#deadbeff>{Config.BehaviorType.Instance[bt].Name}</color>的人）</color>","（意犹未满……）",index);
                    };
                    default: return ()=>{return string.Empty;};
                }
            }
            public override Func<string> Desc(int index){
                switch(index){
                    case 0: return ()=>"（诵经祈福……）";
                    case 1: return ()=>"（开坛讲法……）";
                    default: return ()=>"";
                }
            }
        }
    }
}
