return {
    Title = "奸商本色",
    BackendPlugins = {"../Backend.dll"},
    Author = "Neutron3529",
--workshop_only    FileId = 2880033630,
--workshop_only    Source = 1,
    Description = "有BUG报https://gitee.com/Neutron3529/MiChangSheng_Mod/issues\n这个Mod的唯一功能是恢复茶马帮的奸商行为。\n传下去：茄子被夺权了\n\n这是一个比较脏的Mod，估计过两天还需要更新一次，因为跟茄子讨论了一下茶马帮，决定把茶马帮的逻辑改成，不按商品最高价，而是按顺序出售物品。由于这里只是强制RemoveAt(0)，所以日后多半需要重新修改一次设置。",
    NeedRestartWhenSettingChanged = false,
    Dependencies = {},
    DefaultSettings = {
        {Key="On", DisplayName="召唤奸商", Description="恢复茶马帮的奸商行为", SettingType="Toggle", DefaultValue=true},
        {Key="N01", DisplayName="逆向删除", Description="优先拿出最后一个，而不是第一个物品，与西域商人交换商品", SettingType="Toggle", DefaultValue=true},
    },
    TagList = {"Modifications"},
}
