// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../Backend/System.dll" -r:"../../Backend/System.Collections.dll" -r:"../../Backend/mscorlib.dll" -r:"../../Backend/netstandard.dll" -r:"../../Backend/GameData.dll" -r:"../../Backend/Redzen.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../Backend/System.Private.CoreLib.dll" -r:"../../Backend/System.Runtime.dll" -r:"../../Backend/System.Linq.dll" -r:"../../Backend/System.ComponentModel.Primitives.dll" -unsafe -optimize -deterministic Plugins.cs ../UTILS/*.CS -out:Plugins.dll -debug -define:BACKEND -define:NO_HARMONY

//! 编译方法，目前来说只能是自己改上面那行字，把缺的东西都补齐，之后送命令提示符或者用python脚本编译。
//! windows就是一坨shit……连通配符都不支持……不然我至少能写出类似 dotnet C:\Program Files\dotnet\sdk\*\Roslyn\bincore\csc.dll 这样的语法
//! 这是一个示例Mod，用途是展示事件Mod的标准写法

/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022-2024 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
//! 这是一个自带文档注释的Mod，虽然代码很shit，但里面有不少注释，或许会对之后准备玩过月逻辑的人有帮助。
//! 所有文档注释会用//!开头，请注意搜索
//! 首先是前后端，前端对后端的调用会先被塞进 ProcessMethodCall，之后分发给对应domain的CallMethod，由CallMethod进行进一步的分发。借助这一点，我们可以快速找到前端调用究竟是在使用哪个函数。
using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using System.ComponentModel;
using Utils;
using static Utils.Logger;

namespace Neutron3529.开战特效;
public static class 开战特效 {
    [DisplayName("void_太吾武器发挥锁定999")]
    public static void LockPerformance(){
        if(GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetGlobalArgBox().Contains<bool>("Neutron.战鼓")){
            logwarn($"effect already created");
        } else {
            GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetGlobalArgBox().Set("Neutron.战鼓",true);
            var id=EventBuff.CreateBuffForChar(GameData.Domains.DomainManager.Taiwu.GetTaiwuCharId());
            // logwarn($"effect {id} created");
        }
    }
    public class EventBuff : GameData.Domains.SpecialEffect.SpecialEffectBase {
        public static long CreateBuffForChar(int characterId)=>GameData.Domains.DomainManager.SpecialEffect.Add(GameData.Domains.DomainManager.TaiwuEvent.MainThreadDataContext, new EventBuff(characterId));
        public EventBuff (int characterId) : base(characterId, 9) { // 0=沛然诀，9=this，100=太祖长拳，因而大概率不会产生冲突
            GameData.DomainEvents.Events.RegisterHandler_CombatSettlement(this.OnCombatSettlement);
            this.AffectDatas = new (){
                {new GameData.Domains.SpecialEffect.AffectedDataKey(characterId, 157,-1), GameData.Domains.SpecialEffect.EDataModifyType.Add},
                {new GameData.Domains.SpecialEffect.AffectedDataKey(characterId, 158,-1), GameData.Domains.SpecialEffect.EDataModifyType.Add},
                {new GameData.Domains.SpecialEffect.AffectedDataKey(characterId, 82,-1), GameData.Domains.SpecialEffect.EDataModifyType.Custom}
            };
        }
        public override int GetModifyValue(GameData.Domains.SpecialEffect.AffectedDataKey dataKey, int currModifyValue) {
            if(dataKey.CharId == this.CharacterId){
                return 100;
            } else {
                return 0;
            }
        }
        public override int GetModifiedValue(GameData.Domains.SpecialEffect.AffectedDataKey dataKey, int currModifyValue) {
            if(dataKey.CharId == this.CharacterId){
                return 999;
            } else {
                return 0;
            }
        }
        public void OnCombatSettlement(GameData.Common.DataContext context, sbyte combatStatus){
            if(GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetGlobalArgBox().Contains<bool>("Neutron.战鼓")){
                GameData.Domains.TaiwuEvent.EventHelper.EventHelper.GetGlobalArgBox().Remove<bool>("Neutron.战鼓");
                // logwarn($"effect {this.Id} removed");
            } else {
                logwarn($"但`Neutron.战鼓`在effect {this.Id}被移除之前清空");
            }
            GameData.DomainEvents.Events.UnRegisterHandler_CombatSettlement(this.OnCombatSettlement);
            GameData.Domains.DomainManager.SpecialEffect.Remove(context,this.Id);
        }
    }
}
