// echo partial file, should not compile along

using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;
using Utils;
namespace GongfaEffect;
using GameData.Utilities;
public partial class GongfaEffect {
//! 虽然这里只是洗头小妹mod，但我们可以很轻松愉快地借用此处逻辑控制各种过月行为，比如在PeriAdvanceMonth_ExecuteGeneralAction搞一个追着太吾指点武功的行为。
    public static class TaiYuanYuNvGong {
//! 首先，请允许我介绍一下partial class这个神器。这个神器允许我们把一个大类拆成很多个小文件。用的时候把文件拼起来就可以了。
//! 这里需要注意一点，写作Config.PersonalNeed.DefKey.MakeLove的这个东西是一个常数，而这个常数随时会被茄子改掉，所以需要用反射，在启动Mod时更新这些常数。
        public static sbyte AgeGroup_Adult=GameData.Domains.Character.AgeGroup.Adult;
        public static short DefKey_VirginityTrue = Config.CharacterFeature.DefKey.VirginityTrue;
        public static short DefKey_VirginityFalse = Config.CharacterFeature.DefKey.VirginityFalse;
        public static short DefKey_Pregnant = Config.CharacterFeature.DefKey.Pregnant;
        public const short Defkey_TaiYuanYuNvGong=54; // 目前这个方法并没有被暴露在defkey之外，因此只能如此使用。
        public static void Patch(RobustHarmonyInstance HarmonyInstance, string ModIdStr){
            DefKey_VirginityTrue=(short)typeof(Config.CharacterFeature.DefKey).GetField("VirginityTrue",(BindingFlags)(-1)).GetValue(null);
            DefKey_VirginityFalse=(short)typeof(Config.CharacterFeature.DefKey).GetField("VirginityFalse",(BindingFlags)(-1)).GetValue(null);
            DefKey_Pregnant=(short)typeof(Config.CharacterFeature.DefKey).GetField("Pregnant",(BindingFlags)(-1)).GetValue(null);
            AgeGroup_Adult=(sbyte)typeof(GameData.Domains.Character.AgeGroup).GetField("Adult",(BindingFlags)(-1)).GetValue(null);
            HarmonyInstance.PatchAll(System.Reflection.MethodBase.GetCurrentMethod().ReflectedType);
        }
//! 这里跟冰清玉洁一样，我们需要选择合适的注入位置。比如FixedAction就很不错，这是针对每一个人的，太吾并不能幸免。
        public static MethodInfo AddElement_PregnantStates=typeof(GameData.Domains.Character.CharacterDomain).GetMethod("AddElement_PregnantStates",(BindingFlags)(-1));
        [HarmonyPrefix]
        [HarmonyPatch(typeof(GameData.Domains.Character.Character),"PeriAdvanceMonth_UpdateStatus")]
        public static unsafe void Prefix(GameData.Domains.Character.Character __instance, GameData.Common.DataContext context) {
            GameData.Domains.CombatSkill.CombatSkill cs;
            if(GameData.Domains.DomainManager.CombatSkill.TryGetElement_CombatSkills(new GameData.Domains.CombatSkill.CombatSkillKey(__instance.GetId(), Defkey_TaiYuanYuNvGong),out cs) // 要练玉女功的
                && __instance.GetGender() == 0 // 要妹子
                && __instance.GetAgeGroup()>=AgeGroup_Adult // 小孩子搞这个容易走火入魔
                && context.Random.CheckPercentProb((int)(100f*GameData.Domains.DomainManager.World.GetProbAdjustOfCreatingCharacter()))
                && !__instance.GetFeatureIds().Contains(DefKey_Pregnant)
            ){
                GameData.Domains.DomainManager.LifeRecord.GetLifeRecordCollection().AddMakeLoveIllegal(__instance.GetId(),GameData.Domains.DomainManager.World.GetCurrDate(),__instance.GetId(),__instance.GetLocation());
                __instance.AddFeature(context,DefKey_Pregnant,true);
                int num;
                if (GameData.Domains.DomainManager.Character.TryGetElement_PregnancyLockEndDates(__instance.GetId(), out num)){
                    GameData.Domains.DomainManager.Character.RemovePregnantLock(context, __instance.GetId());
                }
                var state=new GameData.Domains.Character.PregnantState(__instance,__instance,true);
                state.ExpectedBirthDate = GameData.Domains.DomainManager.World.GetCurrDate() + 5;
                state.IsHuman = true;
                if ( cs.GetDirection() == GameData.Domains.CombatSkill.CombatSkillDirection.Reverse ) {
                    __instance.AddFeature(context,DefKey_VirginityFalse,true);
                    state.CreateMotherRelation=false;
                } else if ( cs.GetDirection() == GameData.Domains.CombatSkill.CombatSkillDirection.Direct ) {
                    __instance.AddFeature(context,DefKey_VirginityTrue,true);
                }
                AddElement_PregnantStates.Invoke(GameData.Domains.DomainManager.Character ,new object[]{__instance.GetId(), state, context});
            }
        }
    }
}
