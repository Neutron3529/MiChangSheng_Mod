return {GameVersion="99.99.99.99",
    Title = "<color=#888800>平衡</color>与<color=#008888>不平衡</color>的模组集",
    --workshop_only    Cover = "Cover.PNG", --  "FareyThueMorse.png",
    --workshop_only    FileId = 2953362351, -- 两个减号是注释，在第一次上传创意工坊的时候，需要把这一行注释掉，因为第一次上传需要获取一个新的FileId
    --workshop_only    Source = 1, -- 用于标注Mod是否为本地Mod，如果你想下载Mod到本地文件夹，需要注释掉这一行
    Version = 0, -- 2022<<32+0925,
    BackendPlugins = {},
    --BackendPatches = {},
    FrontendPlugins = {"../NeutronLoader.dll"}, -- 前端Mod，这个前端Mod可以很好地防止前端无法加载重名Mod。理论上你不必特地引用这个Mod，但以防万一的话，引用一下保证大家加载的是正确的mod，或许挺重要的
    --FrontendPatches = {},
    --EventPackages = {},
    Author = "Neutron3529", -- 你可以在这里添加你的名字。AGPL-v3不会阻止这样的行为。
    NeedRestartWhenSettingChanged = false,
    Dependencies = {},
    HasArchive = false,
    TagList = {"Modifications", "Extensions", "Display"},
    Description = "此Mod按AGPL-v3发布，任何人可以遵循AGPL-v3再分发基于这个Mod的任何修改（比如把这个Mod拆包成小Mod）\n目前Mod的更新会自动处理无效Patch（Mod会因此汇报黄字）\n发现Mod有BUG或者黄字时可以在[url=https://steamcommunity.com/linkfilter/?url=gitee.com/Neutron3529/MiChangSheng_Mod/issues/]Gitee[/url]提交BUG。\n\n请注意，这个Mod的设计目标是，加载重名Mod，并使用自动化技术减少填写DefaultSettings的需求\n目前只完成了重名Mod加载，对前端显示的修改仍在施工中。",
    -- --[[
    DefaultSettings = {}
    --]]--
}
