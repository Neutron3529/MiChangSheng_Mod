// echo aux function only, should not compile alone.
// cp Utils.CS .. && cd $_ && for i in `\ls` ; do \cp -f Utils.CS $i/ ; done ; rm Utils.CS && cd "$OLDPWD"
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;
using static Utils.Logger;
namespace Utils {
    public static class Logger {
        public static void logger(string s)=>GameData.Utilities.AdaptableLog.Info(s);
        public static void logwarn(string s)=>GameData.Utilities.AdaptableLog.Warning("<color=#FFFF00>"+s+"</color>",true);
        public static void logerror(string s)=>GameData.Utilities.AdaptableLog.Error(s); // should not use.
        public static void logex(Exception ex)=>logwarn(_strexception(ex,0)); // should not use.
        static string _strexception(Exception ex, int depth){
            var padding=depth==0?"\n":"\n"+new string(' ',depth*4);
            var content=("\n"+ex.GetType().Name+": "+ex.Message+"\n"+ex.StackTrace).Replace("\n",padding);
            if(ex.InnerException !=null){
                return content+_strexception(ex.InnerException,depth+1);
            } else {
                return content;
            }
        }
    }
    public class RobustHarmonyInstance {
        public Harmony Harmony;
        public RobustHarmonyInstance(string s)=>this.Harmony=new Harmony(s);
        public void PatchAll(Type t){try{this.Harmony.PatchAll(t);}catch(Exception ex){logwarn("\n"+this.Harmony.Id+"在加载时出错，错误信息为"+ex.Message+"\n"+ex.StackTrace);}}
        public void UnpatchSelf()=>this.Harmony.UnpatchSelf();
    }
}
