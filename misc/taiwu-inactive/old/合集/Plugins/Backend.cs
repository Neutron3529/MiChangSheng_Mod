// dotnet "C:\Program Files\dotnet\sdk\6.0.400\Roslyn\bincore\csc.dll" -nologo -t:library -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Collections.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\0Harmony.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\mscorlib.dll" -r:"..\..\..\Backend\GameData.dll" -r:"..\..\..\Backend\Redzen.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\TaiwuModdingLib.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Private.CoreLib.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Runtime.dll" -unsafe -optimize -deterministic Backend.cs -out:Backend.dll
// -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Mono.Cecil.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Core.dll"   -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Composition.AttributedModel.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Runtime.dll" 
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
 // Backend: ProcessMethodCall -> CallMethod -> ...
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;

namespace BetterWeapon;
[TaiwuModdingLib.Core.Plugin.PluginConfig("BetterWeapon","NobodyCares","0.1.1")]
public class BetterWeapon : TaiwuModdingLib.Core.Plugin.TaiwuRemakeHarmonyPlugin {
	public override void Initialize() {
		this.HarmonyInstance.PatchAll(typeof(AllFeature));
		this.HarmonyInstance.PatchAll(typeof(Speedup));
		this.HarmonyInstance.PatchAll(typeof(MakeSpeedup));
		this.HarmonyInstance.PatchAll(typeof(WeaponModifier));
		this.HarmonyInstance.PatchAll(typeof(LuckyShow));
		this.HarmonyInstance.PatchAll(typeof(BreakPlus));
		this.HarmonyInstance.PatchAll(typeof(TransNext));
		this.HarmonyInstance.PatchAll(typeof(TaiwuDomainUpdateCombatSkillBookReadingProgress));
		this.HarmonyInstance.PatchAll(typeof(ZhouBaPi));
		this.HarmonyInstance.PatchAll(typeof(FastPractice));
		this.HarmonyInstance.PatchAll(typeof(RevealMap));
		this.HarmonyInstance.PatchAll(typeof(AllYellowPerk));
		this.HarmonyInstance.PatchAll(typeof(AlwaysCapture));
		this.HarmonyInstance.PatchAll(typeof(CiTang_Backend_NoAuthCost));
	}
	[HarmonyPatch(typeof(GameData.Domains.Building.BuildingDomain),"TeachSkill")]
	public class CiTang_Backend_NoAuthCost {
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Ldc_I4_1),
					new CodeMatch(OpCodes.Add),
					new CodeMatch(OpCodes.Conv_U2),
					new CodeMatch(OpCodes.Ldarg_1),
					new CodeMatch(OpCodes.Call,typeof(GameData.Domains.Building.BuildingDomain).GetMethod("SetShrineBuyTimes"))
				).Repeat( matcher => // Do the following for each match
					matcher
					.SetAndAdvance(
						OpCodes.Ldc_I4_0,null
					)
				).InstructionEnumeration();
			return instructions;
		}
	}
	[HarmonyPatch(typeof(GameData.Domains.Map.MapDomain),"GetNeighborBlocks")]
	public class RevealMap {
		public static void Prefix(ref int maxSteps){
			maxSteps=Math.Max(maxSteps,5);
		}
	}
	[HarmonyPatch(typeof(GameData.Domains.Building.BuildingDomain),"GetMakeItems")]
	public class WeaponModifier {
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(i=> i.opcode==OpCodes.Call && ((MethodInfo)i.operand).Name == "GetElement_MakeItemDict")
				).Repeat( matcher => // Do the following for each match
					matcher
					.Advance(1)
					.InsertAndAdvance(
						new CodeInstruction(OpCodes.Call,typeof(WeaponModifier).GetMethod("MZhujianGA"))
					)
				).InstructionEnumeration();
			return instructions;
		}
		public static unsafe GameData.Domains.Building.MakeItemData MZhujianGA(
			GameData.Domains.Building.MakeItemData makeItemData
		){
			fixed(short*p=makeItemData.MaterialResources.Items){
				for(int i=0;i<6;i++){
					*(p+i)=(short)Math.Min((int)(*(p+i)) << 8,32000);
				}
			}
			return makeItemData;
		}
	}
	public class Speedup{
		[HarmonyPrefix]
		[HarmonyPatch(typeof(GameData.Domains.Taiwu.TaiwuDomain),"GetBaseReadingSpeed")]
		public static bool Prefix(ref sbyte __result){
			__result=100;
			return false;
		}
		[HarmonyPostfix]
		[HarmonyPatch(typeof(GameData.Domains.Taiwu.TaiwuDomain),"GetReadingSpeedBonus")]
		public static int Postfix(int res){
			return Math.Max(res*50,600);
		}
	}
	[HarmonyPatch(typeof(GameData.Domains.Building.BuildingDomain),"StartMakeItem")]
	public class MakeSpeedup{
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Ldfld,typeof(Config.MakeItemSubTypeItem).GetField("Time"))
				).Repeat( matcher => // Do the following for each match
					matcher.SetAndAdvance(
						OpCodes.Pop,null
					).InsertAndAdvance(new CodeInstruction(OpCodes.Ldc_I4_0))
				).InstructionEnumeration();
			return instructions;
		}
	}
	[HarmonyPatch(typeof(GameData.Domains.Taiwu.TaiwuDomain),"InitSkillBreakPlate")]
	public class LuckyShow {
		public static void Postfix(GameData.Domains.Taiwu.SkillBreakPlate plate){
			for(int row=0;row<plate.Grids.Length;row++)for(int col=0;col<plate.Grids[row].Length;col++){
				plate.Grids[row][col].SuccessRateFix=100;
				plate.Grids[row][col].State=Math.Max(plate.Grids[row][col].State,(sbyte)0);
			}
		}
	}
	[HarmonyPatch(typeof(GameData.Domains.Taiwu.TaiwuDomain),"InitSkillBreakPlate")]
	public class TransNext{
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Ldloc_S), // match method 2
					new CodeMatch(OpCodes.Callvirt,typeof(Redzen.Random.IRandomSource).GetMethod("Next",new Type[]{typeof(int)})),
					new CodeMatch(OpCodes.Ldloc_S), // match method 2
					new CodeMatch(i=>i.opcode==OpCodes.Callvirt&&((MethodInfo)i.operand).Name=="get_Count")
				).Repeat( matcher => // Do the following for each match
					matcher.SetAndAdvance(
						OpCodes.Ldc_I4_1,null
					)
				).InstructionEnumeration();
			return instructions;
		}
	}
	
	public class AllYellowPerk {
		[HarmonyTranspiler]
		[HarmonyPatch(typeof(GameData.Domains.Taiwu.TaiwuDomain),"InitSkillBreakPlate")]
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(i=>i.opcode==OpCodes.Ldfld && ( (FieldInfo)i.operand==typeof(Config.SkillBreakPlateItem).GetField("PlateWidth") || (FieldInfo)i.operand==typeof(Config.SkillBreakPlateItem).GetField("PlateHeight") ))
				).Repeat( matcher => // Do the following for each match
					matcher.Advance(1).InsertAndAdvance(
						new CodeInstruction(OpCodes.Ldc_I4_2),
						new CodeInstruction(OpCodes.Add)
					)
				).InstructionEnumeration();
			return instructions;
		}
		[HarmonyPrefix]
		[HarmonyPatch(typeof(GameData.Domains.CombatSkill.CombatSkillDomain),"GetBonusBreakGrids")]
		public static bool Prefix(ref List<Config.BreakGrid> __result,short skillTemplateId, sbyte behaviorType){
			Config.SkillBreakGridListItem configData = Config.SkillBreakGridList.Instance[skillTemplateId];
			switch (behaviorType)
			{
			case 0:
				__result = configData.BreakGridListJust;
				break;
			case 1:
				__result = configData.BreakGridListKind;
				break;
			case 2:
				__result = configData.BreakGridListEven;
				break;
			case 3:
				__result = configData.BreakGridListRebel;
				break;
			case 4:
				__result = configData.BreakGridListEgoistic;
				break;
			default:
				__result = null;
				return false;
			}
			__result = new List<Config.BreakGrid>(__result);
			foreach(List<Config.BreakGrid> list in new List<Config.BreakGrid>[]{configData.BreakGridListJust,configData.BreakGridListKind,configData.BreakGridListEven,configData.BreakGridListRebel,configData.BreakGridListEgoistic}){
				foreach(Config.BreakGrid item in list){
					bool flag=true;
					for(int i=0;i<__result.Count;i++){
						if(__result[i].BonusType==item.BonusType){
							if(__result[i].GridCount<item.BonusType){
								__result[i]=item;
							}
							flag=false;
							break;
						}
					}
					if(flag){
						__result.Add(item);
					}
				}
			}
			return false;
		}
	}
	[HarmonyPatch(typeof(GameData.Domains.Taiwu.TaiwuDomain),"UpdateCombatSkillBookReadingProgress")]
	public class TaiwuDomainUpdateCombatSkillBookReadingProgress{
		public static void Postfix(GameData.Domains.Taiwu.TaiwuDomain __instance, GameData.Common.DataContext context, GameData.Domains.Item.SkillBook book){
			short skillTemplateId=book.GetCombatSkillTemplateId();
			GameData.Domains.Taiwu.TaiwuCombatSkill taiwuCombatSkill = (GameData.Domains.Taiwu.TaiwuCombatSkill) typeof(GameData.Domains.Taiwu.TaiwuDomain).GetMethod("GetTaiwuCombatSkill",(BindingFlags)(-1)).Invoke(__instance, new object[]{skillTemplateId});
			for(byte i=0;i<15;i++){
				typeof(GameData.Domains.Taiwu.TaiwuDomain).GetMethod("SetCombatSkillPageComplete",(BindingFlags)(-1)).Invoke(__instance, new object[]{context, book,i});
				typeof(GameData.Domains.Taiwu.TaiwuDomain).GetMethod("OfflineAddReadingProgress",(BindingFlags)(-1)).Invoke(__instance, new object[]{taiwuCombatSkill,i,1000});
			}
		}
	}
	[HarmonyPatch(typeof(GameData.Domains.Combat.CombatDomain),"CalcLootItem")]
	public class ZhouBaPi{
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Call,typeof(GameData.Utilities.RedzenHelper).GetMethod("CheckPercentProb"))
				).Repeat( matcher => // Do the following for each match
					matcher.InsertAndAdvance(
						new CodeInstruction(OpCodes.Pop),
						new CodeInstruction(OpCodes.Ldc_I4_S,100)
					).Advance(1)
				).InstructionEnumeration();
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Call,typeof(Math).GetMethod("Min",new Type[]{typeof(int),typeof(int)}))
				).Repeat( matcher => // Do the following for each match
					matcher.SetAndAdvance(
						OpCodes.Pop,null
					)
				).InstructionEnumeration();
			return instructions;
		}
	}
	[HarmonyPatch(typeof(GameData.Domains.Character.Character),"GenerateRandomBasicFeatures")]
//	[HarmonyPatch(typeof(GameData.Domains.Character.Character),"OfflineCreateProtagonistRandomFeatures")]
	public class AllFeature {
//		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
//			return Transpilers.MethodReplacer(instructions,typeof(GameData.Domains.Character.Character).GetMethod("GenerateRandomBasicFeatures",(BindingFlags)(-1)),typeof(AllFeature).GetMethod("MG"));
//		}
		public static unsafe bool Prefix(Dictionary<short, short> featureGroup2Id, bool isProtagonist, bool allGoodBasicFeatures, ref GameData.Domains.Character.LifeSkillShorts ____baseLifeSkillQualifications, ref GameData.Domains.Character.CombatSkillShorts ____baseCombatSkillQualifications, ref GameData.Domains.Character.MainAttributes ____baseMainAttributes,  ref GameData.Domains.Character.LifeSkillShorts ____lifeSkillQualifications, ref GameData.Domains.Character.CombatSkillShorts ____combatSkillQualifications){
			if(!(isProtagonist || allGoodBasicFeatures)){
				return true;
			}
			fixed(short*ptr=____baseLifeSkillQualifications.Items)for (int i = 0; i < 16; i++){
				*(ptr+i)=1000;
			}
			fixed(short*ptr=____baseCombatSkillQualifications.Items)for (int i = 1; i < 14; i++){ // avoid finger check
				*(ptr+i)=Math.Max(*(ptr+i),(short)95);
			}
			fixed(short*ptr=____baseMainAttributes.Items)for (int i = 0; i < 6; i++){
				*(ptr+i)=100;
			}
			____lifeSkillQualifications=____baseLifeSkillQualifications;
			____combatSkillQualifications=____baseCombatSkillQualifications;
			var alist=new List<short>();
			foreach (Config.CharacterFeatureItem item in ((IEnumerable<Config.CharacterFeatureItem>)Config.CharacterFeature.Instance)) {if (item != null) {
					if (item.Basic) {
						if (item.CandidateGroupId == 0) {
							alist.Add(item.TemplateId);
						} else if (item.CandidateGroupId == 1) {
							//for (int k = 0; k < (int)item.AppearProb; k++)
							//{
							//	CharacterDomain._normalNegativeBasicFeaturesPool.Add(item.TemplateId);
							//}
							//for (int l = 0; l < (int)item.ProtagonistAppearProb; l++)
							//{
							//	CharacterDomain._protagonistNegativeBasicFeaturesPool.Add(item.TemplateId);
							//}
						} else {
							// alist.Add(item.TemplateId); // 无根之人 石芯玉女 阴阳一体
						}
					}
				}
			}
			foreach (short featureId in alist) {
				Config.CharacterFeatureItem template = Config.CharacterFeature.Instance[featureId];
				if((!featureGroup2Id.ContainsKey(template.MutexGroupId))||featureId>featureGroup2Id[template.MutexGroupId]){
					featureGroup2Id[template.MutexGroupId]=featureId;
				}
			}
			return false;
		}
	}
	[HarmonyPatch(typeof(GameData.Domains.Taiwu.TaiwuDomain),"CalcPracticeResult")]
	public class FastPractice {
		public static int Postfix(int result){
			return 100;
		}
	}
	
	[HarmonyPatch(typeof(GameData.Domains.CombatSkill.CombatSkill),"GetBreakoutAvailableStepsCount")]
	public class BreakPlus {
		public static sbyte Postfix(sbyte result){
			return 100;
		}
	}
	[HarmonyPatch(typeof(GameData.Domains.Combat.CombatDomain),"CheckRopeOrSwordHit")]
	public class AlwaysCapture {
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Call,typeof(GameData.Utilities.RedzenHelper).GetMethod("CheckPercentProb"))
				).Repeat( matcher => // Do the following for each match
					matcher.InsertAndAdvance(
						new CodeInstruction(OpCodes.Pop),
						new CodeInstruction(OpCodes.Ldc_I4_S,100)
					).Advance(1)
				).InstructionEnumeration();
			return instructions;
		}
	}
}