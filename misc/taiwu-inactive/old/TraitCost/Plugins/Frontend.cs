// dotnet "C:\Program Files\dotnet\sdk\6.0.400\Roslyn\bincore\csc.dll" -nologo -t:library -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\0Harmony.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\mscorlib.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Assembly-CSharp.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\TaiwuModdingLib.dll" Frontend.cs -out:Frontend.dll
// -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Mono.Cecil.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Core.dll"   -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Composition.AttributedModel.dll"
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;

namespace TraitCost_Frontend;
[TaiwuModdingLib.Core.Plugin.PluginConfig("TraitCost_Frontend","Neutron3529","0.1.1")]
public class TraitCost_Frontend : TaiwuModdingLib.Core.Plugin.TaiwuRemakeHarmonyPlugin {
	public override void Initialize() {
		this.HarmonyInstance.PatchAll(typeof(TraitCost));
	}
	[HarmonyPatch(typeof(UI_CricketCombat),"OnListenerIdReady")]
	public static class CricketAttackFirst {
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Ldc_I4_S,50),
					new CodeMatch(OpCodes.Ldc_I4_S,100)
				).Repeat( matcher => // Do the following for each match
					matcher
					.SetAndAdvance(
						OpCodes.Ldc_I4_S,100
					).Advance(1)
				).InstructionEnumeration();
			return instructions;
		}
	}
	public static class TraitCost{
		static IEnumerable<MethodBase> TargetMethods(){
			yield return typeof(UI_NewGame).GetMethod("OnAbilityClick",(BindingFlags)(-1));
			yield return typeof(UI_NewGame).GetMethod("OnCellItemRender",(BindingFlags)(-1));
			yield return typeof(UI_NewGame).GetMethod("UpdatePoints",(BindingFlags)(-1));
			yield return typeof(UI_NewGame).GetMethod("OnStartNewGame",(BindingFlags)(-1));
		}
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Ldfld,typeof(Config.ProtagonistFeatureItem).GetField("Cost"))
				).Repeat( matcher => // Do the following for each match
					matcher
					.InsertAndAdvance(
						new CodeInstruction(OpCodes.Pop)
					)
					.SetAndAdvance(
						OpCodes.Ldc_I4_0,null
					)
				).InstructionEnumeration();
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(OpCodes.Ldfld,typeof(Config.ProtagonistFeatureItem).GetField("PrerequisiteCost"))
				).Repeat( matcher => // Do the following for each match
					matcher
					.InsertAndAdvance(
						new CodeInstruction(OpCodes.Pop)
					)
					.SetAndAdvance(
						OpCodes.Ldc_I4_0,null
					)
				).InstructionEnumeration();
			return instructions;
		}
	}
}