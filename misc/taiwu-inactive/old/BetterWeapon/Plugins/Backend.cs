// dotnet "C:\Program Files\dotnet\sdk\6.0.400\Roslyn\bincore\csc.dll" -nologo -t:library -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Collections.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\0Harmony.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\mscorlib.dll" -r:"..\..\..\Backend\GameData.dll" -r:"..\..\..\Backend\Redzen.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\TaiwuModdingLib.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Private.CoreLib.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Runtime.dll" -unsafe -optimize -deterministic Backend.cs -out:Backend.dll
// -r:"..\..\..\The Scroll of Taiwu_Data\Managed\Mono.Cecil.dll" -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Core.dll"   -r:"..\..\..\The Scroll of Taiwu_Data\Managed\System.Composition.AttributedModel.dll" -r:"C:\Program Files\dotnet\shared\Microsoft.NETCore.App\5.0.17\System.Runtime.dll" 
/**
 *  Everyone's Unity Game Plugin
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
 
 // Backend: ProcessMethodCall -> CallMethod -> ...
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;

namespace BetterWeapon_Backend;
[TaiwuModdingLib.Core.Plugin.PluginConfig("BetterWeapon_Backend","NobodyCares","0.1.1")]
public class BetterWeapon_Backend : TaiwuModdingLib.Core.Plugin.TaiwuRemakeHarmonyPlugin {
	public override void Initialize() {
		this.HarmonyInstance.PatchAll(typeof(WeaponModifier));
	}
	[HarmonyPatch(typeof(GameData.Domains.Building.BuildingDomain),"GetMakeItems")]
	public class WeaponModifier {
		public static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
			instructions = new CodeMatcher(instructions)
				.MatchForward(false, // false = move at the start of the match, true = move at the end of the match
					new CodeMatch(i=> i.opcode==OpCodes.Call && ((MethodInfo)i.operand).Name == "GetElement_MakeItemDict")
				).Repeat( matcher => // Do the following for each match
					matcher
					.Advance(1)
					.InsertAndAdvance(
						new CodeInstruction(OpCodes.Call,typeof(WeaponModifier).GetMethod("MZhujianGA"))
					)
				).InstructionEnumeration();
			return instructions;
		}
		public static unsafe GameData.Domains.Building.MakeItemData MZhujianGA(
			GameData.Domains.Building.MakeItemData makeItemData
		){
			fixed(short*p=makeItemData.MaterialResources.Items){
				for(int i=0;i<6;i++){
					*(p+i)=(short)Math.Min((int)(*(p+i)) << 8,32000);
				}
			}
			return makeItemData;
		}
	}
}