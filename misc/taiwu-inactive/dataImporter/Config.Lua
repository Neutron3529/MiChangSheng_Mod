return {GameVersion="99.99.99.99",
    Title = "自动游戏数据导入",
    --workshop_only    FileId = 0,
    Version = "3.5.2.9", -- 2022<<32+0925,
    NeedRestartWhenSettingChanged = false,
    Dependencies = {},
    BackendPlugins = {"../Backend.dll"},
    --BackendPatches = {},
    FrontendPlugins = {"../NeutronFrontend-DataImporter.dll"},
    --EventPackages = {"../../NeutronPseudoEvent-DataImporter.dll"},
    --workshop_only    Source = 1,
    HasArchive = false,
    TagList = {"Configurations","Display"},
    Author = "Neutron3529",
    Description = "自动数据导入，目前可以全自动地修改游戏内几乎所有数字类型的参数\n\nMod加载后生效，会遍历全部工坊文件夹，读取文件夹内的tsvlist.tsv文件（区分大小写，毕竟未来游戏可能会被移植到linux）\n基础资源框架指日可待……我的意思是，果然干活还得靠自己……",
    DefaultSettings = {
        {Key="VerboseWarn", DisplayName="log报警",  Description="使用logwarn而非logger报告一些需要Modder注意的事项\n当你安装的Mod未按照设想运行时，可以打开这个开关看看情况\n至少这可以让Mod作者知道发生了什么", SettingType="Toggle", DefaultValue=false},
    }
}
