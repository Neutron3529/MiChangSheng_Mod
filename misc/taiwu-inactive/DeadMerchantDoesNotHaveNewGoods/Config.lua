return {
    Title = "死掉的商人不会拥有新商品",
    --workshop_only    FileId = 2948426990,
    --workshop_only    Source = 1,
    Version = 0, -- 2022<<32+0925,
    NeedRestartWhenSettingChanged = false,
    Dependencies = {},
    BackendPlugins = {"../Backend.dll"},
    --BackendPatches = {},
    --FrontendPlugins = {"../Frontend.dll"},
    --FrontendPatches = {},
    --EventPackages = {},
    Dependencies = {},
    HasArchive = false,
    TagList = {"Configurations"},
    Author = "Neutron3529",
    Description = "这是一个修正at GameData.Domains.Character.CharacterDomain.MerchantHasNewGoods红字的Mod",
    DefaultSettings = {
        {Key="NForceOFF", DisplayName="禁用Mod",  Description="不想重启游戏的话可以点这里禁用Mod（然后看看游戏到底会不会MerchantHasNewGoods红字）", SettingType="Toggle", DefaultValue=false},
        {Key="B0", DisplayName="应用额外检查",  Description="使用额外的检查来避免这个红字的出现", SettingType="Toggle", DefaultValue=true},
    }
}
