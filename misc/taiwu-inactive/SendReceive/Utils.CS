// echo aux function only, should not compile alone.
// cp Utils.CS .. && cd $_ && for i in `\ls` ; do \cp -f Utils.CS $i/ ; done || rm Utils.CS && cd "$OLDPWD"
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;

namespace Utils {
    public class RobustHarmonyInstance {
        public static void logger(string s)=>GameData.Utilities.AdaptableLog.Info(s);
        public static void logwarn(string s)=>GameData.Utilities.AdaptableLog.Warning("<color=#FFFF00>"+s+"</color>",true);
        public static void logerror(string s)=>GameData.Utilities.AdaptableLog.Error(s); // should not use.
        public Harmony Harmony;
        public RobustHarmonyInstance(string s)=>this.Harmony=new Harmony(s);
        public void PatchAll(Type t){try{this.Harmony.PatchAll(t);}catch(Exception ex){logwarn("PatchAll出错，错误信息为"+ex.Message);}}
        public void UnpatchSelf()=>this.Harmony.UnpatchSelf();
    }
}
