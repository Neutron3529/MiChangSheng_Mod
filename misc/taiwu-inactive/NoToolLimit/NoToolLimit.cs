// $DOTNET $DOTNET_CSC_DLL -nologo -t:library -r:"../../The Scroll of Taiwu_Data/Managed/System.dll" -r:"../../The Scroll of Taiwu_Data/Managed/netstandard.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Reflection.Emit.ILGeneration.dll" -r:"../../The Scroll of Taiwu_Data/Managed/0Harmony.dll" -r:"../../The Scroll of Taiwu_Data/Managed/mscorlib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Assembly-CSharp.dll" -r:"../../The Scroll of Taiwu_Data/Managed/TaiwuModdingLib.dll" -r:"../../The Scroll of Taiwu_Data/Managed/Unity.TextMeshPro.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.CoreModule.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.UI.dll" -r:"../../The Scroll of Taiwu_Data/Managed/UnityEngine.dll" -optimize -deterministic NoToolLimit.cs *.CS -debug -out:NoToolLimit.dll
// -r:"../../The Scroll of Taiwu_Data/Managed/Mono.Cecil.dll" -r:"../../The Scroll of Taiwu_Data/Managed/System.Core.dll"   -r:"../../The Scroll of Taiwu_Data/Managed/System.Composition.AttributedModel.dll"
/**
 *  Neutron's Taiwu Collections
 *  Copyright (C) 2022 Neutron3529
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as
 *  published by the Free Software Foundation, either version 3 of the
 *  License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#define FRONTEND
#define Fix_0_0_20

using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;
using HarmonyLib;
using Utils;
[assembly: AssemblyVersion("0.0.0.3529")]
namespace NoToolLimit;
[TaiwuModdingLib.Core.Plugin.PluginConfig("NoToolLimit","Neutron3529","0.1.0")]
public class NoToolLimit : TaiwuModdingLib.Core.Plugin.TaiwuRemakePlugin {
    public override void Initialize()=>this.HarmonyInstance = new RobustHarmonyInstance(this.GetGuid());
    public override void Dispose()=>this.HarmonyInstance.UnpatchSelf();
    public static void logger(string s)=>GameData.Utilities.AdaptableLog.Info(s);
    public static void logwarn(string s)=>GameData.Utilities.AdaptableLog.Warning("<color=#FFFF00>"+s+"</color>",true);
    public RobustHarmonyInstance HarmonyInstance;

    private bool enable(string key){
        try {
            return _enable(key);
        } catch(Exception ex){
            logwarn("enable 出错，出错键值为\""+key+"\"，错误原因是"+ex.Message);
            return false;
        }
    }
    private bool _enable(string key){
        bool enable=false;
        return ModManager.GetSetting(this.ModIdStr, key,ref enable) && enable;
    }
    public override void OnModSettingUpdate(){
        this.HarmonyInstance.UnpatchSelf();
        if(enable("NForceOFF")){return;}
        this.HarmonyInstance.PatchAll(typeof(EnableNullTool));
    }
    public class EnableNullTool {
        static GameData.Domains.Item.Display.ItemDisplayData tool=new GameData.Domains.Item.Display.ItemDisplayData();
        // [HarmonyPostfix, HarmonyPatch(typeof(UI_Make),"OnInit")]
        // public static void Po0(UI_Make __instance, GameData.Domains.Item.Display.ItemDisplayData ____currentTool){
        //     tool.Durability=32000;
        //     typeof(UI_Make).GetMethod("ChangeCurrentTool",(BindingFlags)(-1)).Invoke(__instance,new object[]{tool});
        // }
        [HarmonyPrefix, HarmonyPatch(typeof(UI_Make),"GetToolAttainment", new Type[]{typeof(int)})]
        public static bool Pre1(int itemType, ref short __result){
            if(itemType<0){
                __result=0;
                return false;
            }
            return true;
        }
        [HarmonyPrefix, HarmonyPatch(typeof(UI_Make),"GetToolAttainment", new Type[]{})]
        public static bool pre1(GameData.Domains.Item.Display.ItemDisplayData ____currentTool, ref short __result){
            if(____currentTool==null || ____currentTool.Key.Id==-1){
                tool.Durability=32000;
                ____currentTool=tool;
                __result=0;
                return false;
            }
            return true;
        }
        [HarmonyPrefix, HarmonyPatch(typeof(UI_Make),"ChangeCurrentTool")]
        public static void Pre2(GameData.Domains.Item.Display.ItemDisplayData target){
            if(target==null){
                tool.Durability=32000;
                target=tool;
            }
        }
        [HarmonyPrefix, HarmonyPatch(typeof(UI_Make),"CheckTool")]
        public static void Pre3(GameData.Domains.Item.Display.ItemDisplayData ____currentTool, bool __result){
            if(____currentTool==null){
                tool.Durability=32000;
                ____currentTool=tool;
                logwarn("curr=null");
            }
            // __result=true;
            // return false;
        }
    }
}
