return {
    Title = "卡顿优化",
    BackendPlugins = {"../Backend.dll"},
    Author = "Neutron3529",
--workshop_only    FileId = 2879036796,
--workshop_only    Source = 1,
    Description = "有BUG报gitee\n这里是一些（有副作用的）卡顿优化。\n因为茄子表示我要优化这个函数迟早要重写，这里就不100%复刻了。",
    NeedRestartWhenSettingChanged = false,
    Dependencies = {},
    DefaultSettings = {
        --{Key="N01", DisplayName="福祸难料，生死难知", Description="并行更新门派战力，以减少延迟，副作用是不会因此更新人物战力，过月时或许与原版略有差异。可以把更新战力部分写在前面，但我懒得写。", SettingType="Toggle", DefaultValue=true},
        --{Key="N02", DisplayName="流言蜚语，也冰也炽", Description="完全移除SB秘闻系统（存档中删除秘闻耗时竟然比传播秘闻还要多！这种屎山在螺舟修好之前，最好离远点）", SettingType="Toggle", DefaultValue=true},
        {Key="N03", DisplayName="减少战斗卡顿", Description="通过跳过复杂函数的计算获取性能提升", SettingType="Toggle", DefaultValue=true},
    },
}
