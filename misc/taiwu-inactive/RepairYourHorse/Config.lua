return {
    Title = "修你🐴",
    BackendPlugins = {"../Backend.dll"},
    Author = "Neutron3529",
--workshop_only    FileId = 2890662988,
--workshop_only    Source = 1,
--workshop_only    Cover = "RepairYourHorse.png",
    Description = "有BUG报https://gitee.com/Neutron3529/MiChangSheng_Mod/issues\n这个Mod的唯一功能是，让大部分Fix变成“修你🐴”\n别问我为什么缩略图长这个样子，这是我拿Mod群里夜娘的AI画的，词条是”修你🐴呢 游戏本体没事 fix倒是红了“\n\n这个Mod在将来<color=#FF0000>或许</color>会有一些更神奇的修复，比如允许后端红字时继续游戏（甚至继续存档），这些功能默认关闭，大家有需求的，可以自取。",
    NeedRestartWhenSettingChanged = false,
    Dependencies = {},
    DefaultSettings = {
        {Key="N00", DisplayName="修你🐴呢", Description="彻底禁用存档修复，反正这个修复什么也修复不了。", SettingType="Toggle", DefaultValue=false},
        {Key="N01", DisplayName="修你🐴呢别修了", Description="禁用以下Domain的FixAbnormalDomainArchiveData，按类名匹配，多个类用逗号分割，长度大于1时生效", SettingType= "InputField", DefaultValue=""},
        {Key="N02", DisplayName="这么好的数据修你🐴呢", Description="禁用带FixAbnormalDomainArchiveData的类中的函数，按函数名匹配，多个类用逗号分割，长度大于1时生效", SettingType= "InputField", DefaultValue="FixBreakGridBonus"},
    },
    TagList = {"Modifications"},
}
