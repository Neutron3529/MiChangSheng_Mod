#!/bin/bash -e
#
#   Neutron3529's Unity Game Plugin
#   Copyright (C) 2022-2023 Neutron3529
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
############################################################################
#
#   * compile instructions: put this file and `utils.cs` in `steamapps`
#   * folder, open a terminal in the same folder, and execute:
#   *
#   * ```
#   *     chmod +x ${file}.cs
#   *     ./${file}.cs
#   * ```
#   *
#   * then the mod will be compiled automatically.
#   *
#   * Here we wrote a shebang like file, which is correct
#   * in my computer (Manjaro XFCE), if such script do not work
#   * in your computer, you could just try the instructions below :

export GAME_NAME="${0%\.cs}"                                # might modify if the name mismatch.
export GAME_DIR="$GAME_NAME"                                # might be modified, but "$GAME_NAME" cover most of the cases.

export FILE_NAME="$0"
export ASSEMBLY="Mafi"                           # might be modified
export UTILS="utils.cs"                                     # might be modified if you do not put utils.cs in the current dir.
export PLUGIN_ID="Neutron3529.Cheat"                        # should be modified
export NAMESPACE_ID="Neutron3529.Cheat"                     # should be modified
export GAME_BASE_DIR="common/$GAME_DIR"                     # should modify GAME_DIR instead since GAME_DIR == GAME_NAME is almost always true.

export IFS=$'\n' # to disable the annoying space.
export DOTNET="dotnet" # the location of the DOTNET executable file.
[ -z "$DOTNET_CSC_DLL" ] && export DOTNET_CSC_DLL=`\ls /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll` # In manjaro, the csc.dll is located in /usr/share/dotnet/sdk/*/Roslyn/bincore/csc.dll

case $1 in
    V)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
    v)       EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
    VERBOSE) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
    verbose) EXTRA_DEFINE="-define:DEBUG${IFS}-define:VERBOSE${IFS}-debug" ;;
    D)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
    d)       EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
    DEBUG)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
    debug)   EXTRA_DEFINE="-define:DEBUG${IFS}-debug"                      ;;
    *)       EXTRA_DEFINE=""                                               ;;
esac

_MODE__SELECT_=100

( yes "" | head -n $_MODE__SELECT_ | head -n-1  ; tail $FILE_NAME -n+$_MODE__SELECT_) | sed s/%%NAMESPACE_ID%%/${NAMESPACE_ID}/g | sed s/%%PLUGIN_ID%%/${PLUGIN_ID}/g | $DOTNET $DOTNET_CSC_DLL -nologo -t:library \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/0Harmony.dll" \
  -r:"${GAME_BASE_DIR}/BepInEx/core/BepInEx.Harmony.dll" \
  `[ -e "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll" ] && echo "-r:\"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/netstandard.dll\""` \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/System.Core.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.AIModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.CoreModule.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/UnityEngine.UI.dll" \
  -r:"${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/mscorlib.dll" \
  $(for i in "${GAME_BASE_DIR}/${GAME_NAME}_Data/Managed/$ASSEMBLY"*.dll ; do echo -e "-r:\"$i\"\n" ; done) \
  -out:"${GAME_BASE_DIR}/BepInEx/plugins/${FILE_NAME%.*}".dll \
  -optimize $EXTRA_DEFINE \
  - $UTILS && rm -f "${GAME_BASE_DIR}/BepInEx/config/${PLUGIN_ID}.cfg";

if [ -n "$2" ]; then
    git add ${FILE_NAME}
    case $2 in
        R) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        r) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        RANDOM) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        random) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        U) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        u) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        UPLOAD) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        upload) git commit -am "`curl -s https://whatthecommit.com/index.txt`" ;;
        *) git commit -am "$2" ;;
    esac
    git push
fi
exit





using System;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Collections.Generic;

using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;

namespace %%NAMESPACE_ID%%;

using Neutron3529.Utils;

[BepInPlugin("%%PLUGIN_ID%%", "%%NAMESPACE_ID%%", "0.1.0")]
public class Cheat : ModEntry {
    public Cheat() : base("%%PLUGIN_ID%%") {}
    public new void Awake() {
        logger("叮~修改器正在启动，请。。");
        patch.self=this;
        harmony.Patch(typeof(Mafi.Core.Mods.ModsLoader).GetMethod("LoadAllModsFrom",(BindingFlags)(-1)), null, new HarmonyMethod(SymbolExtensions.GetMethodInfo(() => patch.Postfix())));
        // Mafi.Localization.LocalizationManager.EnableLocalization(); // 不知道为什么不加这一句会导致游戏变成纯英文
        // 主要逻辑放在`utils.cs`中，这里的Awake2只是为了以防万一
        // 目前来说，这个函数的唯一用途是用来“叮”……
        // 就像这样：
        logger("叮~修改器启动，请安心游戏");
    }
    // [HarmonyPatch(typeof(Mafi.Unity.InputControl.GameMenu.IGameMenuController).Assembly.GetType("LocalizationSettings",true),"ApplyCurrentLanguage")]
    class patch {
        public static Cheat self;
        public static void Postfix()=>(patch.self as ModEntry).Awake();
    }
    [Desc("废弃物判定，在开启配方乘除数时必须启用")]
    class Recipe_quantity_mult : Const {
        [Desc("废弃物判定,以竖线分割")]
        string names="Polluted|Exhaust|_Waste";
        static string[] frags;
        public override void Enable(){
            frags=names.Split('|');
        }
        public static bool IsWaste(Mafi.Core.Products.ProductProto product){
            // logwarn($"收到:{product.ToString()}, IsStorable: {product.IsStorable}, CanBeDiscarded: {product.CanBeDiscarded}, IsWaste={product.IsWaste}, IsRecyclable={product.IsRecyclable}");
            var x=product.ToString();
            return frags.Any((y)=>x.Contains(y));
        }
    }
    [Desc("建筑电力消耗最小化",-1)]
    class building_no_electric_cost : Entry {
        static IEnumerable<MethodBase> TargetMethods() {
            foreach(var ty in typeof(Mafi.Base.Prototypes.Transport.PortShapesData).Assembly.GetTypes()){
                foreach(MethodInfo i in ty.GetMethods(/*(BindingFlags)(-1)*/)){
                    if(i.Name=="RegisterData" && !i.DeclaringType.Name.Contains("Generator")){
                        yield return i;
                    }
                }
            }
        }
        static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
            vlogger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法");
            instructions = new CodeMatcher(instructions)
                .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
                    new CodeMatch(OpCodes.Call, typeof(Mafi.ElectricityExtensions).GetMethod("Kw",new Type[]{typeof(int)}))
                    // new CodeMatch(OpCodes.Call, typeof(Mafi.Base.Prototypes.Transport.PortShapesData).GetMethod("Kw",new Type[]{typeof(int)}))
                ).Repeat( matcher => // Do the following for each match
                    matcher
                    .InsertAndAdvance(
                        new CodeInstruction(OpCodes.Pop),
                        new CodeInstruction(OpCodes.Ldc_I4_1)
                    )
                    .Advance(1) // avoid conflict.
                ).InstructionEnumeration();
            return instructions;
        }
    }

//     class building_time_mut : Entry {
//         [Desc("车辆建造耗时除数","gt",1)]
//         static float div = 10;
//         [HarmonyPatch(typeof()"SetDurationToBuild")]
//
//         static void Prefix(Mafi.Duration durationToBuild){
//         }
//     }
    // class faster_duration : Entry {
    //     static IEnumerable<MethodBase> TargetMethods() {
    //         logger($"执行faster_duration的TargetMethods");
    //         foreach(var ty in typeof(Mafi.Base.Prototypes.Vehicles.TreePlanetersData).Assembly.GetTypes(/*(BindingFlags)(-1)*/)){ // GetExportedTypes()
    //             if (ty?.Namespace?.StartsWith("Mafi.Base.Prototypes.Vehicles")??false) {
    //                 logger($"准备注入{ty.Namespace}中的{ty.Name}");
    //                 foreach(MethodInfo i in ty.GetMethods((BindingFlags)(-1))){
    //                     if(i?.Name?.StartsWith("create")??false){ // create something include FuelTank.
    //                         logger($"准备注入{ty.Namespace}.{ty.Name}.{i.Name}");
    //                         yield return i;
    //                     }
    //                 }
    //             }
    //         }
    //         // foreach(var ty in typeof(Mafi.Base.Prototypes.Transport.PortShapesData).Assembly.GetTypes()){
    //         //     foreach(MethodInfo i in ty.GetMethods(/*(BindingFlags)(-1)*/)){
    //         //         if(i.Name=="RegisterData"){
    //         //             yield return i;
    //         //         }
    //         //     }
    //         // }
    //     }
    //
    //     [Desc("注册动画耗时除数","gt",0)]
    //     static int div_factor = 10;
    //     public static Mafi.Duration div(Mafi.Duration num)=> new Mafi.Duration(Math.Min(1,num.Ticks/div_factor));
    //     public static Mafi.Duration mul(Mafi.Duration num)=> new Mafi.Duration(Math.Min(1,num.Ticks*div_factor));
    //     public static Mafi.RelTile1f tdiv(Mafi.RelTile1f num) => new Mafi.RelTile1f(num.Value/div_factor);
    //     public static Mafi.RelTile1f tmul(Mafi.RelTile1f num) => new Mafi.RelTile1f(num.Value*div_factor);
    //     public static Mafi.AngleDegrees1f ddiv(Mafi.AngleDegrees1f num) => Mafi.AngleDegrees1f.FromDegrees(num.Degrees/div_factor);
    //     public static Mafi.AngleDegrees1f dmul(Mafi.AngleDegrees1f num) => Mafi.AngleDegrees1f.FromDegrees(num.Degrees*div_factor);
    //     static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
    //         vlogger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法");
    //         instructions = new CodeMatcher(instructions)
    //             .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
    //                 new CodeMatch(i=>(i.operand is MethodInfo m and not null && m.ReturnType == typeof(Mafi.Duration)) || (i.operand is ConstructorInfo c and not null && c.DeclaringType == typeof(Mafi.Duration)) )
    //             ).Repeat( matcher => // Do the following for each match
    //                 {
    //                     matcher
    //                     .Advance(1)
    //                     .InsertAndAdvance(
    //                         new CodeInstruction(OpCodes.Call, typeof(faster_duration).GetMethod(__originalMethod.Name == "createFuelTank"?"mul":"div"))
    //                     ).Advance(1);
    //                     logger($"使用{(__originalMethod.Name == "createFuelTank"?"mul":"div")}函数注入了{__originalMethod.Name}方法");
    //                 }
    //             ).InstructionEnumeration();
    //         instructions = new CodeMatcher(instructions)
    //             .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
    //                 new CodeMatch(i=>(i.operand is MethodInfo m and not null && m.ReturnType == typeof(Mafi.RelTile1f)) || (i.operand is ConstructorInfo c and not null && c.DeclaringType == typeof(Mafi.RelTile1f)) )
    //             ).Repeat( matcher => // Do the following for each match
    //                 {
    //                     matcher
    //                     .Advance(1)
    //                     .InsertAndAdvance(
    //                         new CodeInstruction(OpCodes.Call, typeof(faster_duration).GetMethod(__originalMethod.Name != "createFuelTank"?"dmul":"ddiv"))
    //                     ).Advance(1);
    //                     logger($"使用{(__originalMethod.Name != "createFuelTank"?"dmul":"ddiv")}函数注入了{__originalMethod.Name}方法");
    //                 }
    //             ).InstructionEnumeration();
    //         instructions = new CodeMatcher(instructions)
    //             .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
    //                 new CodeMatch(i=>(i.operand is MethodInfo m and not null && m.ReturnType == typeof(Mafi.AngleDegrees1f)) || (i.operand is ConstructorInfo c and not null && c.DeclaringType == typeof(Mafi.AngleDegrees1f)) )
    //             ).Repeat( matcher => // Do the following for each match
    //                 {
    //                     matcher
    //                     .Advance(1)
    //                     .InsertAndAdvance(
    //                         new CodeInstruction(OpCodes.Call, typeof(faster_duration).GetMethod(__originalMethod.Name != "createFuelTank"?"tmul":"tdiv"))
    //                     ).Advance(1);
    //                     logger($"使用{(__originalMethod.Name != "createFuelTank"?"tmul":"tdiv")}函数注入了{__originalMethod.Name}方法");
    //                 }
    //             ).InstructionEnumeration();
    //         return instructions;
    //         // var cnt=0;
    //         // foreach(var i in instructions){
    //         //     logger($"yield {cnt++:X04}: {i}");
    //         //     yield return i;
    //         // }
    //     }
    // }

    class longer_electric_duration : Entry {
        // static Type[] assembly_types=new Type[]{typeof(Mafi.Assert), typeof(Mafi.Base.BaseMod), typeof(Mafi.Base.BaseMod), typeof(Mafi.Core.CoreMod)};
        static Assembly[] Assemblies=AppDomain.CurrentDomain.GetAssemblies();
        static IEnumerable<MethodBase> TargetMethods() {
            foreach(var ass in Assemblies){
                foreach(var ty in ass.GetTypes()){
                    if(typeof(Mafi.Core.Factory.Recipes.IRecipeForUi).IsAssignableFrom(ty)) {
                        foreach(var c in ty.GetConstructors((BindingFlags)(-1))) {
                            var counter=0;
                            foreach(var p in c.GetParameters()) {
                                if((p.Name=="duration" && p.ParameterType == typeof(Mafi.Duration)) || ( p.Name=="electricityProto" && p.ParameterType == typeof(Mafi.Core.Products.ProductProto))) {
                                    counter+=1;
                                    if(counter==2){
                                        logger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+c.DeclaringType.Name+"的"+c.Name+"方法");
                                        yield return c;break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        [Desc("电力配方持续时间乘数",10)]
        static int mult=10;
        static void Prefix(ref Mafi.Duration duration)=>duration=duration*mult;
    }
    // // [HarmonyPatch(typeof(Mafi.RecipeProtoBuilderExtensions),"AddOutput")]
    // recipes_quantity_mult是更好的选择
    // class machine_output_mult : Entry {
    //     [Desc("产量乘数")]
    //     static double factor=5;
    //     static IEnumerable<MethodBase> TargetMethods() {
    //         foreach(var ty in typeof(Mafi.Base.Prototypes.Transport.PortShapesData).Assembly.GetTypes()){
    //             foreach(MethodInfo i in ty.GetMethods(/*(BindingFlags)(-1)*/)){
    //                 if(i.Name=="RegisterData"){
    //                     yield return i;
    //                 }
    //             }
    //         }
    //     }
    //     static IEnumerable<CodeInstruction> Transpiler(MethodBase __originalMethod, IEnumerable<CodeInstruction> instructions) {
    //         logger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+__originalMethod.DeclaringType.Name+"的"+__originalMethod.Name+"方法");
    //         // var from=typeof(Mafi.RecipeProtoBuilderExtensions).GetMethod("AddOutput",(BindingFlags)(-1));
    //         // var to=System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.GetMethod("AddOutput",(BindingFlags)(-1));
    //         instructions = new CodeMatcher(instructions)
    //             .MatchForward(false, // false = move at the start of the match, true = move at the end of the match
    //                 new CodeMatch(i=>i.opcode==OpCodes.Call && ((MethodBase)(i.operand)).Name == "AddOutput") // Generics
    //             ).Repeat( matcher => {// Do the following for each match
    //                 var x=((MethodInfo)(matcher.Instruction.operand)).GetGenericArguments();
    //                 if(x!=null){
    //                     var y=typeof(machine_output_mult).GetMethod("AddOutput",(BindingFlags)(-1)).MakeGenericMethod(x);
    //                     matcher.SetAndAdvance(
    //                         OpCodes.Call,y
    //                     ).Advance(1);
    //                 }
    //             }).InstructionEnumeration();
    //         return instructions;
    //     }
    //     static T AddOutput<T>(Mafi.Core.Factory.Recipes.IRecipeProtoBuilderState<T> builder, int quantity, Mafi.Core.Products.ProductProto.ID productId, string portSelector, bool outputAtStart, bool hideInUi)=>Mafi.RecipeProtoBuilderExtensions.AddOutput(builder, (int)(factor*(double)quantity), productId, portSelector, outputAtStart, hideInUi);
    // }
    class RecipeOutput_quantity_mult : Entry {
        static IEnumerable<MethodBase> TargetMethods() {
            foreach(var c in typeof(Mafi.Core.Factory.Recipes.RecipeOutput).GetConstructors((BindingFlags)(-1))){
                int counter=0;
                foreach(var p in c.GetParameters()){
                    if((p.Name=="quantity" && p.ParameterType == typeof(Mafi.Quantity)) || (p.Name=="product" && p.ParameterType == typeof(Mafi.Core.Products.ProductProto))){
                        counter++;
                        if(counter==2){
                            logger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+c.DeclaringType.Name+"的"+c.Name+"方法");
                            yield return c;break;
                        }
                    }
                }
            }
        }
        [Desc("配方产量乘除数（对废品用除法向上取整；其他用乘法）")]
        static int mult=-1;
        static void Prefix(Mafi.Core.Products.ProductProto product, ref Mafi.Quantity quantity){if(Recipe_quantity_mult.IsWaste(product)){quantity=quantity.CeilDiv(mult);}else{quantity=quantity*mult;}}
    }
    class RecipeInput_quantity_mult : Entry {
        static IEnumerable<MethodBase> TargetMethods() {
            foreach(var c in typeof(Mafi.Core.Factory.Recipes.RecipeInput).GetConstructors((BindingFlags)(-1))){
                int counter=0;
                foreach(var p in c.GetParameters()){
                    if((p.Name=="quantity" && p.ParameterType == typeof(Mafi.Quantity)) || (p.Name=="product" && p.ParameterType == typeof(Mafi.Core.Products.ProductProto))){
                        counter++;
                        if(counter==2){
                            logger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+c.DeclaringType.Name+"的"+c.Name+"方法");
                            yield return c;break;
                        }
                    }
                }
            }
        }
        [Desc("配方输入乘除数（对废品用乘法；其他用除法向上取整）")]
        static int mult=-1;
        static void Prefix(Mafi.Core.Products.ProductProto product, ref Mafi.Quantity quantity){if(Recipe_quantity_mult.IsWaste(product)){quantity=quantity*mult;}else{quantity=quantity.CeilDiv(mult);}}
    }
    class RecipeSpeed : Entry {
        // static Type[] assembly_types=new Type[]{typeof(Mafi.Assert), typeof(Mafi.Base.BaseMod), typeof(Mafi.Base.BaseMod), typeof(Mafi.Core.CoreMod)};
        static Assembly[] Assemblies=AppDomain.CurrentDomain.GetAssemblies();
        static IEnumerable<MethodBase> TargetMethods() {
            foreach(var ass in Assemblies){
                foreach(var ty in ass.GetTypes()){
                    if(typeof(Mafi.Core.Factory.Recipes.IRecipeForUi).IsAssignableFrom(ty)) {
                        foreach(var c in ty.GetConstructors((BindingFlags)(-1))) {
                            foreach(var p in c.GetParameters()) {
                                if(p.Name=="duration" && p.ParameterType == typeof(Mafi.Duration)) {
                                    logger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+c.DeclaringType.Name+"的"+c.Name+"方法");
                                    yield return c;break;
                                }
                            }
                        }
                    }
                }
            }
        }
        [Desc("配方生产速度乘数")]
        static int mult=10;
        static void Prefix(ref Mafi.Duration duration)=>duration=new Mafi.Duration((duration.Ticks+mult-1)/mult);
    }

    // class TreeDataGetScaleIgnoringBase : Entry {
    //     [Desc("树木资源倍率")]
    //     static double mul = 2;
    //     static Mafi.Percent Mul = default(Mafi.Percent);
    //     public override void Enable() {
    //         Mul = Mafi.Percent.FromDouble(mul);
    //         base.Enable();
    //     }
    //     [HarmonyPatch(typeof(Mafi.Core.Terrain.Trees.TreeData),"GetScaleIgnoringBase")]
    //     static bool Prefix(ref Mafi.Percent __result){
    //         __result = Mul;
    //         return false;
    //     }
    // }


    public static class Get {
        public static IEnumerable<MethodBase> Ctor(MethodBase original, Type type, string name, Type ty) {
            foreach(var c in type.GetConstructors(/*(BindingFlags)(-1)*/)){
                foreach(var p in c.GetParameters()){
                    if(p.Name==name && p.ParameterType == ty) {
                        vlogger($"%%PLUGIN_ID%%-类{original}正在注入类别"+c.DeclaringType.Name+"的"+c.Name+"方法");
                        yield return c;break;
                    }
                }
            }
        }
        public static IEnumerable<MethodBase> AllCtor(MethodBase original, Type type, string name, Type ty) {
            foreach(var c in type.GetConstructors((BindingFlags)(-1))){
                foreach(var p in c.GetParameters()){
                    if(p.Name==name && p.ParameterType == ty) {
                        vlogger($"%%PLUGIN_ID%%-类{original}正在注入类别"+c.DeclaringType.Name+"的"+c.Name+"方法");
                        yield return c;break;
                    }
                }
            }
        }
    }

    class rot_speed : Entry {
        [Desc("转动速度")]
        static double speed_mul = 10;
        [Desc("转动加速度")]
        static double acc_mul = 10;
        [Desc("转动减速度")]
        static double dec_mul = 10;
        static Mafi.Fix32 speed = default(Mafi.Fix32);
        static Mafi.Fix32 acc = default(Mafi.Fix32);
        static Mafi.Fix32 dec = default(Mafi.Fix32);
        public override void Enable() {
            speed = Mafi.Fix32.FromDouble(speed_mul);
            acc = Mafi.Fix32.FromDouble(acc_mul);
            dec = Mafi.Fix32.FromDouble(dec_mul);
            base.Enable();
        }
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.Ctor(original, typeof(Mafi.Core.Vehicles.RotatingCabinDriverProto), "maxSpeedPerTick", typeof(Mafi.AngleDegrees1f));
        static void Prefix(ref Mafi.AngleDegrees1f maxSpeedPerTick, ref Mafi.AngleDegrees1f maxAccelerationPerTick, ref Mafi.AngleDegrees1f maxBrakingPerTick){
            maxSpeedPerTick *= speed;
            maxAccelerationPerTick *= acc;
            maxBrakingPerTick *= dec;
        }
    }

    class RainwaterHarvesterProto_Ctor : Entry {
        [Desc("雨水收集器容量乘数")]
        static double mul = 100;
        [Desc("雨水收集器收集速度乘数")]
        static double cmul = 10000;
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.Ctor(original, typeof(Mafi.Core.Buildings.RainwaterHarvesters.RainwaterHarvesterProto), "capacity", typeof(Mafi.Quantity));
        static void Prefix(ref Mafi.Quantity capacity, ref Mafi.PartialQuantity waterCollectedPerDay) {
            capacity = capacity.ScaledBy(Mafi.Percent.FromDouble(mul));
            waterCollectedPerDay = waterCollectedPerDay.ScaledBy(Mafi.Percent.FromDouble(cmul));
        }
    }

    [Desc("树木生长速度调整")]
        // [HarmonyPatch(typeof(Mafi.Core.Terrain.Trees.TreePlantingGroupProto), Desc.ctor)]
    class TreePlantingGroupProto_Ctor : Entry {
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.Ctor(original, typeof(Mafi.Core.Terrain.Trees.TreePlantingGroupProto), "productWhenHarvested", typeof(Mafi.Core.ProductQuantity));
        [Desc("树木资源倍率")]
        static double mul = 100;
        static Mafi.Percent Mul = default(Mafi.Percent);
        public override void Enable() {
            Mul = Mafi.Percent.FromDouble(mul);
            base.Enable();
        }
        static void Prefix(
            ref Mafi.Core.ProductQuantity productWhenHarvested,
            ref Mafi.Duration timeTo40PercentGrowth,
            ref Mafi.Duration timeTo60PercentGrowth,
            ref Mafi.Duration timeTo80PercentGrowth,
            ref Mafi.Duration timeTo100PercentGrowth
        ){
            productWhenHarvested = productWhenHarvested.ScaledBy(Mul);
            timeTo40PercentGrowth = Mafi.Duration.OneTick;
            timeTo60PercentGrowth = Mafi.Duration.FromTicks(2);
            timeTo80PercentGrowth = Mafi.Duration.FromTicks(3);
            timeTo100PercentGrowth = Mafi.Duration.FromTicks(4);
        }
    }

    class truck_capacity_mult : Entry {
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.Ctor(original, typeof(Mafi.Core.Vehicles.Trucks.TruckProto), "capacity", typeof(Mafi.Quantity));
        [Desc("卡车容量乘数")]
        static int mult=1000;
        static void Prefix(ref Mafi.Quantity capacity)=>capacity*=mult;
    }
    class treeplanter_capacity_mult : Entry {
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.Ctor(original, typeof(Mafi.Core.Vehicles.TreePlanters.TreePlanterProto), "capacity", typeof(Mafi.Quantity));
        [Desc("植树机容量乘数")]
        static int mult=1000;
        static void Prefix(ref Mafi.Quantity capacity)=>capacity*=mult;
    }
    class storages_capacity_mult : Entry {
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.AllCtor(original, typeof(Mafi.Core.Buildings.Storages.StorageBaseProto), "capacity", typeof(Mafi.Quantity));
        [Desc("仓库容量乘数")]
        static int mult=10000;
        static void Prefix(ref Mafi.Quantity capacity)=>capacity*=mult;
    }
    class excavato_capacity_mult : Entry {
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.Ctor(original, typeof(Mafi.Core.Vehicles.Excavators.ExcavatorProto), "capacity", typeof(Mafi.Quantity));
        [Desc("挖掘机容量乘数")]
        static int mult=2000;
        static void Prefix(ref Mafi.Quantity capacity)=>capacity*=mult;
    }

    class truck_dump_mult : Entry {
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.Ctor(original, typeof(Mafi.Core.Vehicles.Trucks.TruckProto), "dumpedThicknessByDistance", typeof(Mafi.Collections.ImmutableCollections.ImmutableArray<Mafi.ThicknessTilesF>));
        [Desc("倾倒数量乘数")]
        static double amount=1000;
        [Desc("倾倒距离乘数")]
        static int dist=3;
        static void Prefix(ref Mafi.Collections.ImmutableCollections.ImmutableArray<Mafi.ThicknessTilesF> dumpedThicknessByDistance){
            var val = dumpedThicknessByDistance.ToArray();
            var mul = Mafi.Fix32.FromDouble(amount);
            dumpedThicknessByDistance = dumpedThicknessByDistance.RemoveRange(0, val.Length).InsertRange(0, val.SelectMany(x=>Enumerable.Repeat(x * mul, dist)));
        }
    }

    class excavator_mine_mult : Entry {
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.Ctor(original, typeof(Mafi.Core.Vehicles.Excavators.ExcavatorProto), "minedThicknessByDistance", typeof(Mafi.Collections.ImmutableCollections.ImmutableArray<Mafi.ThicknessTilesF>));
        [Desc("挖掘数量乘数")]
        static double amount=1000;
        [Desc("挖掘距离乘数")]
        static int dist=3;
        static void Prefix(ref Mafi.Collections.ImmutableCollections.ImmutableArray<Mafi.ThicknessTilesF> minedThicknessByDistance){
            var val = minedThicknessByDistance.ToArray();
            var mul = Mafi.Fix32.FromDouble(amount);
            minedThicknessByDistance = minedThicknessByDistance.RemoveRange(0, val.Length).InsertRange(0, val.SelectMany(x=>Enumerable.Repeat(x * mul, dist)));
        }
    }

    class excavator_mine_speed : Entry {
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.Ctor(original, typeof(Mafi.Core.Vehicles.Excavators.ExcavatorProto.Timings), "prepareToMineDuration", typeof(Mafi.Duration));
        [Desc("速度乘数")]
        static double speed_mul=10;
        static void Prefix(ref Mafi.Duration prepareToMineDuration, ref Mafi.Duration mineDuration, ref Mafi.Duration prepareToDumpDuration, ref Mafi.Duration dumpDuration, ref Mafi.Duration dumpDelay, int mineTileIterations, ref Mafi.Duration mineIterationDuration){
            var speed = Mafi.Percent.FromDouble(1/speed_mul);
            prepareToMineDuration = Mafi.Duration.OneTick.Max(prepareToMineDuration.ScaledBy(speed));
            prepareToDumpDuration = Mafi.Duration.OneTick.Max(prepareToDumpDuration.ScaledBy(speed));
            mineIterationDuration = Mafi.Duration.OneTick.Max(mineIterationDuration.ScaledBy(speed));
            dumpDuration = Mafi.Duration.OneTick.Max(dumpDuration.ScaledBy(speed));
            dumpDelay = Mafi.Duration.OneTick.Max(dumpDelay.ScaledBy(speed));
            mineDuration = mineTileIterations * mineIterationDuration;
        }
    }
    // [HarmonyPatch(typeof(Mafi.Core.Entities.Dynamic.ExcavatorProtoBuilder.ExcavatorProtoBuilderState),"SetMinedThicknessByDistanceMeters")]
    // class ExcavatorthicknessMeters_mult : Entry {
    //     [Desc("挖掘机深度修正-长宽乘数")]
    //     static int len_mul = 3;
    //     [Desc("挖掘机深度修正-深度乘数")]
    //     static double depth = 10;
    //     static void Prefix(ref float[] thicknessMeters){
    //         thicknessMeters=new float[thicknessMeters.Length*len_mul + len_add];
    //         for(int i=0;i<thicknessMeters.Length;i++){
    //             thicknessMeters[i]=depth;
    //         }
    //     }
    // }

    [HarmonyPatch(typeof(Mafi.Core.Buildings.ResearchLab.ResearchLabProtoBuilder.State),"SetResearchSpeed")]
    class ResearchSpeed_mult : Entry {
        [Desc("研究产出乘数")]
        static int mult=1000;
        [Desc("研究产出速度")]
        static int speed=10;
        static void Prefix(ref Mafi.Duration duration, ref Mafi.Fix32 stepsPerDuration){
            stepsPerDuration=stepsPerDuration*mult;
            duration=new Mafi.Duration((duration.Ticks+speed-1)/speed);
        }
    }
    class TreePlanterSpeed_mult : Entry {
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.Ctor(original, typeof(Mafi.Core.Vehicles.TreePlanters.TreePlanterProto.Timings), "plantingDuration", typeof(Mafi.Duration));
        [Desc("植树机速度")]
        static int speed=10;
        static void Prefix(ref Mafi.Duration plantingDuration, ref Mafi.Duration returningToIdleDuration){
            plantingDuration=new Mafi.Duration((plantingDuration.Ticks+speed-1)/speed);
            returningToIdleDuration=new Mafi.Duration((returningToIdleDuration.Ticks+speed-1)/speed);
        }
    }
    class TreeHarvesterSpeed_mult : Entry {
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.Ctor(original, typeof(Mafi.Core.Vehicles.TreeHarvesters.TreeHarvesterProto.Timings), "toPrepareForHarvestDuration", typeof(Mafi.Duration));
        [Desc("砍树机速度")]
        static int speed=10;
        static void Prefix(ref Mafi.Duration toPrepareForHarvestDuration, ref Mafi.Duration toTreeLayingDownDuration, ref Mafi.Duration toTreeAboveTruckDuration, ref Mafi.Duration toTreeOnTruckDuration, ref Mafi.Duration toArmUpDuration, ref Mafi.Duration toFoldedDuration, ref Mafi.Duration cuttingDuration, ref Mafi.Duration trimmingDuration, ref Mafi.Duration moveToNextSectionDuration, ref Mafi.Duration cutNextSectionDuration){
            toPrepareForHarvestDuration = new Mafi.Duration((toPrepareForHarvestDuration.Ticks+speed-1)/speed);
            toTreeLayingDownDuration = new Mafi.Duration((toTreeLayingDownDuration.Ticks+speed-1)/speed);
            toTreeAboveTruckDuration = new Mafi.Duration((toTreeAboveTruckDuration.Ticks+speed-1)/speed);
            toTreeOnTruckDuration = new Mafi.Duration((toTreeOnTruckDuration.Ticks+speed-1)/speed);
            toArmUpDuration = new Mafi.Duration((toArmUpDuration.Ticks+speed-1)/speed);
            toFoldedDuration = new Mafi.Duration((toFoldedDuration.Ticks+speed-1)/speed);
            cuttingDuration = new Mafi.Duration((cuttingDuration.Ticks+speed-1)/speed);
            trimmingDuration = new Mafi.Duration((trimmingDuration.Ticks+speed-1)/speed);
            moveToNextSectionDuration = new Mafi.Duration((moveToNextSectionDuration.Ticks+speed-1)/speed);
            cutNextSectionDuration = new Mafi.Duration((cutNextSectionDuration.Ticks+speed-1)/speed);
        }
    }
    class ExcavatorSpeed_mult : Entry {
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.Ctor(original, typeof(Mafi.Core.Vehicles.Excavators.ExcavatorProto.Timings), "prepareToMineDuration", typeof(Mafi.Duration));
        [Desc("挖掘机速度")]
        static int speed=10;
        static void Prefix(ref Mafi.Duration prepareToMineDuration, ref Mafi.Duration mineDuration, ref Mafi.Duration prepareToDumpDuration, ref Mafi.Duration dumpDuration, ref Mafi.Duration dumpDelay, ref Mafi.Duration mineIterationDuration){
            prepareToMineDuration=new Mafi.Duration((prepareToMineDuration.Ticks+speed-1)/speed);
            mineDuration=new Mafi.Duration((mineDuration.Ticks+speed-1)/speed);
            prepareToDumpDuration=new Mafi.Duration((prepareToDumpDuration.Ticks+speed-1)/speed);
            dumpDuration=new Mafi.Duration((dumpDuration.Ticks+speed-1)/speed);
            dumpDelay=new Mafi.Duration((dumpDelay.Ticks+speed-1)/speed);
            mineIterationDuration=new Mafi.Duration((mineIterationDuration.Ticks+speed-1)/speed);
        }
    }

    class TreeHarvestSpeed_mult : Entry {
        static IEnumerable<MethodBase> TargetMethods(MethodBase original) => Get.Ctor(original, typeof(Mafi.Core.Vehicles.TreeHarvesters.TreeHarvesterProto.Timings), "toPrepareForHarvestDuration", typeof(Mafi.Duration));
        [Desc("伐木速度")]
        static int speed=10;
        static void Prefix(ref Mafi.Duration toPrepareForHarvestDuration, ref Mafi.Duration toTreeLayingDownDuration, ref Mafi.Duration toTreeAboveTruckDuration, ref Mafi.Duration toTreeOnTruckDuration, ref Mafi.Duration toArmUpDuration, ref Mafi.Duration toFoldedDuration, ref Mafi.Duration cuttingDuration, ref Mafi.Duration trimmingDuration, ref Mafi.Duration moveToNextSectionDuration, ref Mafi.Duration cutNextSectionDuration){
            toPrepareForHarvestDuration=new Mafi.Duration((toPrepareForHarvestDuration.Ticks+speed-1)/speed);
            toTreeLayingDownDuration=new Mafi.Duration((toTreeLayingDownDuration.Ticks+speed-1)/speed);
            toTreeAboveTruckDuration=new Mafi.Duration((toTreeAboveTruckDuration.Ticks+speed-1)/speed);
            toTreeOnTruckDuration=new Mafi.Duration((toTreeOnTruckDuration.Ticks+speed-1)/speed);
            toArmUpDuration=new Mafi.Duration((toArmUpDuration.Ticks+speed-1)/speed);
            toFoldedDuration=new Mafi.Duration((toFoldedDuration.Ticks+speed-1)/speed);
            cuttingDuration=new Mafi.Duration((cuttingDuration.Ticks+speed-1)/speed);
            trimmingDuration=new Mafi.Duration((trimmingDuration.Ticks+speed-1)/speed);
            moveToNextSectionDuration=new Mafi.Duration((moveToNextSectionDuration.Ticks+speed-1)/speed);
            cutNextSectionDuration=new Mafi.Duration((cutNextSectionDuration.Ticks+speed-1)/speed);
        }
    }
    // [HarmonyPatch(typeof(Mafi.Core.Buildings.Farms.Farm),"getNaturalReplenishPerDayAt")]
    // class Farm_getNaturalReplenishPerDayAt : Entry {
    //     [Desc("农场肥力平衡目标百分比")]
    //     static int percentage=1000;
    //     public override void Enable(){
    //         Target=Mafi.Percent.One*percentage;
    //         base.Enable();
    //     }
    //     static Mafi.Percent Target=Mafi.Percent.One;
    //     static bool Prefix(Mafi.Percent __result, Mafi.Percent fertility, Mafi.Core.Buildings.Farms.Farm __instance){
    //         var result = (Target - fertility) * __instance.Prototype.FertilityReplenishPerDay;
    //         if (result.IsNegative) {
    //             return result.ScaleBy(Mafi.Core.Buildings.Farms.Farm.FERTILITY_REPLENISH_MULT_WHEN_ABOVE_100);
    //         } else {
    //             return result;
    //         }
    //     }
    // }
    class farm_yield_mult : Entry {
        static IEnumerable<MethodBase> TargetMethods() {
            foreach(var c in typeof(Mafi.Core.Buildings.Farms.FarmProto).GetConstructors((BindingFlags)(-1))){
                var counter=0;
                foreach(var p in c.GetParameters()){
                    if((p.Name=="waterEvaporationPerDay" && p.ParameterType == typeof(Mafi.PartialQuantity)) || (p.Name=="yieldMultiplier" &&  p.ParameterType == typeof(Mafi.Percent))){
                        counter++;
                        if(counter==2){
                            vlogger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+c.DeclaringType.Name+"的"+c.Name+"方法");
                            yield return c;break;
                        }
                    }
                }
            }
        }
        [Desc("水蒸发速率除数")]
        static int wmult=100;
        [Desc("农田产出乘数")]
        static int ymult=10;
        static Mafi.PartialQuantity we=Mafi.PartialQuantity.Epsilon;
        public override void Enable(){
            we=Mafi.PartialQuantity.Epsilon*(wmult-1); // 向上取整除
            base.Enable();
        }
        static void Prefix(ref Mafi.PartialQuantity waterEvaporationPerDay, ref Mafi.Percent yieldMultiplier){
            waterEvaporationPerDay=(waterEvaporationPerDay+we)/wmult;
            yieldMultiplier=yieldMultiplier*ymult;
        }
    }
    class FuelTankProto_mod : Entry {
        static IEnumerable<MethodBase> TargetMethods() {
            foreach(var c in typeof(Mafi.Core.Entities.Dynamic.FuelTankProto).GetConstructors((BindingFlags)(-1))){
                foreach(var p in c.GetParameters()){
                    if(p.Name == "duration" && p.ParameterType == typeof(Mafi.Duration)){
                        logger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+c.DeclaringType.Name+"的"+c.Name+"方法");
                        yield return c;break;
                    }
                }
            }
        }
        [Desc("车辆油耗除数")]
        static int div=100;
        static void Prefix(ref Mafi.Duration duration)=>duration=duration*div;
    }
    class SettlementFoodModuleProto_mod : Entry {
        static IEnumerable<MethodBase> TargetMethods() {
            foreach(var c in typeof(Mafi.Core.Buildings.Settlements.SettlementFoodModuleProto).GetConstructors((BindingFlags)(-1))){
                foreach(var p in c.GetParameters()){
                    if(p.Name == "capacityPerBuffer" && p.ParameterType == typeof(Mafi.Quantity)){
                        logger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+c.DeclaringType.Name+"的"+c.Name+"方法");
                        yield return c;break;
                    }
                }
            }
        }
        [Desc("定居点食物容量乘数")]
        static int mul=100;
        static void Prefix(ref Mafi.Quantity capacityPerBuffer)=>capacityPerBuffer=capacityPerBuffer*mul;
    }
    class FoodProto_mod : Entry {
        static IEnumerable<MethodBase> TargetMethods() {
            foreach(var c in typeof(Mafi.Core.Population.FoodProto).GetConstructors((BindingFlags)(-1))){
                var counter=0;
                foreach(var p in c.GetParameters()){
                    if((p.Name == "consumedPerHundredPopsPerMonth" && p.ParameterType == typeof(Mafi.Fix32)) || (p.Name=="upointsWhenProvided" && p.ParameterType == typeof(Mafi.Upoints))){
                        counter++;
                        if(counter==2){
                            logger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+c.DeclaringType.Name+"的"+c.Name+"方法");
                            yield return c;break;
                        }
                    }
                }
            }
        }
        [Desc("食物消耗除数")]
        static int div=100;
        [Desc("食物凝聚点倍数")]
        static int mul=100;
        static void Prefix(ref Mafi.Fix32 consumedPerHundredPopsPerMonth, ref Mafi.Upoints upointsWhenProvided){
            consumedPerHundredPopsPerMonth=(consumedPerHundredPopsPerMonth+(div-1))/div;
            upointsWhenProvided*=mul;
        }
    }
    // class DrivingData_mod : Entry {
    //     static IEnumerable<MethodBase> TargetMethods() {
    //         foreach(var c in typeof(Mafi.Core.Entities.Dynamic.DrivingData).GetConstructors((BindingFlags)(-1))){
    //             foreach(var p in c.GetParameters()){
    //                 if(p.Name == "maxForwardsSpeed" && p.ParameterType == typeof(Mafi.RelTile1f)){
    //                     logger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+c.DeclaringType.Name+"的"+c.Name+"方法");
    //                     yield return c;break;
    //                 }
    //             }
    //         }
    //     }
    //     [Desc("车辆速度乘数")]
    //     static int mul=10;
    //     static void Prefix(ref Mafi.RelTile1f maxForwardsSpeed, ref Mafi.RelTile1f maxBackwardsSpeed, ref Mafi.RelTile1f acceleration, ref Mafi.RelTile1f breaking, ref Mafi.AngleDegrees1f maxSteeringAngle, ref Mafi.AngleDegrees1f maxSteeringSpeed){
    //         maxForwardsSpeed=maxForwardsSpeed*mul;
    //         maxBackwardsSpeed=maxBackwardsSpeed*mul;
    //         acceleration=acceleration*mul;
    //         breaking=breaking*mul;
    //         maxSteeringAngle=maxSteeringAngle*mul;
    //         maxSteeringSpeed=maxSteeringSpeed*mul;
    //     }
    // }
    // class SmoothDriverSpeed_mult : Entry {
    //     static IEnumerable<MethodBase> TargetMethods() {
    //         foreach(var c in typeof(Mafi.Core.Entities.Dynamic.SmoothDriver).GetConstructors((BindingFlags)(-1))){
    //             foreach(var p in c.GetParameters()){
    //                 if(p.ParameterType == typeof(Mafi.Fix32)){
    //                     logger("%%PLUGIN_ID%%-类"+System.Reflection.MethodBase.GetCurrentMethod().ReflectedType.DeclaringType.Name+"正在注入类别"+c.DeclaringType.Name+"的"+c.Name+"方法");
    //                     yield return c;break;
    //                 }
    //             }
    //         }
    //     }
    //     [Desc("车辆前进速度")]
    //     static double fspeed=2;
    //     [Desc("车辆后退速度")]
    //     static double bspeed=3;
    //     [Desc("车辆加速度")]
    //     static double aspeed=10;
    //     [Desc("车辆刹车速度")]
    //     static double sspeed=10;
    //     static void Prefix(ref Mafi.Fix32 maxForwardsSpeed, ref Mafi.Fix32 maxBackwardsSpeed, ref Mafi.Fix32 maxAcceleration, ref Mafi.Fix32 maxBraking/*, ref Mafi.Fix32 brakingConservativeness*/){
    //         maxForwardsSpeed = Mafi.Fix32.FromDouble(maxForwardsSpeed.ToDouble()*fspeed);
    //         maxBackwardsSpeed = Mafi.Fix32.FromDouble(maxBackwardsSpeed.ToDouble()*bspeed);
    //         maxAcceleration = Mafi.Fix32.FromDouble(maxAcceleration.ToDouble()*aspeed);
    //         maxBraking = Mafi.Fix32.FromDouble(maxBraking.ToDouble()*sspeed);
    //     }
    // }
    class Mafi__DrivingData : Entry {
        [Desc("车辆前进速度")]
        static double fspeed=2;
        [Desc("车辆后退速度")]
        static double bspeed=5;
        [Desc("车辆加速度")]
        static double aspeed=10;
        [Desc("车辆刹车速度")]
        static double sspeed=1000;
        // [Desc("车辆刹车倾向")]
        // static double breaks=10;
        [Desc("车辆转向速度")]
        static double rspeed=1000;
        [HarmonyPatch(typeof(Mafi.Core.Entities.Dynamic.DrivingData), Desc.ctor, new Type[]{typeof(Mafi.RelTile1f), typeof(Mafi.RelTile1f), typeof(Mafi.Percent), typeof(Mafi.RelTile1f), typeof(Mafi.RelTile1f), typeof(Mafi.AngleDegrees1f), typeof(Mafi.AngleDegrees1f), typeof(Mafi.Fix32), typeof(Mafi.RelTile1f), typeof(Mafi.RelTile1f)})]
        static void Prefix(ref Mafi.RelTile1f maxForwardsSpeed, ref Mafi.RelTile1f maxBackwardsSpeed, ref Mafi.Percent steeringSpeedMult, ref Mafi.RelTile1f acceleration, ref Mafi.RelTile1f breaking, ref Mafi.AngleDegrees1f maxSteeringAngle, ref Mafi.AngleDegrees1f maxSteeringSpeed, ref Mafi.Fix32 breakingConservativness, ref Mafi.RelTile1f steeringAxleOffset, ref Mafi.RelTile1f nonSteeringAxleOffset){
            maxForwardsSpeed = maxForwardsSpeed*Mafi.Fix32.FromDouble(fspeed);
            maxBackwardsSpeed = maxBackwardsSpeed*Mafi.Fix32.FromDouble(bspeed);
            acceleration = acceleration*Mafi.Fix32.FromDouble(aspeed);
            breaking = breaking*Mafi.Fix32.FromDouble(sspeed);
            // breakingConservativness = breakingConservativness*Mafi.Fix32.FromDouble(breaks);
            maxSteeringAngle = Mafi.AngleDegrees1f.FromDegrees(60);
            steeringSpeedMult = Mafi.Percent.FromPercentVal(100);
            maxSteeringSpeed = maxSteeringSpeed*Mafi.Fix32.FromDouble(rspeed);
            steeringAxleOffset = nonSteeringAxleOffset = Mafi.RelTile1f.Zero;
        }
    }
    [Desc("快速交易不涨价")]
    [HarmonyPatch(typeof(Mafi.Core.World.QuickTrade.QuickTradeProvider),"setStep")]
    class no_extra_pay : Entry {
        static void Prefix(ref int newStep){
            newStep = 0;
        }
    }
}
